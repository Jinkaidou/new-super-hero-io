using System.Collections.Generic;
using System.IO;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using Model.DataHero;
using Model.DataName;
using UI.UpgradeStatsPopup.Data;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Controller.LoadData
{
    public class LoadResourceController : SingletonMono<LoadResourceController>
    {
        private Dictionary<string, Object> _resourceCache = new Dictionary<string, Object>();

        #region LoadMethod

        private T Load<T>(string path, string fileName) where T : Object
        {
            var fullPath = Path.Combine(path, fileName);
            if (_resourceCache.ContainsKey(fullPath) is false)
            {
                _resourceCache.Add(fullPath, TryToLoad<T>(path, fileName));
            }

            return _resourceCache[fullPath] as T;
        }

        private static T TryToLoad<T>(string path, string fileName) where T : Object
        {
            var fullPath = Path.Combine(path, fileName);
            var result = Resources.Load<T>(fullPath);
            return result;
        }

        #endregion

        #region Public Load Method

        public PopupBase LoadPanel(PopupType panelType)
        {
            return Load<PopupBase>(ResourcesFolderPath.UiFolder, panelType.ToString());
        }

        public GameObject LoadPanel(string panelName)
        {
            return Load<GameObject>(ResourcesFolderPath.UiFolder, panelName);
        }

        public AudioClip LoadAudioClip(SoundType soundType)
        {
            return Load<AudioClip>(ResourcesFolderPath.SoundFolder, soundType.ToString());
        }

        public Sprite LoadMoneyIcon(MoneyType moneyType)
        {
            var path = Path.Combine(ResourcesFolderPath.IconFolder, "Money");
            return Load<Sprite>(path, moneyType.ToString());
        }

        public Sprite LoadStatUpgradeIcon(StatType statType)
        {
            var path = Path.Combine(ResourcesFolderPath.IconFolder, "StatUpgrade");
            return Load<Sprite>(path, statType.ToString());
        }

        public Sprite LoadSkinIcon(SkinType skinType)
        {
            var path = Path.Combine(ResourcesFolderPath.IconFolder, "Skin");
            return Load<Sprite>(path, skinType.ToString());
        }
        
        public Sprite LoadSkinIconSkin(SkinType skinType)
        {
            var path = Path.Combine(ResourcesFolderPath.IconFolder, "SkinIcon");
            return Load<Sprite>(path, skinType.ToString());
        }
        
        public Sprite LoadDataSkin(string name)
        {
            var path = Path.Combine(ResourcesFolderPath.HeroFolder, "Character");
            return Load<Sprite>(path, name.ToString());
        }

        public Sprite LoadDataWeapon(SkinType skin)
        {
            var path = Path.Combine(ResourcesFolderPath.HeroFolder, "Character/weapon");
            return Load<Sprite>(path, skin.ToString());
        }

        public string LoadPathDataWeapon(SkinType skin)
        {
            var path = Path.Combine(ResourcesFolderPath.HeroFolder, "Character/weapon/");
            return (path+skin.ToString());
        }


        public string LoadPathDataCharacter(SkinType skin)
        {
            var path = Path.Combine(ResourcesFolderPath.HeroFolder, "Character/");
            return (path + skin.ToString());
        }
        #endregion

        #region LoadDataAsset

        public FoodTypeCollection LoadFoodDataCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderFood);
            return Load<FoodTypeCollection>(path, "FoodData");
        }

        public HeroCollection LoadHeroDataCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderHero);
            return Load<HeroCollection>(path, "HeroData");
        }

        public ItemCollection LoadItemDataCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderItem);
            return Load<ItemCollection>(path, "ItemData");
        }

        public NameCollection LoadNameDataCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderName);
            return Load<NameCollection>(path, "NameData");
        }
        
        public NumberSpawnCollection LoadSpawnDataCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderSpawnData);
            return Load<NumberSpawnCollection>(path, "SpawnData");
        }

        public PercentSpawnCollection LoadPercentDataCollection()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderPercentSpawnData);
            return Load<PercentSpawnCollection>(path, "PercentSpawnData");
        }

        public StatUpgradeDataAsset LoadStatUpgradeData()
        {
            var path = string.Format(ResourcesFolderPath.DataFolder, ResourcesFolderPath.DataFolderStatUpgrade);
            return Load<StatUpgradeDataAsset>(path, "StatUpgradeData");
        }



        #endregion
    }
}
