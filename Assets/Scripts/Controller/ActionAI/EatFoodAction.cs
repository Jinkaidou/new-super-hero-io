using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
public class EatFoodAction : BaseActionBehaviourTree
{
    private bool isGoingToEat = false;
    public override void OnAwake()
    {
        if (!aIBehaviourController)
        {
            aIBehaviourController = GetComponent<AIBehaviourController>();
        }
    }
    public override TaskStatus OnUpdate()
    {
        if (isGoingToEat)
        {
            isGoingToEat = false;
        }
        if (aIBehaviourController.CanEat("food"))
        {
            aIBehaviourController.MovingtoPoint(true);
            return TaskStatus.Running;
        }
        return TaskStatus.Success;
    }

    public override void OnEnd()
    {
        isGoingToEat = true;
    }
}
