using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
public class AttackAction : BaseActionBehaviourTree
{
    private bool isGoingtoAttack = false;
    public override void OnAwake()
    {
        if (!aIBehaviourController)
        {
            aIBehaviourController = GetComponent<AIBehaviourController>();
        }
    }
    public override TaskStatus OnUpdate()
    {
        if (isGoingtoAttack)
        {
            isGoingtoAttack = false;
        }
        if (aIBehaviourController.CanEat("otherPlayer1"))
        {
            aIBehaviourController.MovingtoPoint(true);
            return TaskStatus.Running;
        }
        return TaskStatus.Success;
    }

    public override void OnEnd()
    {
        isGoingtoAttack = true;
    }
}
