using Core.Pool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeController : MonoBehaviour
{

    private void Start()
    {
        LeanTween.delayedCall(3f, () =>
        {
            DestroySmoke();
        });
    }

    public void DestroySmoke()
    {
        SmartPool.Instance.Despawn(gameObject);
        //Destroy(this.gameObject);
    }
}
