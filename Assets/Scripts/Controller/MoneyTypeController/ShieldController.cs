using System.Collections;
using System.Collections.Generic;
using Base.Core.Sound;
using Common.Behaviour;
using UnityEngine;

public class ShieldController : MonoBehaviour
{
    public LevelController levelController;

    public WeaponController weapon;
    public CharacterBehaviour _characterBehaviour;
    public GameObject smoke;
    public bool playerOrBot;
    Coroutine ShieldCor;
    private void Update()
    {
        gameObject.transform.position = levelController.gameObject.transform.position;
    }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("weapon") && collision.gameObject.transform.name != weapon.gameObject.transform.name)
        {
            Instantiate(smoke, gameObject.transform.position, gameObject.transform.rotation);
            _characterBehaviour.itemEffectPlayerController.HitShield();
            var check = _characterBehaviour.attackController.checkSound;
            if (check)
            {
                SoundManager.Instance.PlayOneShot(SoundType.Bubble,1);
            }
            return;
        }
        if (collision.CompareTag("weaponPlayer") && collision.gameObject.transform.name != weapon.gameObject.transform.name && !_characterBehaviour.isPlayer)
        {
            Instantiate(smoke, gameObject.transform.position, gameObject.transform.rotation);
            _characterBehaviour.itemEffectPlayerController.HitShield();
            SoundManager.Instance.PlayOneShot(SoundType.Bubble,1);
            return;
        }
        // if (collision.CompareTag("food"))
        // {
        //     var foodController = collision.GetComponent<FoodController>();
        //     levelController.IncreaseLevel(foodController.point);
        //     foodController.SendEvent();
        //     foodController.DestroyGameObject();
        //     return;
        // }
        // if (collision.CompareTag("itemWeapon"))
        // {
        //     var itemWeapon = collision.GetComponent<WeaponController>();
        //     var itemEffectPlayerController = _characterBehaviour.itemEffectPlayerController;
        //     itemEffectPlayerController._pathWeapon = itemWeapon.pathImageWeapon;
        //     _characterBehaviour.weapon.gameObject.SetActive(true);
        //     _characterBehaviour.weapon.InitData(itemEffectPlayerController._pathWeapon, _characterBehaviour.weapon.colorEffect);
        //     Destroy(collision.gameObject);
        //     return;
        // }
        //
        // var itemController = collision.GetComponent<ItemController>();
        // if (collision.CompareTag("speedItem"))
        // {
        //     _characterBehaviour.itemEffectPlayerController.SpeedEff();
        //     itemController.SendEvent();
        //     itemController.DestroyGameObject();
        //     return;
        // }
        // if (collision.CompareTag("fadeItem"))
        // {
        //     _characterBehaviour.itemEffectPlayerController.FadeEff();
        //     itemController.SendEvent();
        //     itemController.DestroyGameObject();
        //     return;
        // }
        // if (collision.CompareTag("shieldItem"))
        // {
        //     _characterBehaviour.itemEffectPlayerController.ShieldEff();
        //     itemController.SendEvent();
        //     itemController.DestroyGameObject();
        //     return;
        // }
        // if (collision.CompareTag("bigSizeItem"))
        // {
        //     _characterBehaviour.itemEffectPlayerController.ScaleEff();
        //     itemController.SendEvent();
        //     itemController.DestroyGameObject();
        //     return;
        // }
        // if (collision.CompareTag("Gold"))
        // {
        //     var itemWeapon = collision.GetComponent<GoldController>();
        //     itemWeapon.DestroyGameObject();
        //     if (playerOrBot)
        //     {
        //         itemWeapon.SendEvent();
        //     }
        //     return;
        // }
        // if (collision.CompareTag("teleport"))
        // {
        //     _characterBehaviour.itemEffectPlayerController.TeleportEffect();
        //     itemController.SendEvent();
        //     itemController.DestroyGameObject();
        //     return;
        // }
        if (collision.CompareTag($"block") && !playerOrBot)
        {
            this.PostEvent(EventID.BlockSide);
        }

    }

}
