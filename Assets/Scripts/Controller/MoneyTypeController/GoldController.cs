using System.Collections;
using System.Collections.Generic;
using Core.Pool;
using UnityEngine;
using LTAUnityBase.Base.DesignPattern;

public class GoldController : MonoBehaviour,IobjectItem
{
    
    public void SendEvent()
    {
        this.PostEvent(EventID.EarnGold);
    }

    public void DestroyGameObject()
    {
        SmartPool.Instance.Despawn(gameObject);
    }
}
