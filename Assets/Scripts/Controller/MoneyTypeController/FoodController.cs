﻿using System;
using UnityEngine;
using foodResources;
using System.Collections.Generic;
using Core.Pool;
using LTAUnityBase.Base.DesignPattern;
public class FoodController : MonoBehaviour
{
    [SerializeField] private int _idFood;
    [SerializeField] private SpriteRenderer _sprite;
    public float point;
    public float scaleIngame;
    public int typeFood;
    public float speed;

    
    private void Start()
    {
        this.gameObject.transform.localScale += new Vector3(scaleIngame, scaleIngame, 0);
    }

    public void InitDataFood(FoodData foodData, int typeFoods, float pointNew = 0)
    {
        typeFood = typeFoods;
        _idFood = foodData.id;
        scaleIngame = foodData.scaleFoodinMap;
        if (pointNew == 0)
        {
            point = foodData.point;
        }
        else
        {
            speed = 5;
            point = pointNew;
        }
        
        _sprite.sprite = Resources.Load<Sprite>(foodData.name);
    }
    
    public void SendEvent()
    {
        this.PostEvent(EventID.EatFood, typeFood);
    }

    public void DestroyGameObject()
    {
        SmartPool.Instance.Despawn(gameObject);
    }
}
