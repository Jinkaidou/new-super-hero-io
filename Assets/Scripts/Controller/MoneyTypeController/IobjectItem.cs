using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IobjectItem
{
    public void SendEvent();
    public void DestroyGameObject();
}
