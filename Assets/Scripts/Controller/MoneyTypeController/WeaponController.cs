using Core.Pool;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public int idWeapon;
    public string pathImageWeapon;
    public TrailRenderer effectTrailRender;
    public Color colorEffect;
    [SerializeField] private SpriteRenderer _weap;
    private void Start()
    {
        effectTrailRender.startColor = Color.white;
        effectTrailRender.endColor = Color.white;
       
    }

    public void InitData(string pathWeapon, Color color)
    {
        gameObject.name = pathWeapon;
        pathImageWeapon = pathWeapon;
        _weap.sprite = Resources.Load<Sprite>(pathImageWeapon);
        colorEffect = color;
        effectTrailRender.startColor = Color.white;
        effectTrailRender.endColor = Color.white;
    }

    
    
    public void SetWitdhCurve(float width)
    {
        effectTrailRender.startWidth = width;
        effectTrailRender.endWidth = 0;
    }
    public void DisablePool()
    {
        SmartPool.Instance.Despawn(gameObject);
    }

    public void EnterShield()
    {
        this.StartDelayMethod(5f, DisableEffect);
    }
    
    public void DisableEffect()
    {
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }
}
