using UnityEngine;

public class WeaponBulletController : MoveController
{
    public int time;
    public WeaponController weaponClone;
    public Animator animatorBullet;
    public float scaleIngame;
    [SerializeField] private SpriteRenderer _weap;

    private float _timer;

    private void Start()
    {
        canMoveLimitNow = true;
        canMoveAttackNow = true;
        gameObject.transform.localScale += new Vector3(scaleIngame, scaleIngame, 0);
    }
    public void InitData(string pathWeapon, float scale)
    {
        scaleIngame = scale;
        _weap.sprite = Resources.Load<Sprite>(pathWeapon);
        _timer = time;
        animatorBullet.Play("BulletWeapon");
    }

    private void Update()
    {
        if (_timer > 0)
        {
            Move(gameObject.transform.up);
            _timer -= Time.deltaTime;
        }
        else
        {
            DropWeapon();
            Destroy(gameObject);
        }
    }

    private void DropWeapon()
    {
        var path = "HeroIO/Character/weapon/"+_weap.sprite.name;
        weaponClone.InitData(path,Color.white);
        Instantiate(weaponClone, gameObject.transform.position, gameObject.transform.rotation);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "shield")
        {
            collision.gameObject.SetActive(false);
        }
    }
}
