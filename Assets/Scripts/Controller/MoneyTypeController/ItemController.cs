using UnityEngine;
using ItemResources;
using System.Collections.Generic;
using Core.Pool;
using LTAUnityBase.Base.DesignPattern;
public class ItemController : MonoBehaviour,IobjectItem
{
    [SerializeField] private int _idItem;
    [SerializeField] private SpriteRenderer _sprite;

    public void InitDataItem(ItemData itemData)
    {
        this.gameObject.transform.tag = itemData.itemTag;
        _idItem = itemData.idItem;
        _sprite.sprite = Resources.Load<Sprite>(itemData.itemPath);
    }

    public void SendEvent()
    {
        this.PostEvent(EventID.EatItem, _idItem);
    }

    public void DestroyGameObject()
    {
        SmartPool.Instance.Despawn(this.gameObject);
    }

}
