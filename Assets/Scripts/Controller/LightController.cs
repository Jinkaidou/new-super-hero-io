using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    public GameObject light1;
    public GameObject light2;

    private void Start()
    {
        StartCoroutine(LedEffectLight());
        StartCoroutine(LedEffectLight2());
    }

    private void Lighton2()
    {
        light1.SetActive(true);
        light2.SetActive(false);
    }

    private void Lighton1()
    {
        light1.SetActive(false);
        light2.SetActive(true);
    }


    public IEnumerator LedEffectLight()
    {
        while (true)
        {
            Lighton2();
            yield return new WaitForSeconds(3f);
        }
    }
    public IEnumerator LedEffectLight2()
    {
        while (true)
        {
            Lighton1();
            yield return new WaitForSeconds(4f);
        }
    }
}
