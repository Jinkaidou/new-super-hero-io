using System;
using System.Collections;
using System.Collections.Generic;
using Base.Core;
using DataAccount;
using UnityEngine;
using TMPro;

namespace Controller
{
    public class CoolDownItemSystem : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI timeCoolDown;
        private Coroutine _countDownCor;

        public void StartCount(long time)
        {
            if (_countDownCor != null)
            {
                StopCoroutine(_countDownCor);
                _countDownCor = null;
            }
            _countDownCor =  StartCoroutine(TimeCounting(time));
        }

        public void StopCount()
        {
            StopCoroutine(_countDownCor);
        }
        
        private IEnumerator TimeCounting(long timeCount)
        {
            while (true)
            {
                timeCount -= 1;
                if (timeCount <= 0)
                {
                    this.gameObject.SetActive(false);
                }
                var timeSpan = TimeSpan.FromSeconds(timeCount);
                DataAccountPlayer.PlayerStatistic.SetValueTime(timeCount);
                DataAccountPlayer.PlayerStatistic.SetBestTime(timeCount);
                timeCoolDown.text = timeSpan.ToString(@"ss");
                yield return new WaitForSeconds(1f);
            }
        }
        
    }
}
