﻿using LTAUnityBase.Base.DesignPattern;
using System.Collections.Generic;
using Model.DataName;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameController
{
    public class RankingController : MonoBehaviour
    {
        public List<CharacterBehaviour> CharacterBehaviour = new List<CharacterBehaviour>();
        public TextMeshProUGUI[] rankingText;
        public float numberSmallest;
        public List<float> rankingPoint = new List<float>();
        public Dictionary<string, float> topFivePlayer = new Dictionary<string, float>();
        public NameCollection nameCollection;
        public float timecoolDown;
        public float defaultTimeCount;
        private void Start()
        {
            InitDataRanking();
        }
        private void Update()
        {
            timecoolDown -= Time.deltaTime;
            if (timecoolDown <= 0)
            {
                SortListRanking();
                timecoolDown = defaultTimeCount;
            }
            
        }
        public void InitDataRanking()
        {
            var rankingTextLength = rankingText.Length;

            for (int i = 0; i < CharacterBehaviour.Count; i++)
            {
                var currentPoint = CharacterBehaviour[i].levelController._currentLevel;
                var name = CharacterBehaviour[i].levelController.transform.name;
                rankingPoint.Add(currentPoint);
            }
            topFivePlayer.Add("Jinkaidou", 600);
            topFivePlayer.Add("Lucy", 100);
            topFivePlayer.Add("Tommy", 250);
            topFivePlayer.Add("Samson", 350);
            topFivePlayer.Add("Cypher", 300);
        }

        public void SortListRanking()
        {
            var rankingTextLength = rankingText.Length;
            numberSmallest = rankingPoint[rankingTextLength];
            foreach (var player in topFivePlayer)
            {
                var checkValue = rankingPoint.Contains(player.Value);
                if (!checkValue && player.Value > numberSmallest)
                {
                    rankingPoint.Add(player.Value);
                }
            }

            rankingPoint.Sort();
            rankingPoint.Reverse();

            
            for (int i = 0; i < rankingTextLength; i++)
            {
                var idName = Random.Range(0, nameCollection.listItemData.Count);
                var name = nameCollection.listItemData[idName].nameUser + i;
                var value = rankingPoint[i];
                foreach (var valuePlayer in topFivePlayer)
                {
                    if (valuePlayer.Value == value)
                    {
                        rankingText[i].text = name + ":" +value;
                    }
                }
                
            }
        }
    }
    public class Ranking : SingletonMonoBehaviour<RankingController>
    {

    }
}