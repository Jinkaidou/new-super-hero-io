using System;
using System.Collections;
using UnityEngine;
using Base.Core.Popup;
using Base.Core.Sound;
using Controller.LoadData;
using DataAccount;
using UI.RevivePopup;
using TMPro;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Base.Core.Analytics;
using UI.EndGameRewardPopup;

namespace GameController
{
    public class GameplayManager : MonoBehaviour
    {

        public PlayerCharacterController player;
        public RankingController rankingController;
        [SerializeField] private Button attackButton;
        [SerializeField] private TextMeshProUGUI timetxt;
        public GameObject[] listMap;
        
        private long timeCount;
        private void Awake()
        {
            this.RegisterListener(EventID.PlayerDie, (sender, param) =>
            {
                OpenPopUp();
            });
            this.RegisterListener(EventID.ReviveSuccess, (sender, param) =>
            {
                Revive();
            });
            
            attackButton.onClick.AddListener(HomeAction);
        }
        
        private void HomeAction()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            PopupController.Instance.OpenPopupAndKeepParent(PopupType.SettingPopup);
        }
        
        private void Start()
        {
            rankingController.CharacterBehaviour.Add(player);
            StartCoroutine(TimeCounting());
            LoadDataPlayer();
            LoadDataMap();
        }

        private void LoadDataMap()
        {
            var mapClone = Random.Range(0, listMap.Length);
            Instantiate(listMap[mapClone].gameObject, this.gameObject.transform.position, this.gameObject.transform.rotation);
        }
        
        private void LoadDataPlayer()
        {
            var skin = DataAccountPlayer.PlayerSkins.skinUsing;
            var heroData = LoadResourceController.Instance.LoadHeroDataCollection();
            var hero = heroData.listHeroData;
            player.transform.name = DataAccountPlayer.PlayerStatistic.playerName;
            player.InitDataHero(hero[(int)skin - 1],"Player");
            player.ActiveSafeZone();
        }
        
        private void PauseGame()
        {
            if (player.reviveCount > 0)
            {
                var popUpRevive = (ModuleUiRevivePopup)PopupController.Instance.OpenPopupAndKeepParent(PopupType.RevivePopup);
           
                FirebaseManager.Instance.LogRankingPlayer(player.transform.name);
                var countPlay = DataAccountPlayer.PlayerTutorialData.countPlayGame;
                if (countPlay > 2)
                { 
                    popUpRevive.InitData(player.reviveCount, 2);
                }
                else
                {
                    popUpRevive.InitData(player.reviveCount, 1);
                }
            }
            else
            {
                var giveupPopup =
                    (ModuleUiEndGameRewardPopup) PopupController.Instance.OpenPopupAndKeepParent(PopupType
                        .EndGameRewardPopup);
                var statParams = new StatParamMessage();
                statParams.Point = DataAccountPlayer.PlayerStatistic.point;
                statParams.Kill = DataAccountPlayer.PlayerStatistic.kill;
                statParams.Time = DataAccountPlayer.PlayerStatistic.time;
                giveupPopup.InitData(statParams, DataAccountPlayer.PlayerStatistic.goldLastRound);
            }
            
        }

        public void OpenPopUp()
        {
            FirebaseManager.Instance.LogEventDieTime(timeCount);
            this.StartDelayMethod(2, PauseGame);
        }

        public void Revive()
        {
            Time.timeScale = 1;
            player.ReviveSuccess();
            player.gameObject.SetActive(true);
            player.gameObject.transform.position = new Vector3(gameObject.transform.position.x,gameObject.transform.position.y,0);
        }
        
        public IEnumerator TimeCounting()
        {
            while (true)
            {
                timeCount += 1;
                var timeSpan = TimeSpan.FromSeconds(timeCount);
                DataAccountPlayer.PlayerStatistic.SetValueTime(timeCount);
                DataAccountPlayer.PlayerStatistic.SetBestTime(timeCount);
                timetxt.text = timeSpan.ToString(@"mm\:ss");
                yield return new WaitForSeconds(1f);
            }
        }
    }
}
