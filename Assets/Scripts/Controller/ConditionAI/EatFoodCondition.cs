using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

public class EatFoodCondition : BaseConditionBehaviourTree
{
    public override void OnAwake()
    {
        if (!aIBehaviourController)
        {
            aIBehaviourController = GetComponent<AIBehaviourController>();
        }
    }
    public override TaskStatus OnUpdate()
    {
        aIBehaviourController.DecideBehavior();
        if (aIBehaviourController.CanEat("food"))
        {
            return TaskStatus.Success;
        }
        return TaskStatus.Failure;
    }

}
