using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

public class RunnAwayCondition : BaseConditionBehaviourTree
{
    public override void OnAwake()
    {
        if (!aIBehaviourController)
        {
            aIBehaviourController = GetComponent<AIBehaviourController>();
        }
    }
    public override TaskStatus OnUpdate()
    {
        if (aIBehaviourController.Runnaway("otherPlayer1"))
        {
            return TaskStatus.Success;
        }
        return TaskStatus.Failure;
    }
}
