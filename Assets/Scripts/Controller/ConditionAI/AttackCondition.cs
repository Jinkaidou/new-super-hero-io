using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
public class AttackCondition : BaseConditionBehaviourTree
{
    public override void OnAwake()
    {
        if (!aIBehaviourController)
        {
            aIBehaviourController = GetComponent<AIBehaviourController>();
        }
    }

    public override TaskStatus OnUpdate()
    {
        aIBehaviourController.DecideBehavior();
        if (aIBehaviourController.checkAttack)
        {
            return TaskStatus.Success;
        }
        return TaskStatus.Failure;
    }

}
