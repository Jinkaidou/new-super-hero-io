using UnityEngine;

namespace Controller
{
    public class AnimLevelUpController : MonoBehaviour
    {
        public Animator levelUp;
        public GameObject glowLv;
        public void LevelUpAnim()
        {
            levelUp.gameObject.SetActive(true);
            levelUp.Play("levelUp");
        }

        public void EventLevelUp()
        {
            levelUp.gameObject.SetActive(false);
        }
    }
}
