using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimAttackController : MonoBehaviour
{
    public Animator attackAnim;
    public bool endAnim;
    public BoxCollider2D hitbox;
    public GameObject trail;

    public void EndAttack()
    {
        attackAnim.enabled = false;
        DisableAnim();
        endAnim = true;
        hitbox.enabled = false;
        this.PostEvent(EventID.MovingAgain);
    }
    
    public void DisableAnim()
    {
        trail.SetActive(false);
    }
    
    public void AttackAnim(float speed)
    {
        endAnim = false;
        trail.SetActive(true);
        hitbox.enabled = true;
        attackAnim.speed = speed;
        attackAnim.enabled = true;
        attackAnim.Play("Arm1Attack");
    }
    
}