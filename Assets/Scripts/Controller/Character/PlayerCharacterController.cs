using System.Collections;
using Base.Core.Analytics;
using Base.Core.Sound;
using Com.LuisPedroFonseca.ProCamera2D;
using Controller;
using Controller.Character;
using DataAccount;
using LTAUnityBase.Base.DesignPattern;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Model.Achivements;
using UnityEditor;
using Core.Pool;

public class PlayerCharacterController : CharacterBehaviour
{
    [SerializeField] private MovementJoystick movementJoystick;
    [SerializeField] private Button attackButton;
    [SerializeField] private GameObject smoke;
    [SerializeField] private Slider _expFill;
    [SerializeField] private TextMeshProUGUI _pointCurrentAndLimitTxt;
    [SerializeField] private TextMeshProUGUI _levelInRankingTxt;
    [SerializeField] private TextMeshProUGUI _levelCurrent;
    [SerializeField] private TextMeshProUGUI _killTxt;
    [SerializeField] private TextMeshProUGUI _goldTxt;
    [SerializeField] private TextMeshProUGUI killCountDown;
    [SerializeField] private TextMeshProUGUI killCount;
    [SerializeField] private GameObject killPopup;
    [SerializeField] private GameObject[] killPopupShow;
    [SerializeField] private GameObject safeZone;
    [SerializeField] private GameObject killCountPopup;
    
    [SerializeField] private GameObject btnMove;
    [SerializeField] private GameObject btnAttack;
    [SerializeField] private GameObject btnThrowWeap;
    [SerializeField] private GameObject levelUpObject;
    public AnimLevelUpController animLevelUpController;
    public CheckMovePlayer checkMovePlayer;
    public Animator bloodSpot;
    public Animator textKill;
    public int reviveCount;
    public float timeCountAchivement;
    private Rigidbody2D _rb;
    private int killNum;
    private int _calculateKill;
    private float timer;
    public int gold;
    private float _attackTimer;
    private bool _firstBlood;
    Coroutine timeCor;

    
    private void Awake()
    {
        levelController._limitLevel = heroCollection.dataLevel[0].dataLevel;
        levelController._currentLevel = 0;
        canControll = true;
        _firstBlood = true;
        InitStatValue();
        this.RegisterListener(EventID.EnemyDie, (sender, param) =>
        {
            CalculateKill();
        });
        this.RegisterListener(EventID.EarnGold, (sender, param) =>
        {
            CalculateGoldEarn();
        });
        this.RegisterListener(EventID.MovingAgain, (sender, param) =>
        {
            MovingAgain();
        });
        
        this.RegisterListener(EventID.LevelUp, (sender, param) =>
        {
            LevelUp();
        });

        attackButton.onClick.AddListener(Attack);
        
        shieldController.playerOrBot = true;
        if (DataAccountPlayer.PlayerTutorialData.countPlayGame > 2)
        {
            reviveCount = 2;
        }
        else
        {
            reviveCount = 1;
        }
        gold = 5;
        DataAccountPlayer.PlayerStatistic.SetValueGold(gold);
        FirstGame();
    }

    private void LevelUp()
    {
        animLevelUpController.LevelUpAnim();
        var vector = gameObject.transform.localScale;

    }
    
    public void FirstGame()
    {
        var playerTutData = DataAccountPlayer.PlayerTutorialData;
        btnMove.SetActive(playerTutData.firstMove);
        btnAttack.SetActive(playerTutData.attack);
        btnThrowWeap.SetActive(playerTutData.throwWeapon);
    }
    
    public void MovingAgain()
    {
        canControll = true;
    }
    
    public void ActiveSafeZone()
    {
        safeZone.SetActive(true);
        LeanTween.delayedCall(5f, () =>
        {
            safeZone.SetActive(false);
        });
    }
    
    private void OnEnable()
    {
        attackController.weapon.gameObject.SetActive(true);
    }

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        ProCamera2D.Instance.AddCameraTarget(transform);
    }
    
    protected override void Update()
    {
        CheckMove();
        CheckInputs(Time.deltaTime);
        ShowExpInTheBattle();
        base.Update();
    }
    
    private void CheckInputs(float dt)
    {
        _attackTimer -= dt;

#if !UNITY_EDITOR
        InputJoyStickMove();
        return;
#endif

        if (Input.GetMouseButtonDown(0))
        {
            Attack();
        }
        if (Input.GetMouseButtonDown(1))
        {
            var transform = movementController.bodyHero.transform;
            attackController.UltimateSkill(transform);
        }
        InputUnityDirectionMove();
    }

    
    private void Attack()
    {
        DataAccountPlayer.PlayerTutorialData.ChangeAttackFirst(false);
        btnAttack.SetActive(false);
        if (_attackTimer > 0)
            return;
        attackController.PlayerAttack();
        _attackTimer = 0.5f;
    }

    private IEnumerator CountAttack()
    {
        bool check = true;
        while (check)
        {
            if(timer <= 0)
            {
                killCountPopup.SetActive(false);
                killPopup .SetActive(false);
                _calculateKill = 0;
                check = false;
            }
            if (timer > 0)
            {
                timer -= 1;
                killPopup .SetActive(true);
                bloodSpot.Play("BloodAnim");
                //textKill.Play("ZoomKillTxtBattle");
                ShowAchivement();
                yield return new WaitForSeconds(1);
            }
        }
    }

    public void DisablePopup()
    {
        for (int i = 0; i < killPopupShow.Length; i++)
        {
            killCountDown.gameObject.SetActive(false);
            killPopupShow[i].gameObject.SetActive(false);
        }
    }
    
    private void ShowAchivement()
    {
        DisablePopup();
        killCountPopup.SetActive(true);
        killCount.text = "KILL X" + _calculateKill;
        
        if (_calculateKill == (int)Achievements.FirstBlood && _firstBlood)
        {
            _firstBlood = false;
            killPopupShow[0].gameObject.SetActive(true);
            killCountDown.gameObject.SetActive(true);
            killCountDown.text = Achievements.FirstBlood.ToString();
        }
        else if (_calculateKill == (int)Achievements.DoubleKill)
        {
            killPopupShow[0].gameObject.SetActive(true);
            killCountDown.gameObject.SetActive(true);
            killCountDown.text = "Double Kill";
        }
        else if (_calculateKill == (int)Achievements.TripleKill)
        {
            killPopupShow[1].gameObject.SetActive(true);
            killCountDown.gameObject.SetActive(true);
            killCountDown.text = "Triple Kill";
        }
        else if (_calculateKill == (int)Achievements.QuadraKill)
        {
            killPopupShow[2].gameObject.SetActive(true);
            killCountDown.gameObject.SetActive(true);
            killCountDown.text = "Quadra Kill";
        }
        else if (_calculateKill == (int)Achievements.PenTaKill)
        {
            killPopupShow[3].gameObject.SetActive(true);
            killCountDown.gameObject.SetActive(true);
            killCountDown.text = "Penta Kill";
        }
        else if (_calculateKill >= (int)Achievements.GodLike)
        {
            killPopupShow[3].gameObject.SetActive(true);
            killCountDown.gameObject.SetActive(true);
            killCountDown.text = "God Like";
        }
    }
    
    private void CalculateKill()
    {
        killNum += 1;
        _calculateKill += 1;
        timer = timeCountAchivement;
        DataAccountPlayer.PlayerStatistic.SetValueKill(killNum);
        DataAccountPlayer.PlayerStatistic.SetBestKill(killNum);
        textKill.Play("ZoomKillTxtBattle",-1,0f);
        if (timeCor != null)
        {
            this.StopCoroutine(timeCor);
        }
        timeCor =  this.StartCoroutine(CountAttack());
    }

    private void CalculateGoldEarn()
    {
        gold += 5;
        DataAccountPlayer.PlayerStatistic.SetValueGold(gold);
    }

    
    public void CheckMove()
    {
        var check = checkMovePlayer.canMove;
        if (check)
        {
            movementController.MovingAgainLimit();
        }
        else
        {
            movementController.StopMovingLimit();
        }
    }

    public void AttackUltimate()
    {
        DataAccountPlayer.PlayerTutorialData.ChangeThroWWeapFirst(false);
        btnThrowWeap.SetActive(false);
        var transform = movementController.bodyHero.transform;
        attackController.UltimateSkill(transform);
    }
    private void InputUnityDirectionMove()
    {
        if (!canControll)
            return;
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horizontal, vertical);
        direction.Normalize();
        movementController.Move(direction);
    }

    private void InputJoyStickMove()
    {
        if (!canControll)
            return;
        if (movementJoystick.joystickVec.y != 0)
        {
            DataAccountPlayer.PlayerTutorialData.ChangeMoveFirst(false);
            btnMove.SetActive(false);
            animController.PlayAnim();
            var horizontal = movementJoystick.joystickVec.x;
            var vertical = movementJoystick.joystickVec.y;
            Vector3 direction = new Vector2(horizontal, vertical);
            direction.Normalize();
            movementController.Move(direction);
        }
        else
        {
            _rb.velocity = Vector2.zero;
            movementController.Move(Vector3.zero);
        }
    }

    private void ShowExpInTheBattle()
    {
        var herocollection = heroCollection.dataLevel;
        _levelInRankingTxt.text = $"{DataAccountPlayer.PlayerStatistic.playerName}: {levelController._currentLevel}";
        _killTxt.text = killNum.ToString();
        _goldTxt.text = gold.ToString();
        _expFill.maxValue = levelController._limitLevel;
        _expFill.minValue = levelController.startLevel;
        if (levelController._currentLevel >= herocollection[herocollection.Count - 1].dataLevel)
        {
            _pointCurrentAndLimitTxt.text = "Max Level";
            _expFill.value = _expFill.maxValue;
        }
        else
        {
            _expFill.value = levelController._currentLevel;
            _pointCurrentAndLimitTxt.text = levelController._currentLevel + "/" + levelController._limitLevel;
            var level = levelController._levelPoint;
            if (level == 0)
            {
                _levelCurrent.text = "1";
            }
            else
            {
                _levelCurrent.text = (level + 1).ToString();
            }
        }
    }

   
    private void DefaultStat()
    {
        DataAccountPlayer.PlayerStatistic.SetValuePoint(levelController._currentLevel);
        DataAccountPlayer.PlayerStatistic.SetBestPoint(levelController._currentLevel);
        itemEffectPlayerController.DisableBigScale();
        itemEffectPlayerController.DisableFade();
        itemEffectPlayerController.DisableShield();
        itemEffectPlayerController.DisableSpeddEff();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("weapon") && !isShieldOpen)
        {
            smoke.gameObject.transform.localScale = gameObject.transform.localScale;
            //Instantiate(smoke, gameObject.transform.position, gameObject.transform.rotation);
            attackController.SetDefaultPos();
            SmartPool.Instance.Spawn(smoke, gameObject.transform.position, gameObject.transform.rotation);
            DefaultStat();
            gameObject.SetActive(false);
            SoundManager.Instance.PlaySound(SoundType.Boom);
            FirebaseManager.Instance.LogKillPerRound(killNum);
            this.PostEvent(EventID.PlayerDie);
        }
        if (collision.CompareTag("weaponPlayer") && !isShieldOpen)
        {
        }
        if (collision.CompareTag("Gold"))
        {
            var itemWeapon = collision.GetComponent<GoldController>();
            itemWeapon.SendEvent();
            itemWeapon.DestroyGameObject();
        }
    }

    public void ReviveSuccess()
    {
        canControll = true;
        reviveCount -= 1;
        _calculateKill = 0;
        movementController.canMoveAttackNow = true;
        InitStatValue();
        itemEffectPlayerController.ShieldEff();
        ActiveSafeZone();
    }
        
}

public class Player : SingletonMonoBehaviour<PlayerCharacterController>
{

}