using UnityEngine;
using UnityEngine.Serialization;

namespace Controller.Character
{
    public class CheckMovePlayer : MonoBehaviour
    {
        [FormerlySerializedAs("CanMove")] public bool canMove = true;

        // Update is called once per frame
        void Update()
        {
            CheckMove();
        }
    
        public void CheckMove()
        {
            RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, transform.up, 0.25f);
            Debug.DrawRay(transform.position, transform.up, Color.cyan, 0.25f);
            for (int i = 0; i < hits.Length; i++)
            {
                var hit = hits[i];
                if (hit.transform.CompareTag("block"))
                {
                    canMove = false;
                }
                else if(!hit.transform.CompareTag("Untagged"))
                {
                    canMove = true;
                }
            }
        }
    }
}
