using System;
using Base.Core.Sound;
using Common.Behaviour;
using Core.Pool;
using LTAUnityBase.Base.DesignPattern;
using UnityEngine;
using PolyNav;
using SpawnController;
using Random = UnityEngine.Random;

public class AIBehaviourController : CharacterBehaviour
{
    public GameObject smoke;
    private float _smallestDistance;
    public WayPointController point;
    private PolyNavAgent _agent;
    private WayPointController _pointDebug;

    public string tagWantFind;
    public string tagDontWantToFight;

    public bool checkEat = true;
    public bool checkAttack = false;
    public bool checkRun = false;

    public bool checkPoint = true;

    public HeroTypeAI TypeEnemy;
    public float RangeScan;
    public float timecoolDown;
    public float defaultTimeCount;
    private PolyNavAgent agent
    {
        get { return _agent != null ? _agent : _agent = GetComponent<PolyNavAgent>(); }
    }


    private void Awake()
    {
        isPlayer = false;
        point.InitInformationb(gameObject.transform.name);
         _pointDebug = Instantiate(point, gameObject.transform.position, gameObject.transform.rotation);
        shieldController.playerOrBot = false;
        this.RegisterListener(EventID.EnemyDie, (sender, param) =>
        {
            ChangeLocation();
        });
    }

    protected override void Update()
    {
        attackController.AutoAttacking();
        timecoolDown -= Time.deltaTime;
        if (timecoolDown <= 0)
        {
            attackController.AutoUseUltimate();
            timecoolDown = defaultTimeCount;
        }
        base.Update();
    }

    public void MovingtoPoint(bool position)
    {
        if (movementController.canMoveAttackNow)
        {
            agent.maxSpeed = 4.8f; 
            if (position)
            {
                agent.SetDestination(DirectionBeheavior());
            }
            else
            {
                agent.SetDestination(-DirectionBeheavior());
            } 
        }
        else
        {
            agent.maxSpeed = 0;
        }
      
    }

    public void MovingRunaway(bool position)
    {
        if (movementController.canMoveAttackNow)
        {
            agent.maxSpeed = 4.8f;
            if (position)
            {
                ChangeLocation();
                agent.SetDestination(DirectionBeheavior());
            }
            else
            {
                ChangeLocation();
                agent.SetDestination(-DirectionBeheavior());
            } 
        }
        else
        {
            agent.maxSpeed = 0;
        }
    }
    
    public bool AttackOrNot(string tagName)
    {
        var enemyPos = gameObject.transform.position;
        RaycastHit2D[] hits = Physics2D.CircleCastAll(enemyPos, RangeScan, Vector3.zero);
        for (int i = 0; i < hits.Length; i++)
        {
            tagWantFind = tagName;
            var hit = hits[i];
            if (hit && hit.transform.name != gameObject.transform.name &&
                hit.transform.tag == tagName)
            {
                var levelOp = hit.transform.GetComponent<LevelController>()._currentLevel;
                var level = levelController._currentLevel;
                if (level >= levelOp)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    public bool  CanEat(string tagName)
    {
        var enemyPos = gameObject.transform.position;
        RaycastHit2D[] hits = Physics2D.CircleCastAll(enemyPos, RangeScan, Vector3.zero);
        for (int i = 0; i < hits.Length; i++)
        {
            tagWantFind = tagName;
            var hit = hits[i];
            if (hit && hit.transform.name != gameObject.transform.name &&
                hit.transform.tag == tagName)
            {
                return true;
            }
        }
        return false;
    }

    public bool Runnaway(string tagName)
    {
        var enemyPos = gameObject.transform.position;
        RaycastHit2D[] hits = Physics2D.CircleCastAll(enemyPos, 30f, Vector3.zero);
        for (int i = 0; i < hits.Length; i++)
        {
            tagWantFind = tagName;
            var hit = hits[i];
            if (hit && hit.transform.name != gameObject.transform.name &&
                hit.transform.tag == tagName)
            {
                var levelOp = hit.transform.GetComponent<LevelController>()._currentLevel;
                var level = levelController._currentLevel;
                if (level < levelOp)
                {
                    return true;
                }
            }
        }
        return true;
    }

    private Vector3 DirectionBeheavior()
    {
        var enemyPos = gameObject.transform.position;
        var distanceToPoint = Vector3.Distance(enemyPos, _pointDebug.transform.position);
        if (distanceToPoint <= 0.5f)
        {
            
            RaycastHit2D[] hits = Physics2D.CircleCastAll(enemyPos, RangeScan, Vector3.zero);
            for (int i = 0; i < hits.Length; i++)
            {
                var hit = hits[i];
                _smallestDistance = Vector3.Distance(enemyPos, hits[0].transform.position);
                var transform1 = _pointDebug.transform;
                transform1.position = hit.transform.position;
                var distance = Vector3.Distance(enemyPos, transform1.position);
                if (hit && hit.transform.name != gameObject.transform.name && hit.transform.CompareTag(tagWantFind) && !hit.transform.CompareTag("block") && !hit.transform.CompareTag("Map"))
                {
                    if (distance <= _smallestDistance)
                    {
                        _smallestDistance = distance;
                        var direction = _pointDebug.transform.position - transform.position;
                        direction.Normalize();
                        return _pointDebug.transform.position;
                    }
                    else
                    {
                        var direction = _pointDebug.transform.position - transform.position;
                        direction.Normalize();
                        return _pointDebug.transform.position;
                    }
                }
            }
        }
        else
        {
            var direction = _pointDebug.transform.position - transform.position;
            direction.Normalize();
            return _pointDebug.transform.position;
        }
        return Vector3.zero;
    }

    public void DecideBehavior()
    {
        var enemyPos = gameObject.transform.position;
        var distanceToPoint = Vector3.Distance(enemyPos, _pointDebug.transform.position);
        if (distanceToPoint <= 0.5f)
        {
            RaycastHit2D[] hits = Physics2D.CircleCastAll(enemyPos, 30f, Vector3.zero);
            for (int i = 0; i < hits.Length; i++)
            {
                var hit = hits[i];
                _smallestDistance = Vector3.Distance(enemyPos, hits[0].transform.position);
                _pointDebug.transform.position = hit.transform.position;
                var distance = Vector3.Distance(enemyPos, _pointDebug.transform.position);

                if (hit && hit.transform.name != gameObject.transform.name)
                {
                    if (distance <= _smallestDistance)
                    {
                        _smallestDistance = distance;
                        if (hit.transform.CompareTag("food"))
                        {
                            checkEat = true;
                        }
                        else if (hit.transform.CompareTag("otherPlayer1"))
                        {
                            checkAttack = true;
                        }
                    }
                }

            }
        }
    }

    private void ChangeLocation()
    {
        var limitGreenAxis = Map.Instance.position.transform.position.y;
        var limitRedAxis = Map.Instance.position.transform.position.x;
        var y = Random.Range(-limitGreenAxis, limitGreenAxis);
        var x = Random.Range(-limitRedAxis, limitRedAxis);
        _pointDebug.transform.position = new Vector3(x, y, 0);
        agent.SetDestination(DirectionBeheavior());
    }

    private void OnDisable()
    {
        this.PostEvent(EventID.RespawnEnemy, TypeEnemy);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("weapon") && !isShieldOpen)
        {
            smoke.gameObject.transform.localScale = gameObject.transform.localScale;
            SmartPool.Instance.Spawn(smoke, gameObject.transform.position, gameObject.transform.rotation);
            itemEffectPlayerController.DropWeapon();
            if (attackController.checkSound)
            {
                SoundManager.Instance.PlayOneShot(SoundType.Boom,1);
            }
            Destroy(gameObject);
        }
        if (collision.name != gameObject.transform.name && collision.transform.CompareTag("otherPlayer1"))
        {
           attackController.InputAttack();
           ChangeLocation();
        }
        if (collision.CompareTag("weaponPlayer") && !isShieldOpen)
        {
            smoke.gameObject.transform.localScale = gameObject.transform.localScale;
            SmartPool.Instance.Spawn(smoke, gameObject.transform.position, gameObject.transform.rotation);
            itemEffectPlayerController.DropWeapon();
            this.PostEvent(EventID.EnemyDie, transform.position);
            var exp = levelController._currentLevel;
            var scale = gameObject.transform.localScale.x;
            SpawnFood.Instance.SpawnFoodEnemyDieIngame(transform.position,exp,scale);
            
            SoundManager.Instance.PlayOneShot(SoundType.Boom,1);
            Destroy(gameObject);
        }
        if (collision.CompareTag($"Gold"))
        {
            var itemWeapon = collision.GetComponent<GoldController>();
            itemWeapon.DestroyGameObject();
        }
        
        if (collision.CompareTag($"blocking"))
        {
            ChangeLocation();
        }
        
        if (collision.CompareTag($"block"))
        {
            ChangeLocation();
        }
        
    }
    
    
}