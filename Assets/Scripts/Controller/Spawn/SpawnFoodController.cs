using System;
using Controller.LoadData;
using Core.Pool;
using LTAUnityBase.Base.DesignPattern;
using Model.ObjectInGame;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpawnController
{
    public class SpawnFoodController : SpawnController
    {
        public FoodController foodObject;
        private FoodTypeCollection _foodTypeCollection;
        public float spawnFoodDrop;
        private void Start()
        {
            _foodTypeCollection = LoadResourceController.Instance.LoadFoodDataCollection();
            this.RegisterListener(EventID.EatFood, (sender, param) =>
            {
                SpawnFoodCircle((int)param);
            });

            CalculateSpawnFood();
        }


        private void CalculateSpawnFood()
        {
            var SpawnData = LoadResourceController.Instance.LoadSpawnDataCollection();
            var numberFoodSpawn = SpawnData.SpawnNumberInGame((int)ObjectInGame.Food);
            var PercentSpawnData = LoadResourceController.Instance.LoadPercentDataCollection();
            var listPercentFood = PercentSpawnData.listItemData[(int)ObjectInGame.Food];
            for (int i = 0; i < listPercentFood.percentSpawn.Count; i++)
            {
                var percent = listPercentFood.percentSpawn[i];
                SpawnFood(numberFoodSpawn, i, percent);
            }
        }


        private void SpawnFoodCircle(int typeFood)
        {
            var scaleFood = _foodTypeCollection.typeFoodCollection[typeFood].scaleFood;
            int idFood = Random.Range(0, scaleFood.Count);
            foodObject.InitDataFood(scaleFood[idFood],typeFood);
            this.StartDelayMethod(2, SpawnFoodEvent);

        }
        private void SpawnFoodEvent()
        {
            CreateFood(SpawnPos());
        }
        private void SpawnFood(float numberFood, int typeFood, float percent)
        {
            var numberFoodinMap = numberFood * (percent / 100);
            if (numberFoodinMap > 0)
            {
                var scaleFood = _foodTypeCollection.typeFoodCollection[typeFood].scaleFood;
                for (int i = 0; i < numberFoodinMap; i++)
                {
                    int idFood = Random.Range(0, scaleFood.Count);
                    foodObject.InitDataFood(scaleFood[idFood], typeFood);
                    CreateFood(SpawnPos());
                }
            }
        }

        public void SpawnFoodEnemyDie(Vector3 poision, float exp)
        {
            var percent = _foodTypeCollection.percentTake;
            var number = (exp / 100) * percent;
            for (int i = 0; i < spawnFoodDrop; i++)
            {
                var typeFoodCount = _foodTypeCollection.typeFoodCollection.Count;
                var typeFood = Random.Range(0, typeFoodCount);
                var scaleFood = _foodTypeCollection.typeFoodCollection[typeFood].scaleFood;
                int idFood = Random.Range(0, scaleFood.Count);
                foodObject.InitDataFood(scaleFood[idFood],typeFood ,number/spawnFoodDrop);
                var x = Random.Range( poision.x - 0.3f, poision.x + 0.3f);
                var y = Random.Range(-poision.y - 0.3f, poision.y + 0.3f);
                Vector3 position = new Vector3(x,y,0);
                var food = SmartPool.Instance.Spawn(foodObject.gameObject,position,gameObject.transform.rotation);
                var foodMove = food.GetComponent<FoodController>();
                foodMove.point = number / spawnFoodDrop;
            }
        }


        public void SpawnFoodEnemyDieIngame(Vector3 poision, float exp, float scale)
        {
            var percent = _foodTypeCollection.percentTake;
            var number = (exp / 100) * percent;
            var typeFoodCount = _foodTypeCollection.typeFoodCollection.Count;
            var typeFood = Random.Range(0, typeFoodCount);
            var scaleFood = _foodTypeCollection.typeFoodCollection[typeFood].scaleFood;
            int idFood = Random.Range(0, scaleFood.Count);
            foodObject.InitDataFood(scaleFood[idFood],typeFood ,number);
            Vector3 position = new Vector3(poision.x,poision.y,0);
            var food = SmartPool.Instance.Spawn(foodObject.gameObject,position,gameObject.transform.rotation);
            var foodMove = food.GetComponent<FoodController>();
            var pointReal = Math.Round(number, 0);
            if (pointReal == 0)
            {
                foodMove.point = 1;
            }
            foodMove.point = (float)pointReal;
            foodMove.scaleIngame = scale;
        }
        
        private void CreateFood(Vector3 position)
        {
           var food = SmartPool.Instance.Spawn(foodObject.gameObject,position,gameObject.transform.rotation);
        }
    }
    public class SpawnFood : SingletonMonoBehaviour<SpawnFoodController>
    {

    }
}

