﻿using Controller.LoadData;
using Core.Pool;
using LTAUnityBase.Base.DesignPattern;
using Model.ObjectInGame;
using UnityEngine;

namespace SpawnController
{
    public class SpawnItemComtroller : SpawnController
    {
        public ItemController _item;
        private ItemCollection _itemCollection;


        private void Start()
        {
            _itemCollection = LoadResourceController.Instance.LoadItemDataCollection();
            this.RegisterListener(EventID.EatItem, (sender, param) =>
            {
                SpawnItemGain((int)param);
            });
            CalculateItemSpawn();
        }


        public void SpawnItemGain(int idType)
        {
            var item = _itemCollection.listItemData[idType - 1];
            _item.InitDataItem(item);
            CreateItem(SpawnPos());
        }

        public void CalculateItemSpawn()
        {
            var SpawnData = LoadResourceController.Instance.LoadSpawnDataCollection();
            var numberItemSpawn = SpawnData.SpawnNumberInGame((int)ObjectInGame.PowerUp);

            var PercentSpawnData = LoadResourceController.Instance.LoadPercentDataCollection();
            var listPercentItem = PercentSpawnData.listItemData[(int)ObjectInGame.PowerUp];
            var rangeLimitItem = listPercentItem.percentSpawn.Count;
            for (int i = 0; i < listPercentItem.percentSpawn.Count; i++)
            {
                var idPercent = Random.Range(0, rangeLimitItem);
                var percent = listPercentItem.percentSpawn[idPercent];
                SpawnItem(numberItemSpawn, i, percent);
            }
        }

        public void SpawnItem(float numberItem, int idTypeItem, float percentItem)
        {
            var numberIteminMap = numberItem * (percentItem / 100);
            if (numberIteminMap > 0)
            {
                for (int i = 0; i < numberIteminMap; i++)
                {
                    var item = _itemCollection.listItemData[idTypeItem];
                    _item.InitDataItem(item);
                    CreateItem(SpawnPos());
                }
            }
        }
        
        private void CreateItem(Vector3 position)
        {
            var food = SmartPool.Instance.Spawn(_item.gameObject,position,this.gameObject.transform.rotation);
        }

    }

}