using Controller.LoadData;
using Core.Pool;
using LTAUnityBase.Base.DesignPattern;
using Model.ObjectInGame;
using UnityEngine;

namespace SpawnController
{
    public class SpawnGoldController : SpawnController
    {
        public GoldController goldController;


        private void Start()
        {
            CalculateSpawnGold();

            this.RegisterListener(EventID.EarnGold, (sender, param) =>
            {
                CreateGold(SpawnPos());
            });
        }
        public void CalculateSpawnGold()
        {
            var SpawnData = LoadResourceController.Instance.LoadSpawnDataCollection();
            var numberGoldSpawn = SpawnData.SpawnNumberInGame((int)ObjectInGame.Gold);
            SpawnGold(numberGoldSpawn);
        }

        private void SpawnGold(float numberFood)
        {
            for (int i = 0; i < numberFood; i++)
            {
                CreateGold(SpawnPos());
            }
        }


        private void CreateGold(Vector3 position)
        { 
            SmartPool.Instance.Spawn(goldController.gameObject, position, this.gameObject.transform.rotation);
        }
        
    }
}

