 using Controller.LoadData;
using LTAUnityBase.Base.DesignPattern;
using Model.ObjectInGame;
using UnityEngine;
using GameController;
using Model.DataName;

namespace SpawnController
{
    public class SpawnHeroController : SpawnController
    {
        public AIBehaviourController Collector;
        public AIBehaviourController Killer;
        public AIBehaviourController Neutral;

        private AIBehaviourController cloneHero;

        public RankingController rankingController;
        public NameCollection nameCollection;
        
        
        void Start()
        {

            this.RegisterListener(EventID.RespawnEnemy, (sender, param) =>
            {
                SpawnHeroType((int)param);
            });
            
            CalculateHero();
        }


        public void SpawnHero(int heroID, float numberSpawn)
        {
            var heroData = LoadResourceController.Instance.LoadHeroDataCollection();
            var hero = heroData.listHeroData;
            if (heroID == (int)HeroTypeAI.Collector)
            {
                for (int i = 0; i < numberSpawn; i++)
                {
                    int idHero = Random.Range(0, hero.Count);
                    var idName = Random.Range(0, nameCollection.listItemData.Count);
                    var name = nameCollection.listItemData[idName].nameUser + i;
                    Collector.InitDataHero(hero[idHero], name);
                    if (Collector is null)
                    {
                        return;
                    }
                    cloneHero = Instantiate(Collector, SpawnPos(), this.gameObject.transform.rotation);
                    cloneHero.name = name;
                    rankingController.CharacterBehaviour.Add(cloneHero);
                }
            }
            else if (heroID == (int)HeroTypeAI.Killer)
            {
                for (int i = 0; i < numberSpawn; i++)
                {
                    int idHero = Random.Range(0, hero.Count);
                    var idName = Random.Range(0, nameCollection.listItemData.Count);
                    var name = nameCollection.listItemData[idName].nameUser + i;

                    Killer.InitDataHero(hero[idHero], name);
                    if (Killer is null)
                    {
                        return;
                    }
                    cloneHero = Instantiate(Killer, SpawnPos(), this.gameObject.transform.rotation);
                    cloneHero.name = name;
                    rankingController.CharacterBehaviour.Add(cloneHero);
                }
            }
            else if (heroID == (int)HeroTypeAI.Neutral)
            {
                for (int i = 0; i < numberSpawn; i++)
                {
                    int idHero = Random.Range(0, hero.Count);
                    var idName = Random.Range(0, nameCollection.listItemData.Count);
                    var name = nameCollection.listItemData[idName].nameUser + i;
                    Neutral.InitDataHero(hero[idHero], name);
                    if (Neutral is null)
                    {
                        return;
                    }
                    cloneHero = Instantiate(Neutral, SpawnPos(), this.gameObject.transform.rotation);
                    cloneHero.name = name;
                    rankingController.CharacterBehaviour.Add(cloneHero);
                }
            }
        }

        public void CalculateHero()
        {
            var SpawnData = LoadResourceController.Instance.LoadSpawnDataCollection();
            var numberHeroSpawn = SpawnData.SpawnNumberInGame((int)ObjectInGame.Hero);
            Debug.LogError(numberHeroSpawn);
            var PercentSpawnData = LoadResourceController.Instance.LoadPercentDataCollection();
            var listPercentHero = PercentSpawnData.listItemData[(int)ObjectInGame.Hero];
            
            for (int i = 0; i < listPercentHero.percentSpawn.Count; i++)
            {
                var percent = listPercentHero.percentSpawn[i];
                var numberSpawnEachHero = numberHeroSpawn * (percent / 100f);
                SpawnHero(i+1, numberSpawnEachHero);
            }
        }

        public void SpawnHeroType(int data)
        {
            var typeHero = data;
            var heroData = LoadResourceController.Instance.LoadHeroDataCollection();
            var hero = heroData.listHeroData;
            for (int i = 0; i < 1; i++)
            {
                int idHero = Random.Range(0, hero.Count);
                var idName = Random.Range(0, nameCollection.listItemData.Count);
                var name = nameCollection.listItemData[idName].nameUser + i;

                if (typeHero == (int)HeroTypeAI.Collector)
                {
                    Collector.InitDataHero(hero[idHero], name);
                    if (Collector is null && cloneHero is null)
                    {
                        return;
                    }
                    cloneHero = Instantiate(Collector, SpawnPos(), this.gameObject.transform.rotation);
                    cloneHero.name = name;
                    rankingController.CharacterBehaviour.Add(cloneHero);
                }
                else if (typeHero == (int)HeroTypeAI.Killer)
                {
                    Killer.InitDataHero(hero[idHero], name);
                    if (Killer is null && cloneHero is null)
                    {
                        return;
                    }
                    cloneHero = Instantiate(Killer, SpawnPos(), this.gameObject.transform.rotation);
                    cloneHero.name = name;
                    rankingController.CharacterBehaviour.Add(cloneHero);
                }
                else if (typeHero == (int)HeroTypeAI.Neutral)
                {
                    Neutral.InitDataHero(hero[idHero], name);
                    if (Neutral is null && cloneHero is null)
                    {
                        return;
                    }
                    cloneHero = Instantiate(Neutral, SpawnPos(), this.gameObject.transform.rotation);
                    cloneHero.name = name;
                    rankingController.CharacterBehaviour.Add(cloneHero);
                }
            }
        }
    }
}

