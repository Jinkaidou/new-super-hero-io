using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;

namespace foodResources
{
    [Serializable]
    public class FoodCollection
    {
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public int typeFood;
        public List<FoodData> scaleFood = new List<FoodData>();
    }
}