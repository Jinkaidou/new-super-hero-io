using System;
using Sirenix.OdinInspector;
namespace foodResources
{
    [Serializable]
    public class FoodData
    {
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public int id;
        public float scaleFoodinMap;
        public string name;
        public int point;
    }
}