using System.Collections.Generic;
using UnityEngine;
using foodResources;
[CreateAssetMenu(menuName = "Data/food", fileName = "FoodData")]
[SerializeField]
public class FoodTypeCollection : ScriptableObject
{
    public float percentTake;
    public List<FoodCollection> typeFoodCollection = new List<FoodCollection>();
}
