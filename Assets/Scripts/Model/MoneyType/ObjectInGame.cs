namespace Model.ObjectInGame
{
    public enum ObjectInGame
    {
        Hero = 0,
        Food = 1,
        Gold = 2,
        PowerUp = 3,
    }
}
