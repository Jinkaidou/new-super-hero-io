using System.Collections.Generic;
using UnityEngine;
namespace Model.DataName
{
    [CreateAssetMenu(menuName = "Data/name", fileName = "NameData")]
    [SerializeField]
    public class NameCollection : ScriptableObject
    {
        public List<NameData> listItemData = new List<NameData>();
    }
}