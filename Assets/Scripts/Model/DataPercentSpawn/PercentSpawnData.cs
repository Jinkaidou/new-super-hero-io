using System;
using Sirenix.OdinInspector;
using System.Collections.Generic;
namespace PercentSpawn
{
    [Serializable]
    public class PercentSpawnData
    {
        public string typeObjetSpawn;
        public int typeId;
        public List<float> percentSpawn = new List<float>();
    }

}