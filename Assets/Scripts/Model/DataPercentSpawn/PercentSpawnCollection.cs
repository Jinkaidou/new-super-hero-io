using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PercentSpawn;

[CreateAssetMenu(menuName = "Data/PercentSpawn", fileName = "PercentSpawnData")]
[SerializeField]
public class PercentSpawnCollection : ScriptableObject
{
    public List<PercentSpawnData> listItemData = new List<PercentSpawnData>();

}
