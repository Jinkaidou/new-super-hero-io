using System;

namespace SpawnData
{
    [Serializable]
    public class NumberSpawnData
    {
        public string typeSpawn;
        public int idType;
        public float numberSpawn;
    }
}