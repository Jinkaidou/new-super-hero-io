using System.Collections.Generic;
using UnityEngine;
using SpawnData;

[CreateAssetMenu(menuName = "Data/SpawnData", fileName = "SpawnData")]
[SerializeField]
public class NumberSpawnCollection : ScriptableObject
{
    public List<NumberSpawnData> SpawnItemData = new List<NumberSpawnData>();

    public float SpawnNumberInGame(int MoneyType)
    {
        for (int i = 0; i < SpawnItemData.Count; i ++)
        {
            var numberSpawnData = SpawnItemData[i];
            var idType = numberSpawnData.idType;
            if (idType == MoneyType)
            {
                return numberSpawnData.numberSpawn;
            }
        }
        return 0;
    }
}
