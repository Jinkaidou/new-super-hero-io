using System;
using Base.Core.GameResources;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace Model.DataHero
{
    [Serializable]
    public class HeroData
    {
        [FormerlySerializedAs("skinPtype")] [GUIColor(1f, 0.8f, 0.8f)]
        public SkinType skinType;
        public Color Color;
    }
}

