using System;
using Sirenix.OdinInspector;

namespace Model.DataHero
{
    [Serializable]
    public class HeroScaleData
    {
        [GUIColor(1f, 0.8f, 0.8f)]
        public int dataLevel;
        public float scale;
        public float sizeCamera;
        public int numberFood;
    }
}