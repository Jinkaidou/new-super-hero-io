﻿using System;
using Base.Core.GameResources;
using Sirenix.OdinInspector;

namespace Model.DataHero
{
    [Serializable]
    public class SkinData
    {
        [GUIColor(0.3f, 0.8f, 0.8f)]
        public SkinType skinName;

        public int numberOfAds;
    }
}