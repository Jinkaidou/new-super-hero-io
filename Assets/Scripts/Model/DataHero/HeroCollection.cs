using System.Collections.Generic;
using Base.Core.GameResources;
using UI.SkinPopup;
using UnityEngine;

namespace Model.DataHero
{
    [CreateAssetMenu(menuName = "Data/hero", fileName = "HeroData")]
    public class HeroCollection : ScriptableObject
    {
        public List<HeroData> listHeroData = new List<HeroData>();
        public List<HeroScaleData> dataLevel = new List<HeroScaleData>();

        [Header("Skins")] 
        public int baseCoinSkin = 30000;
        public List<SkinData> normalSkins = new List<SkinData>();
        public List<SkinData> goldSkins = new List<SkinData>();
        public List<SkinData> premiumSkins = new List<SkinData>();

        public List<SkinData> GetSkinCollection(SkinType skinType, out SkinUnlockType skinUnlockType)
        {
            foreach (var skin in normalSkins)
            {
                if (skin.skinName == skinType)
                {
                    skinUnlockType = SkinUnlockType.Normal;
                    return normalSkins;
                }
            }

            foreach (var skin in goldSkins)
            {
                if (skin.skinName == skinType)
                {
                    skinUnlockType = SkinUnlockType.Gold;
                    return goldSkins;
                }
            }

            foreach (var skin in premiumSkins)
            {
                if (skin.skinName == skinType)
                {
                    skinUnlockType = SkinUnlockType.Premium;
                    return premiumSkins;
                }
            }

            skinUnlockType = SkinUnlockType.Normal;
            return normalSkins;
        }

        public bool IsGoldSkin(SkinType skinType)
        {
            foreach (var skin in goldSkins)
            {
                if (skin.skinName == skinType)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsNormalSkin(SkinType skinType)
        {
            foreach (var skin in normalSkins)
            {
                if (skin.skinName == skinType)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsPremiumSkin(SkinType skinType)
        {
            foreach (var skin in premiumSkins)
            {
                if (skin.skinName == skinType)
                {
                    return true;
                }
            }

            return false;
        }

        public HeroScaleData GetHeroScaleData(int level)
        {
            foreach (var heroScaleData in dataLevel)
            {
                if (heroScaleData.dataLevel == level)
                {
                    return heroScaleData;
                }
            }

            return dataLevel[0];
        }
    }
}
