using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Model.DataUpgrade
{
    [CreateAssetMenu(menuName = "Data/upgrade", fileName = "UpgradeData")]
    public class UpgradeCollection : ScriptableObject
    {
        public List<UpgradeData> listLevelValueRangeAttack = new List<UpgradeData>();
        public List<UpgradeData> listLevelSpeedRange = new List<UpgradeData>();
        public List<UpgradeData> listLevelSpeedAttack = new List<UpgradeData>();

        public float GetValueRangeAttack(int level)
        {
            if (level != 0)
            {
                var count = level - 1;
                for (int i = 0; i < count; i++)
                {
                    var defaultValue = listLevelValueRangeAttack[0].upgradeValue;
                    var numberUp = defaultValue * 2;
                    var upValue = numberUp / 100;
                    return upValue;
                }
            }
            return 0;
        }

        public float GetValueSpeed(int level)
        {
            if (level != 0)
            {
                var count = level - 1;
                for (int i = 0; i < count; i++)
                {
                    var defaultValue = listLevelSpeedRange[0].upgradeValue;
                    var numberUp = defaultValue * 20;
                    var value = defaultValue + numberUp / 100;
                    return value;
                }
            }
            return listLevelSpeedRange[0].upgradeValue;
        }

        public float GetValueSpeedAttack(int level)
        {
            if (level != 0)
            {
                var count = level - 1;
                for (int i = 0; i < count; i++)
                {
                    var defaultValue = listLevelSpeedAttack[0].upgradeValue;
                    var numberUp = defaultValue * 20;
                    var value = defaultValue - numberUp / 100;
                    return value;
                }
            }
            return listLevelSpeedAttack[0].upgradeValue;
        }
    }
}