using System;
using Sirenix.OdinInspector;
using Base.Core.GameResources;
namespace Model.DataUpgrade
{
    [Serializable]
    public class UpgradeData
    {
        public int levelStat;
        public float upgradeValue;
    }
}

