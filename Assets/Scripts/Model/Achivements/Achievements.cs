using UnityEngine;

namespace Model.Achivements
{
    public enum Achievements 
    {
       FirstBlood = 1,
       DoubleKill = 2,
       TripleKill = 3,
       QuadraKill = 4,
       PenTaKill = 5,
       GodLike = 6,
    }
}
