using System.Collections.Generic;
using UnityEngine;
using ItemResources;
[CreateAssetMenu(menuName = "Data/item", fileName = "ItemData")]
[SerializeField]
public class ItemCollection : ScriptableObject
{
    public List<ItemData> listItemData = new List<ItemData>();

    public float TimeCoolDown(string tag)
    {

        for (int i= 0; i< listItemData.Count; i++)
        {
            var tagItem = listItemData[i].itemTag;
            if(tagItem == tag)
            {
                return listItemData[i].timeCoolDown;
            }
        }
        return 0;
    }

    public float ValueEff(string tag)
    {
        for (int i = 0; i < listItemData.Count; i++)
        {
            var tagItem = listItemData[i].itemTag;
            if (tagItem == tag)
            {
                return listItemData[i].valueItemEffect;
            }
        }
        return 0;
    }

    public float ValueSideKick(string tag)
    {
        for (int i = 0; i < listItemData.Count; i++)
        {
            var tagItem = listItemData[i].itemTag;
            if (tagItem == tag)
            {
                return listItemData[i].SideKichEffect;
            }
        }
        return 0;
    }
}
