using System;
using Sirenix.OdinInspector;

namespace ItemResources
{
    [Serializable]
    public class ItemData
    {
        public int idItem;
        public float timeCoolDown;
        public string itemPath;
        public string itemTag;
        public float valueItemEffect;
        public float SideKichEffect;
    }
}