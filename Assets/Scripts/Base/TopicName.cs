public struct TopicName
{
    public const string BOTDESTROY = "BotDestroy";
    public const string WAYPOINTDESTROY = "WayPointDestroy";
    public const string EATFOOD = "EatFood";
}
