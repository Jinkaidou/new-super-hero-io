﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static partial class Extensions
{
    public static int Replace<T>(this IList<T> source, T oldValue, T newValue)
    {
        if (source == null)
            throw new ArgumentNullException("source");

        var index = source.IndexOf(oldValue);
        if (index != -1)
            source[index] = newValue;
        return index;
    }

    public static void ReplaceAll<T>(this IList<T> source, T oldValue, T newValue)
    {
        if (source == null)
            throw new ArgumentNullException("source");

        int index = -1;
        do
        {
            index = source.IndexOf(oldValue);
            if (index != -1)
                source[index] = newValue;
        } while (index != -1);
    }


    public static IEnumerable<T> Replace<T>(this IEnumerable<T> source, T oldValue, T newValue)
    {
        if (source == null)
            throw new ArgumentNullException("source");

        return source.Select(x => EqualityComparer<T>.Default.Equals(x, oldValue) ? newValue : x);
    }

    public static Coroutine StartDelayMethod(this MonoBehaviour mono, float time, Action callback)
    {
        if (mono.gameObject.activeInHierarchy)
        {
            return mono.StartCoroutine(Delay(time, callback));
        }

        return null;
    }

    public static Coroutine StartDelayMethodByFrame(this MonoBehaviour mono, int frameCount, Action callback)
    {
        if (mono.gameObject.activeInHierarchy)
        {
            return mono.StartCoroutine(DelayByFrame(frameCount, callback));
        }

        return null;
    }

    static IEnumerator Delay(float time, Action callback)
    {
        yield return new WaitForSeconds(time);
        if (callback != null)
        {
            callback.Invoke();
        }
        else
        {
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation only print log in UnityEditor

        }
    }

    static IEnumerator DelayByFrame(int frameCount, Action callback)
    {
        for (int i = 0; i < frameCount; i++)
        {
            yield return new WaitForEndOfFrame();
        }

        if (callback != null)
        {
            callback.Invoke();
        }
        else
        {
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation only print log in UnityEditor

        }
    }

    public static Coroutine StartDelayMethodRealTime(this MonoBehaviour mono, float time, Action callback)
    {
        return mono.StartCoroutine(DelayRealTime(time, callback));
    }

    static IEnumerator DelayRealTime(float time, Action callback)
    {
        yield return new WaitForSecondsRealtime(time);
        if (callback != null)
        {
            callback.Invoke();
        }
        else
        {
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation only print log in UnityEditor

        }
    }


    public static TValue GetOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
    {
        if (dictionary.TryGetValue(key, out var value))
        {
            return value;
        }
        else
        {
            return defaultValue;
        }
    }



    public static int CompareVersion(this string versionA, string versionB)
    {
        var version1 = new Version(versionA);
        var version2 = new Version(versionB);
        return version1.CompareTo(version2);
    }


}