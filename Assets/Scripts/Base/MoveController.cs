using UnityEngine;

public class MoveController : MonoBehaviour
{
    public float speed;
    public bool canMoveLimitNow;
    public bool canMoveAttackNow;
    public float speedItem;
    private void Start()
    {
        canMoveLimitNow = true;
        canMoveAttackNow = true;
        speedItem = 1;
    }
    public virtual void Move(Vector3 direction)
    {
        if (!canMoveAttackNow)
        {
            this.transform.position += Vector3.zero;
        }
        else if (canMoveLimitNow && canMoveAttackNow)
        {
            this.transform.position += direction * Time.deltaTime * speed  * speedItem;
        }
    }
}