﻿using System.Collections.Generic;
using Base.Core.GameResources;
using Controller.LoadData;
using UI.FlyObject;
using UI.HUD;
using UI.RewardPopup;
using UI.Toast;
using UnityEngine;

namespace Base.Core.Popup
{
    public class PopupController : SingletonMono<PopupController>
    {
        [SerializeField] private ModuleUiToast toastModule;
        private ModuleUiHud _moduleUiHud;
        [SerializeField] private List<PopupBase> _currrentPanelOpen = new List<PopupBase>();
        private Dictionary<PopupType, List<PopupBase>> _container = new Dictionary<PopupType, List<PopupBase>>();
        private RectTransform _canvasRect;
        private List<ModuleUiToast> _listToasts = new List<ModuleUiToast>();
        private List<ModuleUiMoneyFly> _listGoldFlies = new List<ModuleUiMoneyFly>();

        #region CallMethod

        public void OpenNotification(string content)
        {
            ModuleUiToast toastNeed = null;
            foreach (var toast in _listToasts)
            {
                if (!toast.gameObject.activeInHierarchy)
                {
                    toastNeed = toast;
                    break;
                }
            }

            if (toastNeed == null)
            {
                var toastClone = Instantiate(toastModule, transform, false);
                _listToasts.Add(toastClone);
                toastNeed = toastClone;
            }

            toastNeed.gameObject.SetActive(true);
            toastNeed.InitData(content);
        }

        public void ShowEarnReward(MoneyType moneyType, int number, Vector3 startPos)
        {
            ModuleUiMoneyFly moduleNeed = null;
            foreach (var moduleUiGoldFly in _listGoldFlies)
            {
                if (!moduleUiGoldFly.gameObject.activeInHierarchy)
                {
                    moduleNeed = moduleUiGoldFly;
                    break;
                }
            }

            if (moduleNeed == null)
            {
                var moduleSample = LoadResourceController.Instance.LoadPanel("MoneyRewardFly");
                var moduleClone= Instantiate(moduleSample, transform, false);
                if (moduleClone == null)
                    return;

                moduleNeed = moduleClone.GetComponent<ModuleUiMoneyFly>();
                _listGoldFlies.Add(moduleNeed);
            }

            moduleNeed.gameObject.SetActive(true);
            Vector2 targetPos;
            if (moneyType == MoneyType.Gold)
            {
                targetPos = GetHud().GoldPosition;
            }
            else
            {
                targetPos = GetHud().LifePosition;
            }
            
            moduleNeed.InitData(moneyType, number, startPos, targetPos);
        }

        public ModuleUiHud GetHud()
        {
            if (_moduleUiHud is null)
            {
                _moduleUiHud = (ModuleUiHud) GetPanelInCache(PopupType.Hud);
            }

            return _moduleUiHud;
        }

        public void ShowHud()
        {
            if (_moduleUiHud is null)
            {
                _moduleUiHud = (ModuleUiHud) GetPanelInCache(PopupType.Hud);
            }

            _moduleUiHud.gameObject.SetActive(true);
            _moduleUiHud.transform.SetAsLastSibling();
        }

        public void HideHud()
        {
            if (_moduleUiHud != null)
            {
                _moduleUiHud.gameObject.SetActive(false);
            }
        }

        public PopupBase OpenPopupAndKeepParent(PopupType panelType)
        {
            var panel = GetPanelInCache(panelType);
            OpenPopUp(panel);
            return panel;
        }

        public PopupBase OpenPopupAndKeepParentWithHud(PopupType panelType)
        {
            var panel = GetPanelInCache(panelType);
            OpenPopUp(panel);
            ShowHud();
            return panel;
        }

        public PopupBase OpenPopupAndCloseParent(PopupType panelType)
        {
            var currentPanel = _currrentPanelOpen[_currrentPanelOpen.Count - 1];
            if (currentPanel.popupType == panelType)
            {
                return currentPanel;
            }

            currentPanel.ClosePanelOnly();
            var nextPanel = GetPanelInCache(panelType);
            OpenPopUp(nextPanel);
            return nextPanel;
        }

        public PopupBase OpenPopupAndCloseParentWithHud(PopupType panelType)
        {
            if (_currrentPanelOpen.Count <= 0)
                return null;

            var currentPanel = _currrentPanelOpen[_currrentPanelOpen.Count - 1];
            if (currentPanel.popupType == panelType)
            {
                return currentPanel;
            }

            currentPanel.ClosePanelOnly();
            var nextPanel = GetPanelInCache(panelType);
            OpenPopUp(nextPanel);
            ShowHud();
            return nextPanel;
        }

        public PopupBase CloseCurrentPopupAndOpenParent()
        {
            var panel = _currrentPanelOpen[_currrentPanelOpen.Count - 1];
            ClosePopUp(panel);

            if (_currrentPanelOpen.Count > 0)
            {
                var parent = _currrentPanelOpen[_currrentPanelOpen.Count - 1];
                parent.Open();
            }

            return panel;
        }

        public void ShowRewardPopup(Resource resource)
        {
            List<Resource> result = new List<Resource>()
            {
                resource
            };

            this.StartDelayMethod(0.2f, () =>
            {
                var moduleUiRewardPopup = (ModuleUiRewardPopup) OpenPopupAndKeepParent(PopupType.RewardPopup);
                moduleUiRewardPopup.InitData(result);
            });
        }

        public void ShowRewardPopup(List<Resource> resources)
        {
            this.StartDelayMethod(0.2f, () =>
            {
                var moduleUiRewardPopup = (ModuleUiRewardPopup) OpenPopupAndKeepParent(PopupType.RewardPopup);
                moduleUiRewardPopup.InitData(resources);
            });
        }

        #endregion

        #region PrivateProcessMethod

        private void OpenPopUp(PopupBase panel)
        {
            _currrentPanelOpen.Add(panel);
            panel.Open();
        }

        private void ClosePopUp(PopupBase panel)
        {
            _currrentPanelOpen.RemoveAt(_currrentPanelOpen.Count - 1);
            panel.ClosePanelOnly();
        }

        private PopupBase GetPanelInCache(PopupType panelType)
        {
            if (panelType is PopupType.None)
            {
                CloseAllPanel();
                return null;
            }

            if (_container.ContainsKey(panelType))
            {
                var allBase = _container[panelType];
                foreach (var panel in allBase)
                {
                    if (panel.gameObject.activeInHierarchy is false)
                    {
                        return panel;
                    }
                }

                var panelClone = LoadFromResources(panelType);
                allBase.Add(panelClone);
                return panelClone;
            }
            else
            {
                var panelClone = LoadFromResources(panelType);
                _container[panelType] = new List<PopupBase> {panelClone};
                return panelClone;
            }
        }

        private PopupBase LoadFromResources(PopupType type)
        {
            var obj = LoadResourceController.Instance.LoadPanel(type);
            if (obj == null) return null;

            var panelClone = Instantiate(obj, transform, false);
            return panelClone;

        }

        public void CloseAllPanel()
        {
            foreach (var panel in _currrentPanelOpen)
            {
                panel.ClosePanelOnly();
            }

            _currrentPanelOpen.Clear();
        }

        public RectTransform GetRectCanvas()
        {
            if (_canvasRect == null)
            {
                _canvasRect = GetComponent<RectTransform>();
            }

            return _canvasRect;
        }

        #endregion

        #region Check Method

        public bool IsHavePopUpOpen(PopupType panelType)
        {
            if (_container.ContainsKey(panelType) is false)
            {
                return false;
            }
            else
            {
                var listPanels = _container[panelType];
                foreach (var panel in listPanels)
                {
                    if (panel.gameObject.activeInHierarchy)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsHaveAnyPopUpOpening()
        {
            return _currrentPanelOpen.Count > 0;
        }

        #endregion
    }
}