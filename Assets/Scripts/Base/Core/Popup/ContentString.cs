﻿namespace Base.Core.Popup
{
    public class ContentString
    {
        public static string NotEnoughMoney = "Not enough money";
        public static string WatchAdFail = "Watch ads is fail for some reason!";
        public static string AdsNotHave = "Not have ads to watch!!";

        public static string CanNotReceiveGift = "Can not receive gift!";
        public static string ComingSoon = "Coming Soon!";

        public static string NameInvalid = "Name Invalid";
        public static string NameChangeSuccess = "Change Name Successfully";

    }
}