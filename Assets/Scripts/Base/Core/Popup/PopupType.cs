﻿namespace Base.Core.Popup
{
    public enum PopupType
    {
        None,
        Hud,
        MainMenu,
        ShopPopup,
        SpinPopup,
        SpinRewardPopup,
        DailyCheckInPopup,
        SettingPopup,
        RevivePopup,
        SkinPopup,
        StatUpgradePopup,
        RewardPopup,
        EndGameRewardPopup,
        EndGameNewSkinUnlock,
    }
}