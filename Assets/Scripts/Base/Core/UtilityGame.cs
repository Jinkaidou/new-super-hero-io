﻿using System;
using Base.Core.Sound;
using UnityEngine;

namespace Base.Core
{
    public class UtilityGame : MonoBehaviour
    {
        public static void PauseGame()
        {
            Time.timeScale = 0;
            SoundManager.Instance.PauseMusic();
        }

        public static void ResumeGame()
        {
            Time.timeScale = 1;
            SoundManager.Instance.ResumeMusic();
        }

        public static long GetCurrentTime()
        {
            var now = DateTimeOffset.Now.ToUnixTimeSeconds();
            return now;
        }
    }
}