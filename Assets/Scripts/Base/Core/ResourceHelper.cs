﻿using System.Collections.Generic;
using Base.Core.GameResources;
using DataAccount;
using UnityEngine;

namespace Base.Core
{
    public class ResourceHelper : MonoBehaviour
    {
        public static void AddResource(Resource resource)
        {
            switch (resource.resourceType)
            {
                case ResourceType.Money:
                    AddMoney((MoneyType) resource.id, resource.value);
                    break;
                case ResourceType.Skin:
                    AddSkin((SkinType) resource.id);
                    break;
                case ResourceType.Stat:
                    AddStat((StatType) resource.id, resource.value);
                    break;
            }
        }

        public static void AddResources(List<Resource> resources)
        {
            foreach (var resource in resources)
            {
                AddResource(resource);
            }
        }

        private static void AddMoney(MoneyType moneyType, int value)
        {
            DataAccountPlayer.PlayerMoney.SetMoney(true, moneyType, value);
        }

        private static void AddSkin(SkinType skinType)
        {
            DataAccountPlayer.PlayerSkins.AddOwnedSkin(skinType);
        }

        private static void AddStat(StatType statType, int value)
        {
            DataAccountPlayer.PlayerStats.OnStatUpgrade(statType, value);
        }
    }
}