﻿namespace Base.Core.GameResources
{
    public enum EarnSourceType
    {
        Campaign,
        WordCollect,
        PowerUpLife,
        X2WinMap,
        Shop,
        Spin,
    }
}