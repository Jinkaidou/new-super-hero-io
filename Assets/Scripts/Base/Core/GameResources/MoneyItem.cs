﻿using System;

namespace Base.Core.GameResources
{
    [Serializable]
    public class MoneyItem : Resource
    {
        public MoneyType moneyType;

        public MoneyItem(MoneyType moneyType, int value)
        {
            resourceType = ResourceType.Money;
            this.moneyType = moneyType;
            id = (int) moneyType;
            this.value = value;
        }
    }
}