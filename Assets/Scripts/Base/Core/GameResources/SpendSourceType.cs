﻿namespace Base.Core.GameResources
{
    public enum SpendSourceType
    {
        SkinUnlockRandom,
        CampaignRevive,
        WordCollectRevive,
    }
}