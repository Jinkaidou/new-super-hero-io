﻿namespace Base.Core.GameResources
{
    public enum ResourceType
    {
        Money = 0,
        Skin = 1,
        Stat = 2,
    }
}