﻿namespace Base.Core.GameResources
{
    public enum SkinType
    {
        none = -1,
        bat = 1,
        america = 2,
        black = 3,
        brazil = 4,
        captain = 5,
        china = 6,
        cycplops = 7,
        deadpool = 8,
        flash = 9,
        france = 10,
        hellboy = 11,
        hulk = 12,
        iron = 13,
        japan = 14,
        joker = 15,
        russia = 16,
        spider = 17,
        strange = 18,
        thanos = 19,
        thor = 20,
        venom = 21,
        vietnam = 22,
        wolverine = 23,
        blue = 24,
        red = 25,
    }
}