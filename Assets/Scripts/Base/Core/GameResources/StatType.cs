﻿namespace Base.Core.GameResources
{
    public enum StatType
    {
        AttackSpeed = 0,
        AttackRange = 1,
        MoveSpeed = 2
    }
}