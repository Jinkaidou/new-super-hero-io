﻿using System;

namespace Base.Core.GameResources
{
    [Serializable]
    public class Resource
    {
        public ResourceType resourceType;
        public int id;
        public int value;

        public Resource()
        {
        }
        
        public Resource(ResourceType resourceType, int id, int value)
        {
            this.resourceType = resourceType;
            this.id = id;
            this.value = value;
        }
    }
}