﻿namespace Base.Core.GameResources
{
    public enum AdRewardFor
    {
        WatchAdsTrySkin,
        WatchAdsGetSkin,
        WatchAdsGetPowerUp,
        WatchAdsGetGift,
        WatchAdX2WinMap,
        WatchAdsGetPet,
        WatchAdsForShop,
        WatchAdsForSpin,
        WatchAdsForSpinX2,
        WatchAdsForEarnChestWordCollect,
        WatchAdsForChestWordCollectX2,
    }
}