﻿using System.Collections.Generic;

namespace Base.Core.Analytics
{
    public class TrackingGamePlayButton
    {
        #region Const

        private const string AttackCount = "attack_count";
        private const string JumpCount = "jump_count";
        private const string FireCount = "fire_count";

        #endregion

        private int _attackCount;
        private int _jumpCount;
        private int _fireCount;

        public TrackingGamePlayButton()
        {
            _attackCount = 0;
            _jumpCount = 0;
            _fireCount = 0;
        }

        public void ParamUpdate(TrackingGamePlayButtonParam param, int value = 0)
        {
            switch (param)
            {
                case TrackingGamePlayButtonParam.AttackCount:
                    _attackCount += value;
                    break;
                case TrackingGamePlayButtonParam.JumpCount:
                    _jumpCount += value;
                    break;
                case TrackingGamePlayButtonParam.FireCount:
                    _fireCount += value;
                    break;
                default:
                    break;
            }
        }

        public Dictionary<string, long> GetTrackingParam()
        {
            var trackingParams = new Dictionary<string, long>();
            trackingParams.Add(AttackCount, _attackCount);
            trackingParams.Add(JumpCount, _jumpCount);
            trackingParams.Add(FireCount, _fireCount);
            return trackingParams;
        }
    }

    public enum TrackingGamePlayButtonParam
    {
        AttackCount,
        JumpCount,
        FireCount,
    }
}