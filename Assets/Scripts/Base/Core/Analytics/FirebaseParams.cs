﻿namespace Base.Core.Analytics
{
    public class FirebaseParams
    {
        #region Params

        //Main Menu
        public const string BUTTON_PLAY = "M_01";
        public const string BUTTON_SKIN = "M_02";
        public const string BUTTON_UPGRADE = "M_03";
        public const string BUTTON_NAME = "M_04";
        public const string BUTTON_REMOVE_ADS = "M_05";
        //GamePlay
        public const string RANKING_GAMEPLAY = "G_R_01";
        public const string SPEED_UP = "G_P_02";
        public const string BIG_SIZE = "G_P_03";
        public const string SHIELD = "G_P_04";
        public const string TELEPORT = "G_P_05";
        public const string FADE = "G_P_06";
        public const string DIE_TIME = "G_D_07";
        public const string KILL = "G_K_08k";
        public const string QUITDONE = "G_Q_10";
        

        //Skin
        public const string GOLD_RANDOM = "S_B_02";
        public const string WATCH_AD = "S_B_03";
        
        //ADS
        public const string QUIT_ADS = "A_01";
        public const string ENTER_ADS = "A_03";
        public const string DONE_ADS = "A_04";
        
        //TUT
        public const string EndGameTakeGold = "T_02";

        public const string LEVEL_START = "level_start";
        public const string LEVEL_COMPLETE = "level_complete";
        public const string LEVEL_REPLAY = "level_replay";

        public const string WORD_COLLECT_START = "word_collect_start";
        public const string WORD_COLLECT_COMPLETE = "word_collect_complete";

        public const string POWER_UP_COLLECT = "power_up";

        public const string GAMEPLAY_WIN_BUTTON = "gameplay_win_button";
        public const string GAMEPLAY_LOSE_BUTTON = "gameplay_lose_button";

        public const string BUTTON_MAIN_MENU = "button_main_menu";

        public const string EARN_VIRTUAL_CURRENCY = "earn_virtual_currency";
        public const string SPEND_VIRTUAL_CURRENCY = "spend_virtual_currency";

        public const string ADS_REWARD_LOAD_FAIL = "ads_reward_load_fail";
        public const string ADS_REWARD_CLICK = "ads_reward_click";
        public const string ADS_REWARD_SHOW = "ads_reward_show";
        public const string ADS_REWARD_COMPLETE = "ads_reward_complete";
        public const string ADS_REWARD_FAIL = "ads_reward_fail";

        public const string AD_INTER_LOAD_FAIL = "ad_inter_load_fail";
        public const string AD_INTER_LOAD = "ad_inter_load";
        public const string AD_INTER_SHOW = "ad_inter_show";
        public const string AD_INTER_CLICK = "ad_inter_click";
        public const string AD_INTER_FAIL = "ad_inter_fail";

        #endregion
    }
}