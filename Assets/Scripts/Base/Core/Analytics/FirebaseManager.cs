using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Base.Core.Ads;
using Base.Core.GameResources;
using Firebase;
using Firebase.Analytics;
using Firebase.Extensions;
using Firebase.RemoteConfig;
using GameController;
using UnityEngine;
using UnityEngine.Events;

namespace Base.Core.Analytics
{
    public class FirebaseManager : SingletonMonoDontDestroy<FirebaseManager>
    {
        public FirebaseManager(string className) : base(className)
        {
        }

        private bool _firebaseInitialized = false;

        private UnityAction _loadConfigSuccess;

        #region Base Code

        private void Start()
        {
#if UNITY_EDITOR
            return;
#endif

#if !USE_RELEASE
            Debug.LogError("NOT USE RELEASE => NOT TRACKING....");
            return;
#endif
            Debug.LogError("USE RELEASE => USE TRACKING...");
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
            {
                if (task.Result == DependencyStatus.Available)
                {
                    InitializeFirebase();

                }
                else
                {
                    Debug.LogError(
                        "Error Could not resolve all Firebase dependencies: " + task.Result);
                }
            });
        }

        void InitializeFirebase()
        {
            DebugLog("Enabling data collection.");
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            _firebaseInitialized = true;

            Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
            Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
            DebugLog("Firebase Messaging Initialized");
            InitRemoteConfig();
            DebugLog("Firebase RemoteConfig Initialized");
            //// This will display the prompt to request permission to receive
            //// notifications if the prompt has not already been displayed before. (If
            //// the user already responded to the prompt, thier decision is cached by
            //// the OS and can be changed in the OS settings).
            //Firebase.Messaging.FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(
            //  task => {
            //      LogTaskCompletion(task, "RequestPermissionAsync");
            //  }
            //);

        }

        private void InitRemoteConfig()
        {

#if UNITY_EDITOR
            var configSettings = new ConfigSettings
            {
                MinimumFetchInternalInMilliseconds = 0
            };
            FirebaseRemoteConfig.DefaultInstance.SetConfigSettingsAsync(configSettings).ContinueWithOnMainThread(task1 =>
            {

#endif
                Debug.Log("start Init RemoteConfig");
                Dictionary<string, object> defaults =
                    new Dictionary<string, object>();
                //            defaults.Add("TIME_NO_INTERSTITIAL_AFTER_SHOW_REWARDED", 30);
                //            defaults.Add("MIN_LEVEL_SHOW_INTERSTITIAL", 2);
                //            defaults.Add("NUMBER_LEVEL_PER_SHOW_INTERSTITIAL", 2);

                FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults)
                    .ContinueWithOnMainThread(task => { FetchDataAsync(); });

#if UNITY_EDITOR

            });
#endif

        }

        private void FetchDataAsync()
        {
            Debug.Log("Set Default finish");
            System.Threading.Tasks.Task fetchTask =
                Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.FetchAsync(
                    TimeSpan.Zero).ContinueWithOnMainThread(FetchComplete);
            FirebaseRemoteConfig.DefaultInstance.FetchAndActivateAsync().ContinueWithOnMainThread(task =>
            {

            });

        }

        void FetchComplete(Task fetchTask)
        {
            if (fetchTask.IsCanceled)
            {
                Debug.Log("Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                Debug.Log("Fetch completed successfully!");
            }

            var info = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.Info;
            switch (info.LastFetchStatus)
            {
                case Firebase.RemoteConfig.LastFetchStatus.Success:
                    Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.ActivateAsync().ContinueWithOnMainThread(task =>
                    {
                        Debug.Log($"Remote data loaded and ready (last fetch time {info.FetchTime}).");
                        var values = FirebaseRemoteConfig.DefaultInstance.AllValues;
                        if (_loadConfigSuccess != null)
                        {
                            _loadConfigSuccess();
                        }
                    });

                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case Firebase.RemoteConfig.FetchFailureReason.Error:
                            Debug.Log("Fetch failed for unknown reason");
                            break;
                        case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Pending:
                    Debug.Log("Latest Fetch call still pending.");
                    break;
            }
        }

        //LOG

        public void LogEvent(string eventName)
        {
#if UNITY_EDITOR
            return;
#endif
            
#if !USE_RELEASE
            return;
#endif
            if (_firebaseInitialized)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName);
            }
        }

        private void LogEvent(string eventName, Parameter[] parameters)
        {
#if UNITY_EDITOR
            return;
#endif
            
#if !USE_RELEASE
            return;
#endif
            if (_firebaseInitialized)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, parameters);
            }
        }

        private void LogEvent(string eventName, string parameterName, int parameterValue)
        {
#if UNITY_EDITOR
            return;
#endif
            
#if !USE_RELEASE
            return;
#endif
            if (_firebaseInitialized)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, parameterName, parameterValue);
            }
        }

        private void LogEvent(string eventName, string parameterName, string parameterValue)
        {
#if UNITY_EDITOR
            return;
#endif
            
#if !USE_RELEASE
            return;
#endif
            if (_firebaseInitialized)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, parameterName, parameterValue);
            }
        }

        private void LogEvent(string eventName, string parameterName, long parameterValue)
        {
#if UNITY_EDITOR
            return;
#endif
            
#if !USE_RELEASE
            return;
#endif
            if (_firebaseInitialized)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, parameterName, parameterValue);
            }
        }

        #endregion

        #region Log Events

        public void LogAdsRewardFor(AdRewardFor rewardFor)
        {
            var parameters = new Parameter[1];
            parameters[0] = new Parameter("adRewardFor", rewardFor.ToString());
            LogEvent("ads_reward_for", parameters);
        }

        public void LogLevelAchieve(int level)
        {
            LogEvent($"checkpoint_{Helpers.GetLevelStringFormat(level)}");
        }

        public void LogLevelReplay(int level)
        {
            LogEvent(FirebaseParams.LEVEL_REPLAY, "level", Helpers.GetLevelStringFormat(level));
        }

        public void LogRankingPlayer(string name)
        {
            var listRanking = Ranking.Instance.topFivePlayer;
            if (listRanking.ContainsKey(name))
            {
                var rankingUser = listRanking[name];
                for (int i = 0; i < Ranking.Instance.rankingPoint.Count; i++)
                {
                    if (rankingUser == Ranking.Instance.rankingPoint[i])
                    {
                        LogEvent(FirebaseParams.RANKING_GAMEPLAY, "ranking_use" ,i);
                    }
                }
            }
        }
        
        
        public void LogKillPerRound(int kill)
        {
            LogEvent(FirebaseParams.KILL, "kill", Helpers.GetLevelStringFormat(kill));
        }

        public void LogEventDieTime(long time)
        {
            LogEvent(FirebaseParams.DIE_TIME, "die_time", time);
        }

        public void LogSkinUsing(SkinType skin)
        {
            var parram = "0" + (int)skin;
            LogEvent("S_"+parram);
        }
        
        public void LogUpgradeStat(StatType stat)
        {
            var parram = "0" + (int)stat;
            LogEvent("U_" + parram);
        }

        public void LogLevelStart(int level)
        {
            LogEvent(FirebaseParams.LEVEL_START, "level", Helpers.GetLevelStringFormat(level));
        }

        public void LogLevelComplete(int level, bool isWin, long time)
        {
            string levelString = Helpers.GetLevelStringFormat(level);
            string winLose = isWin ? "win" : "lose";
            List<Parameter> fParamters = new List<Parameter>();
            fParamters.Add(new Parameter(levelString, winLose));
            fParamters.Add(new Parameter(levelString, time));

            LogEvent(FirebaseParams.LEVEL_COMPLETE, fParamters.ToArray());
        }

        public void LogWordCollectLevelStart(int level)
        {
            LogEvent(FirebaseParams.WORD_COLLECT_START, "level", Helpers.GetLevelStringFormat(level));
        }

        public void LogWordCollectLevelComplete(int level, bool isWin, long time)
        {
            string levelString = Helpers.GetLevelStringFormat(level);
            string winLose = isWin ? "win" : "lose";
            List<Parameter> fParamters = new List<Parameter>();
            fParamters.Add(new Parameter(levelString, winLose));
            fParamters.Add(new Parameter(levelString, time));

            LogEvent(FirebaseParams.WORD_COLLECT_COMPLETE, fParamters.ToArray());
        }

        public void LogGamePlayButton(bool isWin, TrackingGamePlayButton trackingGamePlayButton)
        {
            string eventName = isWin ? FirebaseParams.GAMEPLAY_WIN_BUTTON : FirebaseParams.GAMEPLAY_LOSE_BUTTON;
            var trackingParams = trackingGamePlayButton.GetTrackingParam();

            List<Parameter> fParamters = new List<Parameter>();
            foreach (var param in trackingParams)
            {
                fParamters.Add(new Parameter(param.Key, param.Value));
            }

            LogEvent(eventName, fParamters.ToArray());
        }

        public void LogButtonMainMenu(string functionName)
        {
            LogEvent(FirebaseParams.BUTTON_MAIN_MENU, "function", functionName);
        }

        #region Ads Inter

        public void LogLoadAdInterFail(int count)
        {
            var parameters = new Parameter[1];
            parameters[0] = new Parameter("count", count);
            if (_firebaseInitialized)
            {
                LogEvent(FirebaseParams.AD_INTER_LOAD_FAIL, parameters);
            }
        }

        public void LogShowAdInterSuccess()
        {
            LogEvent(FirebaseParams.AD_INTER_SHOW);
        }

        public void LogShowAdInterFail(string errorMsg)
        {
            var parameters = new Parameter[1];
            parameters[0] = new Parameter("ErrorMsg", errorMsg);
            LogEvent(FirebaseParams.AD_INTER_FAIL, parameters);
        }

        public void LogLoadAdInterSuccess()
        {
            LogEvent(FirebaseParams.AD_INTER_LOAD);
        }

        public void LogClickAdInter()
        {
            LogEvent(FirebaseParams.AD_INTER_CLICK);
        }

        #endregion

        #region Ads Reward

        public void LogLoadAdsRewardFail(int count)
        {
            var parameters = new Parameter[1];
            parameters[0] = new Parameter("count", count);
            if (_firebaseInitialized)
            {
                LogEvent(FirebaseParams.ADS_REWARD_LOAD_FAIL, parameters);
            }
        }

        public void LogShowRewardSuccess()
        {
            LogEvent(FirebaseParams.ADS_REWARD_SHOW);
        }

        public void LogClickAdsReward()
        {
            LogEvent(FirebaseParams.ADS_REWARD_CLICK);
        }

        public void LogAdsRewardComplete()
        {
            LogEvent(FirebaseParams.ADS_REWARD_COMPLETE);
        }

        public void LogShowRewardFail(string message)
        {
            var parameters = new Parameter[1];
            parameters[0] = new Parameter("ErrorMsg", message);
            LogEvent(FirebaseParams.ADS_REWARD_FAIL, parameters);
        }

        #endregion

        #region Currency

        public void LogEarnCurrency(MoneyType moneyType, int value, EarnSourceType earnSourceType)
        {
            var parameters = new Parameter[3];
            parameters[0] = new Parameter("virtual_currency_name", moneyType.ToString());
            parameters[1] = new Parameter("value", (long) value);
            parameters[2] = new Parameter("source", earnSourceType.ToString());
            if (_firebaseInitialized)
            {
                LogEvent(FirebaseParams.EARN_VIRTUAL_CURRENCY, parameters);
            }
        }

        public void LogSpendCurrency(MoneyType moneyType, int value, SpendSourceType spendSourceType)
        {
            var parameters = new Parameter[3];
            parameters[0] = new Parameter("virtual_currency_name", moneyType.ToString());
            parameters[1] = new Parameter("value", (long) value);
            parameters[2] = new Parameter("item_name", spendSourceType.ToString());
            if (_firebaseInitialized)
            {
                LogEvent(FirebaseParams.SPEND_VIRTUAL_CURRENCY, parameters);
            }
        }

        #endregion

        #endregion

        #region FIREBASE MESSAGE
        public virtual void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
        {
            //DebugLog("Received a new message");
            //var notification = e.Message.Notification;
            //if (notification != null)
            //{
            //    DebugLog("title: " + notification.Title);
            //    DebugLog("body: " + notification.Body);
            //    var android = notification.Android;
            //    if (android != null)
            //    {
            //        Debug.Log("android channel_id: " + android.ChannelId);
            //    }
            //}
            //if (e.Message.From.Length > 0)
            //    DebugLog("from: " + e.Message.From);
            //if (e.Message.Link != null)
            //{
            //    DebugLog("link: " + e.Message.Link.ToString());
            //}
            //if (e.Message.Data.Count > 0)
            //{
            //    DebugLog("data:");
            //    foreach (System.Collections.Generic.KeyValuePair<string, string> iter in
            //             e.Message.Data)
            //    {
            //        DebugLog("  " + iter.Key + ": " + iter.Value);
            //    }
            //}
        }

        public virtual void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
        {
            //DebugLog("Received Registration Token: " + token.Token);

        }

        //// Log the result of the specified task, returning true if the task
        //// completed successfully, false otherwise.
        //protected bool LogTaskCompletion(System.Threading.Tasks.Task task, string operation)
        //{
        //    bool complete = false;
        //    if (task.IsCanceled)
        //    {
        //        DebugLog(operation + " canceled.");
        //    }
        //    else if (task.IsFaulted)
        //    {
        //        DebugLog(operation + " encounted an error.");
        //        foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
        //        {
        //            string errorCode = "";
        //            Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
        //            if (firebaseEx != null)
        //            {
        //                errorCode = String.Format("Error.{0}: ",
        //                  ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
        //            }
        //            DebugLog(errorCode + exception.ToString());
        //        }
        //    }
        //    else if (task.IsCompleted)
        //    {
        //        DebugLog(operation + " completed");
        //        complete = true;
        //    }
        //    return complete;
        //}
        #endregion

        private void DebugLog(object s)
        {
            Debug.Log(s);
        }
    }
}