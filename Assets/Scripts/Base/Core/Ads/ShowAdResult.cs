﻿namespace Base.Core.Ads
{
    public enum ShowAdResult
    {
        Failed,
        Skipped,
        Finished,
    }
}