﻿// using System;
// using UnityEngine;
// using AppsFlyerSDK;
// using Core.Common.Analytics;
//
// namespace Core.Common.Ads
// {
//     public class AdsMaxMediation : MonoBehaviour
//     {
//         public bool internetConnected;
//         private bool _isAdsSet;
//
//         [SerializeField] private string sdkKey;
//         [SerializeField] private string bannerStringId, interstitialStringId, rewardedStringId;
//         private int _interstitialsRetryTime, _rewardedRetryTime;
//         private Action<ShowAdResult> _onComplete;
//
//         private void Start()
//         {
//             CheckInternetConnection();
//             if (internetConnected)
//             {
//                 InitData();
//                 _isAdsSet = true;
//             }
//             else
//             {
//                 _isAdsSet = false;
//             }
//         }
//
//         private void GetInternetConnection()
//         {
//             CheckInternetConnection();
//             if (_isAdsSet == false)
//             {
//                 if (internetConnected)
//                 {
//                     InitData();
//                     _isAdsSet = true;
//                 }
//             }
//         }
//
//         private void CheckInternetConnection()
//         {
//             if (Application.internetReachability == NetworkReachability.NotReachable)
//             {
//                 internetConnected = false;
//             }
//             else
//             {
//                 internetConnected = true;
//             }
//         }
//
//         private void InitData()
//         {
//             MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
//             {
//                 // AppLovin SDK is initialized, start loading ads
//                 InitializeBannerAds();
//                 InitializeInterstitialAds();
//                 InitializeRewardedAds();
//             };
//
//             MaxSdk.SetSdkKey(sdkKey);
//             //MaxSdk.SetUserId("USER_ID");
//             MaxSdk.InitializeSdk();
//         }
//
//         #region API
//
//
//
//         #endregion
//
//         #region Banner
//         
//         public void InitializeBannerAds()
//         {
//             MaxSdkCallbacks.Banner.OnAdLoadedEvent += OnBannerAdLoadedEvent;
//             MaxSdkCallbacks.Banner.OnAdLoadFailedEvent += OnBannerAdLoadFailedEvent;
//             MaxSdkCallbacks.Banner.OnAdClickedEvent += OnBannerAdClickedEvent;
//             MaxSdkCallbacks.Banner.OnAdRevenuePaidEvent += OnBannerAdRevenuePaidEvent;
//             MaxSdkCallbacks.Banner.OnAdExpandedEvent += OnBannerAdExpandedEvent;
//             MaxSdkCallbacks.Banner.OnAdCollapsedEvent += OnBannerAdCollapsedEvent;
//             
//             // Banners are automatically sized to 320×50 on phones and 728×90 on tablets
//             // You may call the utility method MaxSdkUtils.isTablet() to help with view sizing adjustments
//             MaxSdk.CreateBanner(bannerStringId, MaxSdkBase.BannerPosition.BottomCenter);
//             MaxSdk.SetBannerExtraParameter(bannerStringId, "adaptive_banner", "false");
//             // Set background or background color for banners to be fully functional
//             MaxSdk.SetBannerBackgroundColor(bannerStringId, new Color(0.07f, 0.09f, 0.14f));
//             // MaxSdk.SetBannerWidth(bannerStringId, 320f);
//         }
//
//         public void ShowBannerAds()
//         {
//             if (!_isAdsSet)
//             {
//                 GetInternetConnection();
//             }
//
//             MaxSdk.ShowBanner(bannerStringId);
//         }
//
//         public void HideBannerAds()
//         {
//             MaxSdk.HideBanner(bannerStringId);
//         }
//
//         public float GetBannerHeight()
//         {
//             return MaxSdkUtils.GetAdaptiveBannerHeight();
//         }
//
//         private void OnBannerAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//         }
//
//         private void OnBannerAdLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
//         {
//         }
//
//         private void OnBannerAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//         }
//
//         private void OnBannerAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//         }
//
//         private void OnBannerAdExpandedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//         }
//
//         private void OnBannerAdCollapsedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//         }
//
//         #endregion
//
//         #region interstitials
//
//         public void InitializeInterstitialAds()
//         {
//             // Attach callback
//             MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
//             MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialLoadFailedEvent;
//             MaxSdkCallbacks.Interstitial.OnAdDisplayedEvent += OnInterstitialDisplayedEvent;
//             MaxSdkCallbacks.Interstitial.OnAdClickedEvent += OnInterstitialClickedEvent;
//             MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialHiddenEvent;
//             MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += OnInterstitialAdFailedToDisplayEvent;
//
//             // Load the first interstitial
//             LoadInterstitial();
//         }
//
//         public bool ShowInterstitialAds(Action<ShowAdResult> resultAction)
//         {
//             AppsFlyer.sendEvent(Analytics.AFInAppEvents.INTERSTITIALS_AD_ELIGIBLE, null);
//
//             if (IsHaveInterstitialAd())
//             {
//                 UtilityGame.PauseGame();
//                 _onComplete = resultAction;
//                 MaxSdk.ShowInterstitial(interstitialStringId);
//                 return true;
//             }
//             else
//             {
//                 resultAction(ShowAdResult.Failed);
//                 return false;
//             }
//         }
//
//         private bool IsHaveInterstitialAd()
//         {
//             if (!_isAdsSet)
//             {
//                 GetInternetConnection();
//             }
//
//             return MaxSdk.IsInterstitialReady(interstitialStringId);
//         }
//
//         private void LoadInterstitial()
//         {
//             MaxSdk.LoadInterstitial(interstitialStringId);
//         }
//
//         private void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             // Interstitial ad is ready for you to show. MaxSdk.IsInterstitialReady(adUnitId) now returns 'true'
//             AppsFlyer.sendEvent(Analytics.AFInAppEvents.INTERSTITIALS_AD_API_CALLED, null);
//             FirebaseManager.Instance.LogLoadAdInterSuccess();
//
//             // Reset retry attempt
//             _interstitialsRetryTime = 0;
//         }
//
//         private void OnInterstitialLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
//         {
//             // Interstitial ad failed to load 
//             // AppLovin recommends that you retry with exponentially higher delays, up to a maximum delay (in this case 64 seconds)
//
//             _interstitialsRetryTime++;
//             FirebaseManager.Instance.LogLoadAdInterFail(_interstitialsRetryTime);
//         
//             double retryDelay = Math.Pow(2, Math.Min(6, _interstitialsRetryTime));
//             Invoke(nameof(LoadInterstitial), (float) retryDelay);
//         }
//
//         private void OnInterstitialDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             AppsFlyer.sendEvent(Analytics.AFInAppEvents.INTERSTITIALS_AD_DISPLAYED, null);
//             FirebaseManager.Instance.LogShowAdInterSuccess();
//         }
//
//         private void OnInterstitialAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo,
//             MaxSdkBase.AdInfo adInfo)
//         {
//             // Interstitial ad failed to display. AppLovin recommends that you load the next ad.
//             FirebaseManager.Instance.LogShowAdInterFail(errorInfo.Message);
//             LoadInterstitial();
//
//             UtilityGame.ResumeGame();
//             _onComplete?.Invoke(ShowAdResult.Failed);
//             _onComplete = null;
//         }
//
//         private void OnInterstitialClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             FirebaseManager.Instance.LogClickAdInter();
//         }
//
//         private void OnInterstitialHiddenEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             // Interstitial ad is hidden. Pre-load the next ad.
//             LoadInterstitial();
//
//             UtilityGame.ResumeGame();
//             _onComplete?.Invoke(ShowAdResult.Finished);
//             _onComplete = null;
//         }
//
//         #endregion
//
//         #region Rewared
//
//         public void InitializeRewardedAds()
//         {
//             // Attach callback
//             MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
//             MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdLoadFailedEvent;
//             MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += OnRewardedAdDisplayedEvent;
//             MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdClickedEvent;
//             MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEvent;
//             MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdHiddenEvent;
//             MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
//             MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;
//
//             // Load the first rewarded ad
//             LoadRewardedAd();
//         }
//
//         public bool IsHaveVideoRewarded()
//         {
//             if (!_isAdsSet)
//             {
//                 GetInternetConnection();
//             }
//
//             return MaxSdk.IsRewardedAdReady(rewardedStringId);
//         }
//
//         public void ShowVideoRewarded(Action<ShowAdResult> callback)
//         {
//             AppsFlyer.sendEvent(Analytics.AFInAppEvents.REWARDED_AD_ELIGIBLE, null);
//             if (IsHaveVideoRewarded())
//             {
//                 MaxSdk.ShowRewardedAd(rewardedStringId);
//                 _onComplete = callback;
//             }
//         }
//
//         private void LoadRewardedAd()
//         {
//             MaxSdk.LoadRewardedAd(rewardedStringId);
//         }
//
//         private void OnRewardedAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             // Rewarded ad is ready for you to show. MaxSdk.IsRewardedAdReady(adUnitId) now returns 'true'.
//             AppsFlyer.sendEvent(Analytics.AFInAppEvents.REWARDED_AD_API_CALLED, null);
//
//             // Reset retry attempt
//             _rewardedRetryTime = 0;
//         }
//
//         private void OnRewardedAdLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
//         {
//             // Rewarded ad failed to load
//             // AppLovin recommends that you retry with exponentially higher delays, up to a maximum delay (in this case 64 seconds).
//
//             _rewardedRetryTime++;
//             FirebaseManager.Instance.LogLoadAdsRewardFail(_rewardedRetryTime);
//             double retryDelay = Math.Pow(2, Math.Min(6, _rewardedRetryTime));
//
//             Invoke(nameof(LoadRewardedAd), (float) retryDelay);
//         }
//
//         private void OnRewardedAdDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             FirebaseManager.Instance.LogShowRewardSuccess();
//             AppsFlyer.sendEvent(Analytics.AFInAppEvents.REWARDED_AD_DISPLAYED, null);
//         }
//
//         private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo,
//             MaxSdkBase.AdInfo adInfo)
//         {
//             // Rewarded ad failed to display. AppLovin recommends that you load the next ad.
//             FirebaseManager.Instance.LogShowRewardFail(errorInfo.Message);
//             LoadRewardedAd();
//
//             UtilityGame.ResumeGame();
//             _onComplete?.Invoke(ShowAdResult.Failed);
//             _onComplete = null;
//         }
//
//         private void OnRewardedAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             FirebaseManager.Instance.LogClickAdsReward();
//         }
//
//         private void OnRewardedAdHiddenEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             // Rewarded ad is hidden. Pre-load the next ad
//             LoadRewardedAd();
//
//             UtilityGame.ResumeGame();
//             if (_onComplete != null)
//             {
//                 _onComplete.Invoke(ShowAdResult.Skipped);
//                 _onComplete = null;
//             }
//         }
//
//         private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo adInfo)
//         {
//             // The rewarded ad displayed and the user should receive the reward.
//             // print("Rewarded user: " + reward.Amount + " " + reward.Label);
//             FirebaseManager.Instance.LogAdsRewardComplete();
//             AppsFlyer.sendEvent(Analytics.AFInAppEvents.REWARDED_AD_COMPLETED, null);
//
//             UtilityGame.ResumeGame();
//             _onComplete?.Invoke(ShowAdResult.Finished);
//             _onComplete = null;
//         }
//
//         private void OnRewardedAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
//         {
//             // Ad revenue paid. Use this callback to track user revenue.
//         }
//
//         #endregion
//     }
// }