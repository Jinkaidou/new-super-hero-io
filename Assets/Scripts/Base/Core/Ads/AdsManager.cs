﻿using System;
using Base.Core.Ads.GoogleAdmob;
using UnityEngine;

namespace Base.Core.Ads
{
    public class AdsManager : SingletonMonoDontDestroy<AdsManager>
    {
        [SerializeField] private AdsAdmobContainer adsAdmobContainer;

        private long _lastShowInterstitialAd;
        private const long InterstitialGapTime = 90;

        public AdsManager(string className) : base(className)
        {

        }

        public bool IsHaveVideoRewardedAds()
        {
            return adsAdmobContainer.IsHaveVideoRewarded();
        }
        
        public void PlayRewardedAds(Action<ShowAdResult> resultAction)
        {
            UtilityGame.PauseGame();
            adsAdmobContainer.ShowVideoRewarded(resultAction);
        }
        
        public void PlayInterstitialAds(Action<ShowAdResult> resultAction)
        {
            if (UtilityGame.GetCurrentTime() - _lastShowInterstitialAd < InterstitialGapTime)
            {
                resultAction(ShowAdResult.Failed);
                return;
            }
        
            var isSuccess = adsAdmobContainer.ShowInterstitialAds(resultAction);
            if (isSuccess)
            {
                _lastShowInterstitialAd = UtilityGame.GetCurrentTime();
            }
        }

        public void PlayBannerAds()
        {
            adsAdmobContainer.ShowBannerAds();
        }

        public void HideBannerAds()
        {
            adsAdmobContainer.HideBannerAds();
        }

        public float GetBannerHeight()
        {
            var height = adsAdmobContainer.GetBannerHeight();
            return height;
        }
    }
}