﻿// using System;
// using UnityEngine;
// using UnityEngine.Advertisements;
//
// namespace Core.Common.Ads.UnityAds
// {
//     public class AdsUnityContainer : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsShowListener, IUnityAdsLoadListener
//     {
// #if UNITY_IOS
//     private string gameId = "4008146";
// #elif UNITY_ANDROID
//         private string gameId = "4008147";
// #endif
//
//         public bool internetConnected;
//
//         private bool _testMode = true;
//         private bool _isAdsSet;
//         private Action<ShowAdResult> _resultCallback;
//
//         private bool _isReadyInterstitialAd = false;
//         private bool _isReadyRewardedAd = false;
//
//         private void Start()
//         {
//             CheckInternetConnection();
//             if (internetConnected)
//             {
//                 Advertisement.Initialize(gameId, _testMode);
//                 _isAdsSet = true;
//             }
//             else
//             {
//                 _isAdsSet = false;
//             }
//         }
//
//         private void CheckInternetConnection()
//         {
//             if (Application.internetReachability == NetworkReachability.NotReachable)
//             {
//                 internetConnected = false;
//             }
//             else
//             {
//                 internetConnected = true;
//             }
//         }
//
//         public bool GetInternetConnection()
//         {
//             CheckInternetConnection();
//             if (_isAdsSet == false)
//             {
//                 if (internetConnected)
//                 {
//                     Advertisement.Initialize(gameId, _testMode, this);
//                     _isAdsSet = true;
//                 }
//             }
//
//             return internetConnected;
//         }
//         
//         public bool IsHaveVideoRewarded()
//         {
//             if (!_isAdsSet)
//             {
//                 GetInternetConnection();
//                 Advertisement.Load(AdsPlacementIds.adsX2GoldReceive, this);
//                 return false;
//             }
//             
//             if (!_isReadyRewardedAd)
//                 Advertisement.Load(AdsPlacementIds.adsX2GoldReceive, this);
//
//             return _isReadyRewardedAd;
//         }
//
//         public void ShowVideoRewarded(Action<ShowAdResult> callback)
//         {
//             if (IsHaveVideoRewarded())
//             {
//                 Advertisement.Show(AdsPlacementIds.adsX2GoldReceive, this);
//                 _resultCallback = callback;
//             }
//         }
//
//         private bool IsHaveInterstitialAds()
//         {
//             if (!_isAdsSet)
//             {
//                 GetInternetConnection();
//                 Advertisement.Load(AdsPlacementIds.adsInterstitial1, this);
//                 return false;
//             }
//
//             if (!_isReadyInterstitialAd)
//                 Advertisement.Load(AdsPlacementIds.adsInterstitial1, this);
//             
//             return _isReadyInterstitialAd;
//         }
//
//         public bool ShowInterstitialAds(Action<ShowAdResult> callback)
//         {
//             if (IsHaveInterstitialAds())
//             {
//                 UtilityGame.PauseGame();
//                 _resultCallback = callback;
//                 Advertisement.Show(AdsPlacementIds.adsInterstitial1, this);
//                 return true;
//             }
//             else
//             {
//                 callback(ShowAdResult.Failed);
//                 return false;
//             }
//         }
//         
//         public void OnInitializationComplete()
//         {
//             Debug.Log("Unity Ads initialization complete.");
//         }
//
//         public void OnInitializationFailed(UnityAdsInitializationError error, string message)
//         {
//             Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
//         }
//
//         public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
//         {
//             UtilityGame.ResumeGame();
//             _resultCallback?.Invoke(ShowAdResult.Failed);
//             _resultCallback = null;
//         }
//
//         public void OnUnityAdsShowStart(string placementId)
//         {
//             if (placementId == AdsPlacementIds.adsInterstitial1)
//             {
//                 _isReadyInterstitialAd = false;
//             }
//             else if (placementId == AdsPlacementIds.adsX2GoldReceive)
//             {
//                 _isReadyRewardedAd = false;
//             }
//         }
//
//         public void OnUnityAdsShowClick(string placementId)
//         {
//         }
//
//         public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
//         {
//             UtilityGame.ResumeGame();
//             _resultCallback?.Invoke(ShowAdResult.Finished);
//             _resultCallback = null;
//             
//             Advertisement.Load(placementId, this);
//         }
//
//         public void OnUnityAdsAdLoaded(string placementId)
//         {
//             if (placementId == AdsPlacementIds.adsInterstitial1)
//             {
//                 _isReadyInterstitialAd = true;
//             }
//             else if (placementId == AdsPlacementIds.adsX2GoldReceive)
//             {
//                 _isReadyRewardedAd = true;
//             }
//         }
//
//         public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
//         {
//             if (placementId == AdsPlacementIds.adsInterstitial1)
//             {
//                 _isReadyInterstitialAd = false;
//             }
//             else if (placementId == AdsPlacementIds.adsX2GoldReceive)
//             {
//                 _isReadyRewardedAd = false;
//             }
//         }
//     }
// }