namespace Base.Core.Ads
{
    public static class Helpers
    {
        public static string GetLevelStringFormat(int level)
        {
            if (level < 0) return "9999";
            if (level < 10) return "000" + level;
            if (level < 100) return "00" + level;
            return "0" + level;
        }
    }
}
