﻿using System;
using Base.Core.Analytics;
using GoogleMobileAds.Api;
using UnityEngine;

namespace Base.Core.Ads.GoogleAdmob
{
    public class AdsAdmobContainer : MonoBehaviour
    {
        [Header("Android Ad Unit")]
        [SerializeField] private string bannerAdUnitAndroid;
        [SerializeField] private string interstitialAdUnitAndroid;
        [SerializeField] private string rewardedAdUnitAndroid;
        
        [Header("Ios Ad Unit")]
        [SerializeField] private string bannerAdUnitIos;
        [SerializeField] private string interstitialAdUnitIos;
        [SerializeField] private string rewardedAdUnitIos;

        public bool internetConnected;
        private bool _isAdsSet;
        private Action<ShowAdResult> _onComplete;
        private int _interstitialsRetryTime, _rewardedRetryTime;

        #region Init

        private void Start()
        {
            CheckInternetConnection();
            if (internetConnected)
            {
                InitData();
                _isAdsSet = true;
            }
            else
            {
                _isAdsSet = false;
            }
        }

        private void GetInternetConnection()
        {
            CheckInternetConnection();
            if (_isAdsSet == false)
            {
                if (internetConnected)
                {
                    InitData();
                    _isAdsSet = true;
                }
            }
        }

        private void CheckInternetConnection()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                internetConnected = false;
            }
            else
            {
                internetConnected = true;
            }
        }

        private void InitData()
        {
            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(initStatus =>
            {
                RequestBanner();
                RequestInterstitial();
                RequestRewardedAd();
            });
        }

        #endregion

        #region Banner

        private BannerView _bannerView;

        public void ShowBannerAds()
        {
            if (!_isAdsSet)
            {
                GetInternetConnection();
            }

            _bannerView?.Show();
        }

        public void HideBannerAds()
        {
            _bannerView?.Hide();
        }

        public float GetBannerHeight()
        {
            if (_bannerView != null)
            {
                return _bannerView.GetHeightInPixels();
            }

            return 0;
        }

        private void RequestBanner()
        {
            string adUnitId;
#if UNITY_ANDROID
            adUnitId = bannerAdUnitAndroid;
#elif UNITY_IOS
            adUnitId = bannerAdUnitIos;
#endif
            
            // Create a 320x50 banner at the top of the screen.
            //_bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
            
            // Called when an ad request has successfully loaded.
            _bannerView.OnAdLoaded += BannerHandleOnAdLoaded;
            // Called when an ad request failed to load.
            _bannerView.OnAdFailedToLoad += BannerHandleOnAdFailedToLoad;
            // Called when an ad is clicked.
            _bannerView.OnAdOpening += BannerHandleOnAdOpened;
            // Called when the user returned from the app after an ad click.
            _bannerView.OnAdClosed += BannerHandleOnAdClosed;
            
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();

            // Load the banner with the request.
            _bannerView.LoadAd(request);
            HideBannerAds();
        }

        #region EventCallBacks

        private void BannerHandleOnAdLoaded(object sender, EventArgs e)
        {
        }

        private void BannerHandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
        }

        private void BannerHandleOnAdOpened(object sender, EventArgs e)
        {
        }

        private void BannerHandleOnAdClosed(object sender, EventArgs e)
        {
        }

        #endregion
        

        #endregion

        #region Interstitials
        
        private InterstitialAd _interstitial;
        
        public bool ShowInterstitialAds(Action<ShowAdResult> resultAction)
        {
            if (IsHaveInterstitialAd())
            {
                UtilityGame.PauseGame();
                _onComplete = resultAction;
                _interstitial.Show();
                return true;
            }

            resultAction(ShowAdResult.Failed);
            return false;
        }
        
        private bool IsHaveInterstitialAd()
        {
            if (!_isAdsSet)
            {
                GetInternetConnection();
            }

            if (_interstitial is null)
            {
                return false;
            }
            
            return _interstitial.IsLoaded();
        }

        private void RequestInterstitial()
        {
            string adUnitId;
#if UNITY_ANDROID
            adUnitId = interstitialAdUnitAndroid;
#elif UNITY_IOS
            adUnitId = interstitialAdUnitIos;
#endif

            // Initialize an InterstitialAd.
            //_interstitial = new InterstitialAd(adUnitId);
            
            // Called when an ad request has successfully loaded.
            _interstitial.OnAdLoaded += InterstitialHandleOnAdLoaded;
            // Called when an ad request failed to load.
            _interstitial.OnAdFailedToLoad += InterstitialHandleOnAdFailedToLoad;
            // Called when an ad is shown.
            _interstitial.OnAdOpening += InterstitialHandleOnAdOpening;
            // Called when the ad is closed.
            _interstitial.OnAdClosed += InterstitialHandleOnAdClosed;
            
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            
            // Load the interstitial with the request.
            _interstitial.LoadAd(request);
        }

        #region Callbacks

        private void InterstitialHandleOnAdLoaded(object sender, EventArgs e)
        {
            // Interstitial ad is ready for you to show. MaxSdk.IsInterstitialReady(adUnitId) now returns 'true'
            FirebaseManager.Instance.LogLoadAdInterSuccess();

            // Reset retry attempt
            _interstitialsRetryTime = 0;
        }

        private void InterstitialHandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            // Interstitial ad failed to load 
            // AppLovin recommends that you retry with exponentially higher delays, up to a maximum delay (in this case 64 seconds)

            _interstitialsRetryTime++;
            FirebaseManager.Instance.LogLoadAdInterFail(_interstitialsRetryTime);
        
            double retryDelay = Math.Pow(2, Math.Min(6, _interstitialsRetryTime));
            Invoke(nameof(RequestInterstitial), (float) retryDelay);
        }

        private void InterstitialHandleOnAdOpening(object sender, EventArgs e)
        {
            FirebaseManager.Instance.LogShowAdInterSuccess();
        }

        private void InterstitialHandleOnAdClosed(object sender, EventArgs e)
        {
            RequestInterstitial();
            
            UtilityGame.ResumeGame();
            _onComplete?.Invoke(ShowAdResult.Finished);
            _onComplete = null;
        }

        #endregion

        #endregion

        #region Rewarded

        private RewardedAd _rewardedAd;

        public void ShowVideoRewarded(Action<ShowAdResult> callback)
        {
            if (IsHaveVideoRewarded())
            {
                _rewardedAd.Show();
                _onComplete = callback;
            }
        }
        
        public bool IsHaveVideoRewarded()
        {
            if (!_isAdsSet)
            {
                GetInternetConnection();
            }

            if (_rewardedAd is null)
                return false;
            
            return _rewardedAd.IsLoaded();
        }

        private void RequestRewardedAd()
        {
            string adUnitId;
#if UNITY_ANDROID
            adUnitId = rewardedAdUnitAndroid;
#elif UNITY_IOS
            adUnitId = rewardedAdUnitIos;
#endif
            
            //_rewardedAd = new RewardedAd(adUnitId);
            
            // Called when an ad request has successfully loaded.
            _rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
            // Called when an ad request failed to load.
            _rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
            // Called when an ad is shown.
            _rewardedAd.OnAdOpening += HandleRewardedAdOpening;
            // Called when an ad request failed to show.
            _rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
            // Called when the user should be rewarded for interacting with the ad.
            _rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
            // Called when the ad is closed.
            _rewardedAd.OnAdClosed += HandleRewardedAdClosed;

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            
            // Load the rewarded ad with the request.
            _rewardedAd.LoadAd(request);
        }

        #region Callbacks

        private void HandleRewardedAdLoaded(object sender, EventArgs args)
        {
            // Rewarded ad is ready for you to show. MaxSdk.IsRewardedAdReady(adUnitId) now returns 'true'.

            // Reset retry attempt
            _rewardedRetryTime = 0;
        }

        private void HandleRewardedAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            // Rewarded ad failed to load
            // AppLovin recommends that you retry with exponentially higher delays, up to a maximum delay (in this case 64 seconds).

            _rewardedRetryTime++;
            FirebaseManager.Instance.LogLoadAdsRewardFail(_rewardedRetryTime);
            double retryDelay = Math.Pow(2, Math.Min(6, _rewardedRetryTime));

            Invoke(nameof(RequestRewardedAd), (float) retryDelay);
        }

        private void HandleRewardedAdOpening(object sender, EventArgs args)
        {
            FirebaseManager.Instance.LogShowRewardSuccess();
        }

        private void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
        {
            // Rewarded ad failed to display. AppLovin recommends that you load the next ad.
            FirebaseManager.Instance.LogShowRewardFail(args.AdError.GetMessage());
            // RequestRewardedAd();

            UtilityGame.ResumeGame();
            _onComplete?.Invoke(ShowAdResult.Failed);
            _onComplete = null;
        }

        private void HandleRewardedAdClosed(object sender, EventArgs args)
        {
            RequestRewardedAd();

            UtilityGame.ResumeGame();
            if (_onComplete != null)
            {
                _onComplete.Invoke(ShowAdResult.Finished);
                _onComplete = null;
            }
        }

        private void HandleUserEarnedReward(object sender, Reward args)
        {
            FirebaseManager.Instance.LogAdsRewardComplete();
        }

        #endregion

        #endregion
    }
}