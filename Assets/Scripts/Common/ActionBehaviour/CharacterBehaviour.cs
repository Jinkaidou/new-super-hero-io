using System;
using Base.Core.GameResources;
using Common.Behaviour;
using DataAccount;
using Model.DataHero;
using Model.DataUpgrade;
using UnityEngine;

public class CharacterBehaviour : MonoBehaviour
{
    public ItemEffectPlayerController itemEffectPlayerController;
    public MovementController movementController;
    public AttackController attackController;
    public LevelController levelController;
    public ShieldController shieldController;
    public AnimController animController;
    public WeaponController weapon;
    

    public UpgradeCollection upgradeCollection;
    public HeroCollection heroCollection;
    public bool isPlayer;
    public bool isShieldOpen;

    public bool canControll;
    protected  virtual void Update()
    {
        var width = gameObject.transform.localScale.x;
        weapon.SetWitdhCurve(width);
    }

    public void InitDataHero(HeroData heroData, string name)
    {
        gameObject.transform.name = name;
        itemEffectPlayerController.InitSpriteArt(heroData);
        levelController._limitLevel = heroCollection.dataLevel[0].dataLevel;
        levelController._currentLevel = 0;
    }

    public void InitStatValue()
    {
        
        var levelSpeed = DataAccountPlayer.PlayerStats.GetStatLevel(StatType.MoveSpeed);
        var levelAttackRange = DataAccountPlayer.PlayerStats.GetStatLevel(StatType.AttackRange);
        var levelAttackSpeed = DataAccountPlayer.PlayerStats.GetStatLevel(StatType.MoveSpeed);

        var statSpeedLv = upgradeCollection.GetValueSpeed(levelSpeed);
        var statAttackRangeLv = upgradeCollection.GetValueRangeAttack(levelAttackRange);
        var statAttackSpeedLv = upgradeCollection.GetValueSpeedAttack(levelAttackSpeed);

        movementController.speed = statSpeedLv;
        attackController.rangeAttack = -statAttackRangeLv;
        attackController.speedAttack = statAttackSpeedLv;
    }
    
    public void OpenShield()
    {
        isShieldOpen = true;
    }

    public void CloseShield()
    {
        isShieldOpen = false;
    }
}
