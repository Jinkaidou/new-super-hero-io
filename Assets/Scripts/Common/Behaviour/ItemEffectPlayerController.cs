﻿using System;
using Base.Core.Analytics;
using Com.LuisPedroFonseca.ProCamera2D;
using Controller;
using Controller.LoadData;
using Core.Pool;
using Model.DataHero;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Common.Behaviour
{
    public class ItemEffectPlayerController : MonoBehaviour
    {
        public ItemCollection itemCollection;
        public GameObject shieldItemEff;
        public GameObject speedItemEffet;
        public GameObject smokeEffect;
        public string _pathWeapon;

        [SerializeField] private SpriteRenderer _head;
        //[SerializeField] private SpriteRenderer _headLayer;
        [SerializeField] private SpriteRenderer _leftArm;
        [SerializeField] private SpriteRenderer _rightArm;
        [SerializeField] private SpriteRenderer _weap;

        public WeaponController weaponClone;
        public CoolDownItemSystem bigSizeEff;
        public CoolDownItemSystem fadeEff;
        public CoolDownItemSystem speedEff;
        public CoolDownItemSystem shieldEff;
        //other module

        private bool overrideSpeed = true;
        public bool overrideScale = true;
        public float lastestSizeCamera;

        Coroutine SpeedCor;
        Coroutine BigCor;
        Coroutine FadeCor;
        Coroutine ShieldCor;
    
        public CharacterBehaviour _characterBehaviour;

        private void Update()
        {
            smokeEffect.transform.localScale = new Vector3(this.gameObject.transform.localScale.x, this.gameObject.transform.localScale.y, 0);
        }

        private void Start()
        {

            this.RegisterListener(EventID.ShieldHit, (sender, param) =>
            {
                HitShield();
            });

            ShieldEff();
            var vecter = this.gameObject.transform.position;
            _head.transform.position = new Vector3(vecter.x, vecter.y, 0);
            //_characterBehaviour = GetComponent<CharacterBehaviour>();
            lastestSizeCamera = ProCamera2D.Instance.GameCamera.orthographicSize;
        }

        public void InitSpriteArt(HeroData heroData)
        {
            //Resources.Load<Sprite>(pathImageWeapon);
            var path = LoadResourceController.Instance.LoadPathDataCharacter(heroData.skinType);
            _head.sprite = Resources.Load<Sprite>(path + "/head");
            //_headLayer.sprite = Resources.Load<Sprite>(path + "/head");
            _leftArm.sprite = Resources.Load<Sprite>(path  + "/left");
            _rightArm.sprite = Resources.Load<Sprite>(path + "/right");
            _pathWeapon = LoadResourceController.Instance.LoadPathDataWeapon(heroData.skinType);
            _characterBehaviour.weapon.InitData(_pathWeapon,heroData.Color);
        }


        public void DropWeapon()
        {
            var path = _characterBehaviour.weapon.pathImageWeapon;
            var weaponDrop = SmartPool.Instance.Spawn(weaponClone.gameObject, this.gameObject.transform.position, this.gameObject.transform.rotation);
            weaponDrop.GetComponent<WeaponController>().InitData(path,Color.red);
        }

        public void TeleportEffect()
        {
            // _characterBehaviour.animController.PlayAnimDisapear();
            // LeanTween.delayedCall(0.15f, () =>
            // {
            //     var position = Map.Instance.position.transform.position;
            //     var y = Random.Range(-position.y,position.y);
            //     var x = Random.Range(-position.x,position.x);
            //     _characterBehaviour.animController.AppearAnim();
            //     gameObject.transform.position = new Vector3(x,y,0);
            // });
            var position = Map.Instance.position.transform.position;
            var y = Random.Range(-position.y,position.y);
            var x = Random.Range(-position.x,position.x);
            gameObject.transform.position = new Vector3(x,y,0);
        }

        public void ShieldEff()
        {
            shieldItemEff.gameObject.SetActive(true);
            _characterBehaviour.OpenShield();
            if (ShieldCor != null)
            {
                this.StopCoroutine(ShieldCor);
                ShieldCor = null;
            }
            var time = itemCollection.TimeCoolDown("shieldItem");
            if (shieldEff != null)
            {
                shieldEff.gameObject.SetActive(true);
                shieldEff.StartCount((long)time);
            }

            ShieldCor = this.StartDelayMethod(time, DisableShield);
        }

        public void HitShield()
        {
            if (ShieldCor != null)
            {
                this.StopCoroutine(ShieldCor);
                ShieldCor = null;
            }
            if (shieldEff != null)
            {
                shieldEff.gameObject.SetActive(false);
                shieldEff.StopCount();
            }
            shieldItemEff.gameObject.SetActive(false);
            ShieldCor = this.StartDelayMethod(0.25f, DisableShield2);
        }

        public void DisableShield2()
        {
            _characterBehaviour.CloseShield();
        }

        public void DisableShield()
        {
            shieldItemEff.gameObject.SetActive(false);
            _characterBehaviour.CloseShield();
        }

        public void SpeedEff()
        {
            _characterBehaviour.movementController.speedItem = itemCollection.ValueEff("speedItem");
            speedItemEffet.gameObject.SetActive(true);

            var time = itemCollection.TimeCoolDown("speedItem");
            if (SpeedCor != null)
            {
                this.StopCoroutine(SpeedCor);
                SpeedCor = null;
            }

            if (speedEff != null)
            {
                speedEff.gameObject.SetActive(true);
                speedEff.StartCount((long)time);
            }
         
            SpeedCor = this.StartDelayMethod(time, DisableSpeddEff);
        }

        public void DisableSpeddEff()
        {
            speedItemEffet.gameObject.SetActive(false);
            overrideSpeed = true;
            _characterBehaviour.movementController.speedItem = 1;
        }

        public void FadeEff()
        {
            _head.color = new Color(255, 255, 255, 0.6f);
            _leftArm.color = new Color(255, 255, 255, 0.6f);
            _rightArm.color = new Color(255, 255, 255, 0.6f);
            _weap.color = new Color(255, 255, 255, 0.6f);
            if (FadeCor != null)
            {
                this.StopCoroutine(FadeCor);
                FadeCor = null;
            }
            var time = itemCollection.TimeCoolDown("fadeItem");
            if (fadeEff != null)
            {
                fadeEff.gameObject.SetActive(true);
                fadeEff.StartCount((long)time);
            }
            FadeCor = this.StartDelayMethod(time, DisableFade);
        }

        public void DisableFade()
        {
            _head.color = new Color(255, 255, 255, 1);
            _leftArm.color = new Color(255, 255, 255, 1);
            _rightArm.color = new Color(255, 255, 255, 1);
            _weap.color = new Color(255, 255, 255, 1);
        }
         public void ScaleEff()
        {
            var scaleEff = itemCollection.ValueEff("bigSizeItem");
            this.gameObject.transform.localScale = new Vector3(scaleEff, scaleEff, 0);
            overrideScale = false;
            ResizeCamera();
            if (BigCor != null)
            {
                StopCoroutine(BigCor);
                BigCor = null;
            }
            var time = itemCollection.TimeCoolDown("bigSizeItem");
            if (bigSizeEff != null)
            {
                bigSizeEff.gameObject.SetActive(true);
                bigSizeEff.StartCount((long)time);
            }
            BigCor = this.StartDelayMethod(time, DisableBigScale);
        }

        public void DisableBigScale()
        {
            overrideScale = true;
            if (_characterBehaviour.isPlayer)
            {
                ProCamera2D.Instance.UpdateScreenSize(lastestSizeCamera, 0.5f);
            }
            var currentScale = _characterBehaviour.levelController._currentScale;
            gameObject.transform.localScale = new Vector3(currentScale, currentScale, 1);
        }
        private void ResizeCamera()
        {
            if (_characterBehaviour.isPlayer)
            {
                ProCamera2D.Instance.UpdateScreenSize(itemCollection.ValueSideKick("bigSizeItem"), 0.5f);
            }
        }

       

        public void DisableEff()
        {
            bigSizeEff.gameObject.SetActive(false);
            bigSizeEff.StopCount(); 
            
            fadeEff.gameObject.SetActive(false);
            fadeEff.StopCount();
            
            speedEff.gameObject.SetActive(false);
            speedEff.StopCount();
            
            shieldEff.gameObject.SetActive(false);
            shieldEff.StopCount();
        }
        
        private void OnTriggerEnter2D(Collider2D collision)
        {
            
            if (collision.CompareTag("itemWeapon"))
            {
                var itemWeapon = collision.GetComponent<WeaponController>();
                _pathWeapon = itemWeapon.pathImageWeapon;

                var weapon = _characterBehaviour.weapon;
                weapon.gameObject.SetActive(true);
                weapon.InitData(_pathWeapon,itemWeapon.colorEffect);
                itemWeapon.DisablePool();
            }
            var itemController = collision.GetComponent<ItemController>();
            if (collision.CompareTag("speedItem"))
            {
                if (overrideSpeed)
                {
                    SpeedEff();
                }
                if (_characterBehaviour.isPlayer)
                {
                    FirebaseManager.Instance.LogEvent(FirebaseParams.SPEED_UP);
                }
                itemController.SendEvent();
                itemController.DestroyGameObject();
            }

            if (collision.CompareTag("fadeItem"))
            {
                FadeEff();
                if (_characterBehaviour.isPlayer)
                {
                    FirebaseManager.Instance.LogEvent(FirebaseParams.FADE);
                }
                itemController.SendEvent();
                itemController.DestroyGameObject();
            }

            if (collision.CompareTag("shieldItem"))
            {
                ShieldEff();
                if (_characterBehaviour.isPlayer)
                {
                    FirebaseManager.Instance.LogEvent(FirebaseParams.SHIELD);
                }
                itemController.SendEvent();
                itemController.DestroyGameObject();
            }

            if (collision.CompareTag("bigSizeItem"))
            {
                //if (overrideScale)
                //{
                //    ScaleEff();
                //}
                ScaleEff();
                if (_characterBehaviour.isPlayer)
                {
                    FirebaseManager.Instance.LogEvent(FirebaseParams.BIG_SIZE);
                }
                itemController.SendEvent();
                itemController.DestroyGameObject();
            }

            if (collision.CompareTag("teleport"))
            {
                if (_characterBehaviour.isPlayer)
                {
                    FirebaseManager.Instance.LogEvent(FirebaseParams.TELEPORT);
                }
                TeleportEffect();
                itemController.SendEvent();
                itemController.DestroyGameObject();
            }
        }
    
    }
}
