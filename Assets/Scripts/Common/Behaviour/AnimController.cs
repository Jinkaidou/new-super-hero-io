using UnityEngine;

namespace Common.Behaviour
{
    public class AnimController : MonoBehaviour
    {
        public Animator animatorLeftHand;
        public Animator animatorRightHand;
       
        //public Animator animatorTwistDisapear;
        //public Animator animatorTwistDisapear;

        public void PlayAnim()
        {
            animatorLeftHand.Play("LeftArm");
            animatorRightHand.Play("RightArm");
        }

        public void DisableAnim()
        {
            animatorLeftHand.enabled = false;
            animatorRightHand.enabled = false;

        }

        public void EnebleAnim()
        {
            animatorLeftHand.enabled = true;
            animatorRightHand.enabled = true;
        }

       
        // public void PlayAnimDisapear()
        // {
        //     animatorTwistDisapear.enabled = false;
        //     animatorLeftHand.gameObject.SetActive(false);
        //     animatorRightHand.gameObject.SetActive(false);
        // }
        //
        // public void AppearAnim()
        // {
        //     animatorTwistDisapear.enabled = true;
        //     animatorTwistDisapear.Play("Twist 1");
        //     animatorLeftHand.gameObject.SetActive(true);
        //     animatorRightHand.gameObject.SetActive(true);
        // }
    }
}
