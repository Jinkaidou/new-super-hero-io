﻿using System.Collections;
using Base.Core.Sound;
using UnityEngine;

namespace Common.Behaviour
{
    public class AttackController : MonoBehaviour
    {
        [SerializeField] private Transform arm;
        [SerializeField] private GameObject sword;
        [SerializeField] private SpriteRenderer weap;

        public WeaponController weapon;
        public GameObject effectTrailAttack;
        public float timeCoolDownAttack;
        public float rangeAttack;
        public float timeThinkingAttack;
        public float speedAttack;
        public float speedRecallAttack;
        private float _timeAttack;
        //otherModule
        public CharacterBehaviour characterBehaviour;
        public WeaponBulletController weaponBulletController;
        public AnimAttackController attackAnim;
        Coroutine attackCount;
        Coroutine revertAttack;

        public bool checkSound;
        private bool _attackCondition;
    
        private void Awake()
        {
            _attackCondition = true;
        }

        private void Start()
        {
            weapon.GetComponent<BoxCollider2D>().enabled = false;
        }
        private void FixedUpdate()
        {
            
            if (attackAnim != null && attackAnim.endAnim)
            {
                SetWeaponPositionPlayer();
            }
            
        }

        #region Player

        public void AttackPlayer()
        {
            characterBehaviour.movementController.StopMovingAttack();
            arm.Rotate(0,0,rangeAttack);
            SoundManager.Instance.PlaySound(SoundType.Slash);
            attackAnim.AttackAnim(speedAttack);
        }

        public void SetWeaponPositionPlayer()
        {
            arm.Rotate(0,0, -rangeAttack);
            characterBehaviour.animController.EnebleAnim();
            _attackCondition = false;
            characterBehaviour.movementController.MovingAgainAttack();
            attackAnim.endAnim = false;
        }

        #endregion

        public void Attack()
        {
            characterBehaviour.movementController.StopMovingAttack();
            _attackCondition = false;
            characterBehaviour.animController.DisableAnim();
            var weaponBox = sword.GetComponent<BoxCollider2D>();
            weaponBox.enabled = true;
            effectTrailAttack.SetActive(true);
            StartCoroutine(CorAttack());
        }

        public void SetWeaponPosition()
        {
            characterBehaviour.animController.EnebleAnim();
            var weaponBox = sword.GetComponent<BoxCollider2D>();
            weaponBox.enabled = false;
            revertAttack = StartCoroutine(CorRevertAttack());
        }

       

        public void SetDefaultPos()
        {
            arm.Rotate(0,0,0);
        }

        private IEnumerator CorAttack()
        {
            if (checkSound)
            {
                SoundManager.Instance.PlayOneShot(SoundType.Slash);
            }
            float startAngel = arm.localRotation.eulerAngles.z;
            float timer = speedAttack;
            float dt = Time.deltaTime;
            float angleForDeltaTime = ((rangeAttack - startAngel) / timer) * dt;
            Quaternion direction = Quaternion.identity;

            while (timer > 0)
            {
                startAngel += angleForDeltaTime;
                direction.eulerAngles = new Vector3(0, 0, startAngel);
                arm.localRotation = direction;
                yield return new WaitForSeconds(dt);
                timer -= dt;
            }
            SetWeaponPosition();
        }
        private IEnumerator CorRevertAttack()
        {
            
            if (checkSound)
            {
                SoundManager.Instance.PlayOneShot(SoundType.Slash);
            }
            effectTrailAttack.SetActive(false);
            float startAngel = arm.localRotation.eulerAngles.z;
            float timer = speedAttack;
            float dt = Time.deltaTime;
            float angleForDeltaTime = (-rangeAttack / timer) * dt;
            Quaternion direction = Quaternion.identity;

            while (timer > 0)
            {
                startAngel += angleForDeltaTime;
                direction.eulerAngles = new Vector3(0, 0, startAngel);
                arm.localRotation = direction;

                yield return new WaitForSeconds(dt);
                timer -= dt;
            }
            arm.localRotation = Quaternion.identity;
            _attackCondition = false;
            characterBehaviour.canControll = true;
            characterBehaviour.movementController.MovingAgainAttack();
        }

        public void InputAttack()
        {
            if (weapon.gameObject.activeSelf)
            {
                characterBehaviour.canControll = false;
                Attack();
            }
        }

        public void PlayerAttack()
        {
            if(weapon.gameObject.activeSelf && characterBehaviour.isPlayer)
            {
                characterBehaviour.canControll = false;
                AttackPlayer();
            }
        }
        
        public void AutoAttacking()
        {
            _timeAttack -= Time.deltaTime;
            if (_timeAttack > 0)
            {
                return;
            }
            var enemyPos = gameObject.transform.position;
            RaycastHit2D[] hits = Physics2D.CircleCastAll(enemyPos, 2f, Vector3.zero);
            for (int i = 0; i < hits.Length; i++)
            {
                var hit = hits[i];
                var checkOpp = hit.transform.CompareTag("otherPlayer1") || hit.transform.CompareTag("Shield");
                var checkCondition = hit && hit.transform.name != gameObject.transform.name && _attackCondition;
                var playSound1 = (hit.transform.name == "Player");
                //var playSound = hit.transform.CompareTag("MainCamera");
                if (checkCondition && checkOpp)
                {
                    LeanTween.delayedCall(timeThinkingAttack, () =>
                    {
                        if (weapon is null)
                        {
                            return;
                        }
                        InputAttack();
                    });
                    _timeAttack = 1f;
                }

                if (playSound1)
                {
                    checkSound = true;
                }
                else
                {
                    checkSound = false;
                }
            }
        }

        public void AutoUseUltimate()
        {
            UltimateSkill(this.transform);
        }
        
        public void UltimateSkill(Transform transform)
        {
            if (weapon.gameObject.activeInHierarchy)
            {
                weapon.gameObject.SetActive(false);
                var pathWeapon = "HeroIO/Character/weapon/" + weap.sprite.name;
                var script = Instantiate(weaponBulletController, transform.position, transform.rotation);
                script.InitData(pathWeapon,this.gameObject.transform.localScale.x);
               
                script.gameObject.transform.tag = weapon.gameObject.transform.tag;
            }
        }
    }
}
