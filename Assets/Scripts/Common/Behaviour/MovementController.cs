using UnityEngine;

namespace Common.Behaviour
{
    public class MovementController : MoveController
    {
        //other module
        public AnimController animControll;
        public GameObject bodyHero;
        public override void Move(Vector3 direction)
        {
            animControll.EnebleAnim();
            if (direction != Vector3.zero)
            {
                animControll.EnebleAnim();
                animControll.PlayAnim();
                Quaternion toRotation = Quaternion.LookRotation(Vector3.forward, direction);
                bodyHero.transform.rotation = Quaternion.RotateTowards(bodyHero.transform.rotation, toRotation, 720 * Time.deltaTime);
            }
            else if (direction == Vector3.zero)
            {
                animControll.DisableAnim();
            }

            base.Move(direction);
        }

        public void StopMovingLimit()
        {
            canMoveLimitNow = false;
            animControll.DisableAnim();
        }

        public void MovingAgainLimit()
        {
            canMoveLimitNow = true;
            animControll.EnebleAnim();
            animControll.PlayAnim();
        }

        public void StopMovingAttack()
        {
            canMoveAttackNow = false;
            animControll.DisableAnim();
        }

        public void MovingAgainAttack()
        {
            canMoveAttackNow = true;
            animControll.EnebleAnim();
            animControll.PlayAnim();
        }
    }
}
