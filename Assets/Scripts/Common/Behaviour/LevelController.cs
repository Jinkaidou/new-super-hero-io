using Com.LuisPedroFonseca.ProCamera2D;
using Controller.LoadData;
using GameController;
using Model.DataHero;
using UnityEngine;

namespace Common.Behaviour
{
    public class LevelController : MonoBehaviour
    {
        public float _currentLevel;
        public int startLevel;
        public int _limitLevel;
        public float _currentScale;
        public float _levelPoint = 1;

        private HeroCollection _heroCollection;
        private CharacterBehaviour _characterBehaviour;

        private void Awake()
        {
            _currentScale = 1;
            _characterBehaviour = GetComponent<CharacterBehaviour>();
            _heroCollection = LoadResourceController.Instance.LoadHeroDataCollection();
        }

        public void IncreaseLevel(float point)
        {
            var herocollection = _heroCollection.dataLevel;
            _currentLevel += point;
            if (_currentLevel >= _limitLevel && _currentLevel < herocollection[herocollection.Count - 1].dataLevel)
            {
                for (int i = 0; i < herocollection.Count; i++)
                {
                    if (herocollection[i].dataLevel == _limitLevel)
                    {
                      
                        LevelUpScale(herocollection[i].scale);
                        if (i + 1 < herocollection.Count)
                        {
                            _limitLevel = herocollection[i + 1].dataLevel;
                            startLevel = herocollection[i].dataLevel;
                            _levelPoint = i + 1;
                            if (_characterBehaviour.isPlayer)
                            {
                                this.PostEvent(EventID.LevelUp);
                            }
                            return;
                        }
                    }
                }
            }
            var lowestRanking = Ranking.Instance.numberSmallest;
            if (_currentLevel > lowestRanking)
            {
                if (Ranking.Instance.topFivePlayer.ContainsKey(this.transform.gameObject.name))
                {
                    Ranking.Instance.topFivePlayer[this.transform.gameObject.name] = _currentLevel;
                }
                else
                {
                    Ranking.Instance.topFivePlayer.Add(this.transform.gameObject.name,_currentLevel);
                }
            }
        }
    
        public void LevelUpScale(float scale)
        {
            if (_characterBehaviour.itemEffectPlayerController.overrideScale)
            {
                this.gameObject.transform.localScale = new Vector3(scale, scale, 0);

                if (_characterBehaviour.isPlayer)
                {
                    var heroScaleData = _heroCollection.GetHeroScaleData(_limitLevel);
                    _characterBehaviour.itemEffectPlayerController.lastestSizeCamera = heroScaleData.sizeCamera;
                    ProCamera2D.Instance.UpdateScreenSize(heroScaleData.sizeCamera, 0.5f);
                }
            }
        
            _currentScale = scale;
        }

        protected virtual void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("food"))
            {
                collision.GetComponent<FoodController>();
          
                IncreaseLevel(collision.GetComponent<FoodController>().point);
                collision.GetComponent<FoodController>().SendEvent();
                collision.GetComponent<FoodController>().DestroyGameObject();
                
            }
        }
    }
}
