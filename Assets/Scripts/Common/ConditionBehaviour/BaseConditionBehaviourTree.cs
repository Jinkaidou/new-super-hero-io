using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
public class BaseConditionBehaviourTree : Conditional
{
    public AIBehaviourController aIBehaviourController = null;
    public override void OnAwake()
    {
        if (!aIBehaviourController)
        {
            aIBehaviourController = GetComponent<AIBehaviourController>();
        }
    }
}
