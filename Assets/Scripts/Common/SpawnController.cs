using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpawnController
{
    public class SpawnController : MonoBehaviour
    {
        private float limitGreenAxis;
        private float limitRedAxis;

        public Vector3 SpawnPos()
        {
            if (Map.Instance is null)
            {
               return new Vector3(0, 0, 0);
            }
            limitGreenAxis = Map.Instance.position.transform.position.y;
            limitRedAxis = Map.Instance.position.transform.position.x;
            var y = Random.Range(-limitGreenAxis, limitGreenAxis);
            var x = Random.Range(-limitRedAxis, limitRedAxis);
            var pos = Player.Instance.transform.position;
            if (Math.Abs(pos.x - x) <= 0 && Math.Abs(pos.y - y) <= 0)
            {
                return  Vector3.zero;
            }
            return new Vector3(x, y, 0);
        }
    }
}