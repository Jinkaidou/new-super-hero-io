﻿using System.Collections.Generic;
using Base.Core.GameResources;
using Base.Core.Popup;
using UI.ItemReward;
using UnityEngine;
using UnityEngine.UI;

namespace UI.RewardPopup
{
    public class ModuleUiRewardPopup : PopupBase
    {
        [SerializeField] private ModuleItemShowReward moduleItemShowReward;
        [SerializeField] private Button backgroundBtn;

        private void Start()
        {
            backgroundBtn.onClick.AddListener(Close);
        }

        public void InitData(List<Resource> rewards)
        {
            moduleItemShowReward.InitData(rewards);
        }

        protected override void OnShow()
        {
        }

        protected override void OnHide()
        {
        }
    }
}