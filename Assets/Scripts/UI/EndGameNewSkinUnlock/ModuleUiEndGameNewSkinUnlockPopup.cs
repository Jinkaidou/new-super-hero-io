﻿using Base.Core.Ads;
using Base.Core.Analytics;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using Controller.LoadData;
using DataAccount;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.EndGameNewSkinUnlock
{
    public class ModuleUiEndGameNewSkinUnlockPopup : PopupBase
    {
        [SerializeField] private Transform board;
        [SerializeField] private Image skinIcon;
        [SerializeField] private Button watchAdBtn;
        [SerializeField] private Button freeBtn;
        [SerializeField] private Button noThanksBtn;
        [SerializeField] private GameObject tutHand;
        [SerializeField] private TextMeshProUGUI textBtnFree;

        private SkinType _skinType;

        private void Awake()
        {
            watchAdBtn.onClick.AddListener(OnClickWatchAd);
            freeBtn.onClick.AddListener(OnClickFree);
            noThanksBtn.onClick.AddListener(OnClickNoThanks);
        }

        protected override void OnShow()
        {
            board.localScale = Vector3.zero;
            board.DOScale(Vector3.one, 0.5f).SetUpdate(true);
            noThanksBtn.gameObject.SetActive(false);
            SetTutSkin();
            this.StartDelayMethod(3f, () =>
            {
                noThanksBtn.gameObject.SetActive(true);
            });
        }

        private void SetTutSkin()
        {
            if (DataAccountPlayer.PlayerTutorialData.unlockSkin)
            {
                tutHand.gameObject.SetActive(true);
                watchAdBtn.gameObject.SetActive(false);
                freeBtn.gameObject.SetActive(true);
            }
            else
            {
                tutHand.gameObject.SetActive(false);
                watchAdBtn.gameObject.SetActive(true);
                freeBtn.gameObject.SetActive(false);
            }
        }
        
        protected override void OnHide()
        {
        }

        public void InitData(SkinType skinUnlock)
        {
            _skinType = skinUnlock;
            skinIcon.sprite = LoadResourceController.Instance.LoadSkinIcon(_skinType);
        }

        private void OnClickFree()
        {
            DataAccountPlayer.PlayerSkins.AddOwnedSkin(_skinType);
            DataAccountPlayer.PlayerSkins.OnChangeSkin(_skinType);
            DataAccountPlayer.PlayerTutorialData.ChangeSkinValue(false);
            textBtnFree.text = "Equipped";
            Close();

        }
        
        private void OnClickWatchAd()
        {
            FirebaseManager.Instance.LogEvent(FirebaseParams.ENTER_ADS);
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            {
                PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                return;
            }
            
            AdsManager.Instance.PlayRewardedAds((result) =>
            {
                if (result != ShowAdResult.Finished)
                {
                    PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                    return;
                }
                FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS); 
                FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsGetSkin);
                DataAccountPlayer.PlayerSkins.AddOwnedSkin(_skinType);
                DataAccountPlayer.PlayerSkins.OnChangeSkin(_skinType);
                Close();
            });
            // DataAccountPlayer.PlayerSkins.AddOwnedSkin(_skinType);
            // DataAccountPlayer.PlayerSkins.OnChangeSkin(_skinType);
            Close();
        }

        private void OnClickNoThanks()
        {
            Close();
        }
    }
}