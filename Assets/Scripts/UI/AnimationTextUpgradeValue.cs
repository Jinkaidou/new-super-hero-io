using UnityEngine;

namespace UI
{
    public class AnimationTextUpgradeValue : MonoBehaviour
    {
        public Animator text;

        public void DisAnim()
        {
            text.enabled = false;
        }
    }
}
