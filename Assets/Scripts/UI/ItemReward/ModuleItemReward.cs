﻿using Base.Core.GameResources;
using Controller.LoadData;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.ItemReward
{
    public class ModuleItemReward : MonoBehaviour
    {
        [SerializeField] private Image itemIcon;
        [SerializeField] private TextMeshProUGUI itemValue;

        public bool useCustomReward = false;

        [ShowIf("useCustomReward"), SerializeField]
        private Resource reward;

        public Resource ResourceData => reward;

        public Vector3 IconPosition => itemIcon.transform.position;

        private void Start()
        {
            if (useCustomReward)
                LoadData();
        }

        public void InitData(Resource rewardData, string format = "x{0}")
        {
            if (!useCustomReward)
            {
                reward = rewardData;
            }

            LoadData(format);
        }

        [Button]
        private void LoadData(string format = "{0}")
        {
            if (reward == null)
                return;

            itemValue.text = string.Format(format, reward.value);

            if (itemIcon != null)
            {
                switch (reward.resourceType)
                {
                    case ResourceType.Money:
                        itemIcon.sprite = LoadResourceController.Instance.LoadMoneyIcon((MoneyType) reward.id);
                        break;
                    case ResourceType.Skin:
                        itemIcon.sprite = LoadResourceController.Instance.LoadSkinIcon((SkinType) reward.id);
                        RotateSkin();
                        break;
                    case ResourceType.Stat:
                        itemIcon.sprite = LoadResourceController.Instance.LoadStatUpgradeIcon((StatType) reward.id);
                        break;
                }
            }
        }

        public void RotateSkin()
        {
            itemIcon.rectTransform.Rotate(0,0,180);
        }
        
        [Button]
        public void SetNativeSize()
        {
            itemIcon.SetNativeSize();
        }
    }
}