﻿using System.Collections.Generic;
using Base.Core.GameResources;
using UnityEngine;

namespace UI.ItemReward
{
    public class ModuleItemShowReward : MonoBehaviour
    {
        [SerializeField] private RectTransform contentParent;
        [SerializeField] private ModuleItemReward itemSample;
        
        private List<ModuleItemReward> _allRewards = new List<ModuleItemReward>();

        public void InitData(List<Resource> rewards)
        {
            HideRewards();
            
            foreach (var reward in rewards)
            {
                var itemReward = GetItemReward();
                itemReward.InitData(reward);
            }
        }

        private ModuleItemReward GetItemReward()
        {
            foreach (var reward in _allRewards)
            {
                if (reward.gameObject.activeInHierarchy)
                    continue;

                reward.gameObject.SetActive(true);
                return reward;
            }

            var rewardClone = Instantiate(itemSample, contentParent, false);
            _allRewards.Add(rewardClone);
            rewardClone.gameObject.SetActive(true);
            return rewardClone;
        }

        private void HideRewards()
        {
            foreach (var reward in _allRewards)
            {
                reward.gameObject.SetActive(false);
            }
        }
    }
}