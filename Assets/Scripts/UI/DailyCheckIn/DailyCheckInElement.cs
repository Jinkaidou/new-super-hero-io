﻿using Base.Core;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using DataAccount;
using TMPro;
using UI.ItemReward;
using UnityEngine;
using UnityEngine.UI;

namespace UI.DailyCheckIn
{
    public class DailyCheckInElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI dayTxt;
        [SerializeField] private ModuleItemReward reward;
        [SerializeField] private Button claimableBtn;
        [SerializeField] private GameObject claimedObj;
        [SerializeField] private GameObject tomorrowObj;

        [SerializeField] private int dayId;

        private void Start()
        {
            dayTxt.text = $"DAY {dayId + 1}";
            claimableBtn.onClick.AddListener(OnClickClaim);
        }

        public void LoadData()
        {
            var claimTime = DataAccountPlayer.PlayerDailyCheckIn.GetDayClaimedTime(dayId);
            if (claimTime > 0)
            {
                claimedObj.SetActive(true);
                claimableBtn.gameObject.SetActive(false);
                tomorrowObj.SetActive(false);
                return;
            }

            bool isDayClaimable = DataAccountPlayer.PlayerDailyCheckIn.IsDayClaimable(dayId, out var lastTimeClaim);
            if (isDayClaimable)
            {
                claimableBtn.gameObject.SetActive(true);
                claimedObj.SetActive(false);
                tomorrowObj.SetActive(false);
                return;
            }
            
            if (lastTimeClaim == -1)
            {
                claimableBtn.gameObject.SetActive(false);
                claimedObj.SetActive(false);
                tomorrowObj.SetActive(DataAccountPlayer.PlayerDailyCheckIn.IsThisTomorrowDay(dayId));
                return;
            }

            claimableBtn.gameObject.SetActive(false);
            claimedObj.SetActive(false);
            tomorrowObj.SetActive(DataAccountPlayer.PlayerDailyCheckIn.IsThisTomorrowDay(dayId));
        }

        private void OnClickClaim()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            DataAccountPlayer.PlayerTutorialData.ChangeDailyValue(false);
            var isClaimable = DataAccountPlayer.PlayerDailyCheckIn.IsDayClaimable(dayId, out var lastTimeClaim);
            if (isClaimable)
            {
                DataAccountPlayer.PlayerDailyCheckIn.ClaimReward(dayId);
                this.PostEvent(EventID.DailyCheckInClaimReward);
                ResourceHelper.AddResource(reward.ResourceData);
                PopupController.Instance.ShowEarnReward((MoneyType) reward.ResourceData.id, reward.ResourceData.value,
                    reward.transform.position);
            }
        }
    }
}