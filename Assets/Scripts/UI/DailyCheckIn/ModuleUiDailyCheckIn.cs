﻿using System.Collections.Generic;
using Base.Core.Popup;
using DataAccount;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI.DailyCheckIn
{
    public class ModuleUiDailyCheckIn : PopupBase
    {
        [SerializeField] private Transform board;
        
        [SerializeField] private Button closeBtn;
        [SerializeField] private GameObject handTutDaily;
        [SerializeField] private List<DailyCheckInElement> allRewards = new List<DailyCheckInElement>();
        
        private void Start()
        {
            this.RegisterListener(EventID.DailyCheckInClaimReward, (sender, param) =>
            {
                RefreshRewards();
               
            });
            this.RegisterListener(EventID.DailyCheckInClaimReward, (sender, param) =>
            {
                SetTutorialDaily();
               
            });
            
            closeBtn.onClick.AddListener(Close);
        }

        protected override void OnShow()
        {
            Vector3 oldScale = board.localScale;
            board.localScale = Vector3.zero;
            board.DOScale(oldScale, 0.5f).SetUpdate(true);
            RefreshRewards();
            SetTutorialDaily();
        }

        private void SetTutorialDaily()
        {
            if (DataAccountPlayer.PlayerTutorialData.unlockDaily)
            {
                handTutDaily.gameObject.SetActive(true);
            }
            else
            {
                handTutDaily.gameObject.SetActive(false);
            }
        }
        
        protected override void OnHide()
        {
        }

        private void RefreshRewards()
        {
            foreach (var reward in allRewards)
            {
                reward.LoadData();
            }
        }
    }
}