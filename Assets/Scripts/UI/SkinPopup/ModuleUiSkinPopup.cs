﻿using System.Collections.Generic;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using Controller.LoadData;
using Model.DataHero;
using UnityEngine;

namespace UI.SkinPopup
{
    public class ModuleUiSkinPopup : PopupBase
    {
        [SerializeField] private UiSkinPopupButtons uiSkinPopupButtons;

        [SerializeField] private SkinElement skinElement;
        [SerializeField] private RectTransform skinParent;

        private HeroCollection _heroDataCollection;
        private List<SkinElement> _allElements = new List<SkinElement>();

        private void Awake()
        {
            _heroDataCollection = LoadResourceController.Instance.LoadHeroDataCollection();
            this.RegisterListener(EventID.SkinUsingSelected, (sender, param) =>
            {
                ReloadData();
            });
            this.RegisterListener(EventID.SkinRandomSuccess, (sender, param) =>
            {
                ReloadData();
            });
            this.RegisterListener(EventID.SkinUnlockSuccess, (sender, param) =>
            {
                ReloadData();
            });
        }

        protected override void OnShow()
        {
            PopupController.Instance.ShowHud();
            PopupController.Instance.GetHud().ShowBackButton(true);
        }

        protected override void OnHide()
        {
            PopupController.Instance.GetHud().ShowBackButton(false);
        }
        
        public void LoadSkin(SkinType skin)
        {
            var listSkins = _heroDataCollection.GetSkinCollection(skin, out var skinUnlockType);
            uiSkinPopupButtons.InitData(skinUnlockType);
            LoadElements(listSkins);

            bool isSelected = false;
            foreach (var element in _allElements)
            {
                if (element.IsSkinType(skin))
                {
                    element.OnClickSelect(false);
                    isSelected = true;
                    break;
                }
            }

            if (!isSelected)
            {
                _allElements[0].OnClickSelect(false);
            }
        }
        
        public void LoadNormalSkins()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            LoadElements(_heroDataCollection.normalSkins);
        }

        public void LoadGoldSkins()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            LoadElements(_heroDataCollection.goldSkins);
        }

        public void LoadPremiumSkins()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            LoadElements(_heroDataCollection.premiumSkins);
        }
        
        private void ReloadData()
        {
            foreach (var element in _allElements)
            {
                if (element.gameObject.activeSelf)
                    element.ReloadData();
            }
        }
        
        private void LoadElements(List<SkinData> data)
        {
            HideAllElement();

            foreach (var skinData in data)
            {
                var element = GetElement();
                element.InitData(skinData, true);
                element.gameObject.SetActive(true);
            }
        }

        private SkinElement GetElement()
        {
            foreach (var element in _allElements)
            {
                if (!element.gameObject.activeSelf)
                    return element;
            }

            var elementClone = Instantiate(skinElement, skinParent, false);
            _allElements.Add(elementClone);
            return elementClone;
        }

        private void HideAllElement()
        {
            foreach (var element in _allElements)
            {
                element.gameObject.SetActive(false);
            }
        }
    }
}