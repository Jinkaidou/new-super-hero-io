﻿using Base.Core.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace UI.SkinPopup
{
    public class UiSkinPopupButtons : MonoBehaviour
    {
        [SerializeField] private Button normalBtn;
        [SerializeField] private Button goldBtn;
        [SerializeField] private Button premiumBtn;

        [SerializeField] private GameObject normalSelected;
        [SerializeField] private GameObject goldSelected;
        [SerializeField] private GameObject premiumSelected;

        [SerializeField] private ModuleUiSkinPopup moduleUiSkinPopup;

        private SkinUnlockType _currentOpen;

        private void Awake()
        {
            normalBtn.onClick.AddListener(OnClickNormal);
            goldBtn.onClick.AddListener(OnClickGold);
            premiumBtn.onClick.AddListener(OnClickPremium);
        }

        public void InitData(SkinUnlockType skinUnlockType)
        {
            _currentOpen = skinUnlockType;
            LoadAllImages();
        }

        private void OnClickNormal()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            _currentOpen = SkinUnlockType.Normal;
            LoadAllImages();
            moduleUiSkinPopup.LoadNormalSkins();
        }

        private void OnClickGold()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            _currentOpen = SkinUnlockType.Gold;
            LoadAllImages();
            moduleUiSkinPopup.LoadGoldSkins();
        }

        private void OnClickPremium()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            _currentOpen = SkinUnlockType.Premium;
            LoadAllImages();
            moduleUiSkinPopup.LoadPremiumSkins();
        }

        private void LoadAllImages()
        {
            switch (_currentOpen)
            {
                case SkinUnlockType.Normal:
                    normalSelected.SetActive(true);
                    goldSelected.SetActive(false);
                    premiumSelected.SetActive(false);
                    break;
                case SkinUnlockType.Gold:
                    normalSelected.SetActive(false);
                    goldSelected.SetActive(true);
                    premiumSelected.SetActive(false);
                    break;
                case SkinUnlockType.Premium:
                    normalSelected.SetActive(false);
                    goldSelected.SetActive(false);
                    premiumSelected.SetActive(true);
                    break;
            }
        }
    }
}