﻿using Base.Core.GameResources;
using Base.Core.Sound;
using Controller.LoadData;
using DataAccount;
using Model.DataHero;
using UnityEngine;
using UnityEngine.UI;

namespace UI.SkinPopup
{
    public class SkinElement : MonoBehaviour
    {
        [SerializeField] private Image avatarIcon;
        [SerializeField] private Button selectBtn;

        [SerializeField] private GameObject selectingObj;
        [SerializeField] private GameObject selectedObj;
        [SerializeField] private GameObject tickIcon;
        [SerializeField] private GameObject lockObj;
        [SerializeField] private GameObject disable;
        private HeroCollection _heroDataCollection;

        private SkinData _skinData;

        private void Awake()
        {
            selectBtn.onClick.AddListener(() => OnClickSelect(true));
            _heroDataCollection = LoadResourceController.Instance.LoadHeroDataCollection();
            this.RegisterListener(EventID.SkinElementSelect, (sender, param) =>
            {
                var skinData = (SkinData) param;
                bool isTheSame = skinData.skinName == _skinData.skinName;
                selectingObj.SetActive(isTheSame);
            });
        }
        
        public bool IsSkinType(SkinType skin)
        {
            if (_skinData == null)
                return false;

            return _skinData.skinName == skin;
        }
        
        public void ReloadData()
        {
            InitData(_skinData);
        }

        public void CheckSkin(SkinData skinData)
        {
            var nomalSkin = _heroDataCollection.normalSkins;
            if (!nomalSkin.Contains(skinData))
            {
                disable.SetActive(false);
            }
        }
        
        public void InitData(SkinData skinData, bool refreshSelecting = false)
        {
           
            _skinData = skinData;
            avatarIcon.sprite = LoadResourceController.Instance.LoadSkinIcon(_skinData.skinName);

            var playerSkins = DataAccountPlayer.PlayerSkins;
            var isOwned = playerSkins.IsHaveOwnedSkin(_skinData.skinName);

            lockObj.SetActive(!isOwned);
            
            if (!isOwned)
            {
                disable.SetActive(true);
            }
            // if (_skinData.skinName == DataAccountPlayer.PlayerSkins.skinNameUnlock)
            // {
            //     disable.SetActive(false);
            // }
            if (refreshSelecting)
                selectingObj.SetActive(false);

            if (isOwned)
            {
                bool isUsing = playerSkins.skinUsing == _skinData.skinName;
                disable.SetActive(false);
                selectedObj.SetActive(isUsing);
                tickIcon.SetActive(isUsing);
            }
            else
            {
                selectedObj.SetActive(false);
                tickIcon.SetActive(false);
            }

            CheckSkin(_skinData);
        }
        
        public void OnClickSelect(bool playSound)
        {
            if (playSound)
                SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            
            this.PostEvent(EventID.SkinElementSelect, _skinData);
        }
    }
}