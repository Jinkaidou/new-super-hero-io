﻿using System.Collections;
using System.Collections.Generic;
using Base.Core.Ads;
using Base.Core.Analytics;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using Controller.LoadData;
using DataAccount;
using Model.DataHero;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.SkinPopup
{
    public class UiSkinPopupRightContent : MonoBehaviour
    {
        [SerializeField] private Image iconAvatar;
        
        [SerializeField] private GameObject selectedBtn;
        [SerializeField] private Button selectBtn;

        [SerializeField] private Button tryBtn;
        [SerializeField] private GameObject inTrial;

        [SerializeField] private Button getBtn;
        [SerializeField] private TextMeshProUGUI watchedAdsNumberTxt;

        [SerializeField] private Button unlockRandomBtn;
        [SerializeField] private TextMeshProUGUI unlockRandomGoldValueTxt;

        private SkinData _skinData;
        private HeroCollection _heroCollectionData;

        private void Awake()
        {
            this.RegisterListener(EventID.SkinElementSelect, (sender, param) =>
            {
                OnSkinElementSelect((SkinData) param);
            });

            selectBtn.onClick.AddListener(OnClickSelect);
            unlockRandomBtn.onClick.AddListener(OnClickUnlockRandomBtn);
            tryBtn.onClick.AddListener(OnClickTryBtn);
            getBtn.onClick.AddListener(OnClickGetBtn);

            _heroCollectionData = LoadResourceController.Instance.LoadHeroDataCollection();
        }

        private void OnDisable()
        {
            this.StopAllCoroutines();
        }

        #region Buttons

        private void OnClickSelect()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            var changeSuccess = DataAccountPlayer.PlayerSkins.OnChangeSkin(_skinData.skinName);
            FirebaseManager.Instance.LogSkinUsing(_skinData.skinName);
            if (changeSuccess)
            {
                this.PostEvent(EventID.SkinUsingSelected);
                LoadButtons();
            }
        }

        private void OnClickTryBtn()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            DataAccountPlayer.PlayerSkins.TrySkin = _skinData.skinName.ToString();
            FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
            FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
            // tryBtn.gameObject.SetActive(false);
            // inTrial.gameObject.SetActive(true);

            FirebaseManager.Instance.LogEvent(FirebaseParams.ENTER_ADS);
            if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            {
                PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                return;
            }
            
            // AdsManager.Instance.PlayRewardedAds((result) =>
            // {
            //     if (result != ShowAdResult.Finished)
            //     {
            //         PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
            //         return;
            //     }
            //     //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
            //     DataAccountPlayer.PlayerSkins.TrySkin = _skinData.skinName.ToString();
            //     FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
            //     FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
            //     tryBtn.gameObject.SetActive(false);
            //     inTrial.gameObject.SetActive(true);
            // });
            //
             if (!AdsManager.Instance.IsHaveVideoRewardedAds())
             {
                 PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                 return;
             }
            
             AdsManager.Instance.PlayRewardedAds((result) =>
             {
                 if (result != ShowAdResult.Finished)
                 {
                     PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                     return;
                 }
            
                 // FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsTrySkin);
                 DataAccountPlayer.PlayerSkins.TrySkin = _skinData.skinName.ToString();
                 
                 tryBtn.gameObject.SetActive(false);
                 inTrial.gameObject.SetActive(true);
             });
        }

        private void OnClickGetBtn()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
            FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
            //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
            // DataAccountPlayer.PlayerSkins.OnWatchAds(_skinData.skinName);
            // if (DataAccountPlayer.PlayerSkins.GetAdsWatchedNumber(_skinData.skinName) >= _skinData.numberOfAds)
            // {
            //     DataAccountPlayer.PlayerSkins.AddOwnedSkin(_skinData.skinName);
            //     this.PostEvent(EventID.SkinUnlockSuccess);
            // }

            LoadButtons();

            if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            {
                PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                return;
            }
            
            AdsManager.Instance.PlayRewardedAds((result) =>
            {
                FirebaseManager.Instance.LogEvent(FirebaseParams.ENTER_ADS);
                if (result != ShowAdResult.Finished)
                {
                    PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                    FirebaseManager.Instance.LogEvent(FirebaseParams.QUIT_ADS);
                    return;
                }
                FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
                //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
                DataAccountPlayer.PlayerSkins.OnWatchAds(_skinData.skinName);
                if (DataAccountPlayer.PlayerSkins.GetAdsWatchedNumber(_skinData.skinName) >= _skinData.numberOfAds)
                {
                    DataAccountPlayer.PlayerSkins.AddOwnedSkin(_skinData.skinName);
                    this.PostEvent(EventID.SkinUnlockSuccess);
                }

                LoadButtons();
    
            });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        }

        private void OnClickUnlockRandomBtn()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            FirebaseManager.Instance.LogEvent(FirebaseParams.GOLD_RANDOM);
            // Random..
            var skinData = LoadResourceController.Instance.LoadHeroDataCollection();
            var goldSkins = skinData.goldSkins;
            var skins = new List<SkinData>(goldSkins);      
            for (int i = skins.Count - 1; i >= 0; i--)
            {
                if (DataAccountPlayer.PlayerSkins.IsHaveOwnedSkin(skins[i].skinName))
                {
                    skins.RemoveAt(i);
                }
            }

            // SubGold
            int totalGoldNeed = skinData.baseCoinSkin;
            if (!DataAccountPlayer.PlayerMoney.IsEnough(MoneyType.Gold, totalGoldNeed))
            {
                PopupController.Instance.OpenNotification("Not enough Gold");
                return;
            }

            if (skins.Count <= 0)
            {
                PopupController.Instance.OpenNotification("Not have skin");
                return;
            }

            this.StartCoroutine(RandomSkin(skins, totalGoldNeed));
        }

        private IEnumerator RandomSkin(List<SkinData> remainSkins, int goldNeed)
        {
            var randomIndex = Random.Range(0, remainSkins.Count);

            float timer = 3f;
            float timeWait = 0.2f;
            while (timer > 0f)
            {
                for (int i = 0; i < remainSkins.Count; i++)
                {
                    this.PostEvent(EventID.SkinElementSelect, remainSkins[i]);
                    timer -= timeWait;
                    yield return new WaitForSeconds(timeWait);
                }

                timeWait += 0.05f;
            }

            this.PostEvent(EventID.SkinElementSelect, remainSkins[randomIndex]);
            DataAccountPlayer.PlayerMoney.SetMoney(false, MoneyType.Gold, goldNeed);
            // FirebaseManager.Instance.LogSpendCurrency(MoneyType.Gold, goldNeed, SpendSourceType.SkinUnlockRandom);

            // Add Skin.
            DataAccountPlayer.PlayerSkins.OnRandomSkinGold(remainSkins[randomIndex].skinName);
            LoadButtons();
            this.PostEvent(EventID.SkinRandomSuccess);
        }

        #endregion

        private void OnSkinElementSelect(SkinData skinData)
        {
            _skinData = skinData;
            // LoadSkin
            iconAvatar.sprite = LoadResourceController.Instance.LoadSkinIconSkin(skinData.skinName);
            // spine.initialSkinName = _skinData.skinName.ToString();
            // spine.Initialize(true);

            LoadButtons();
        }

        private void LoadButtons()
        {
            var playerSkins = DataAccountPlayer.PlayerSkins;
            var isOwned = playerSkins.IsHaveOwnedSkin(_skinData.skinName);
            
            if (isOwned)
            {
                var isUsing = playerSkins.skinUsing == _skinData.skinName;
                selectedBtn.SetActive(isUsing);
                selectBtn.gameObject.SetActive(!isUsing);

                tryBtn.gameObject.SetActive(false);
                inTrial.SetActive(false);
                
                unlockRandomBtn.gameObject.SetActive(false);
                getBtn.gameObject.SetActive(false);
            }
            else
            {
                selectBtn.gameObject.SetActive(false);
                selectedBtn.SetActive(false);

                if (_heroCollectionData.IsGoldSkin(_skinData.skinName))
                {
                    getBtn.gameObject.SetActive(false);
                    unlockRandomBtn.gameObject.SetActive(true);
                    
                    bool isSkinInTrial = DataAccountPlayer.PlayerSkins.TrySkin == _skinData.skinName.ToString();
                    tryBtn.gameObject.SetActive(!isSkinInTrial);
                    inTrial.SetActive(isSkinInTrial);
                    
                    // set unlock gold reward.
                    int totalGoldNeed = _heroCollectionData.baseCoinSkin;
                    unlockRandomGoldValueTxt.text = totalGoldNeed.ToString();
                }
                else
                {
                    unlockRandomBtn.gameObject.SetActive(false);

                    bool isUnlocked;
                    bool isPremium = _heroCollectionData.IsPremiumSkin(_skinData.skinName);
                    if (isPremium)
                    {
                        isUnlocked = true;
                    }
                    else
                    {
                        // Normal Skins
                        isUnlocked = DataAccountPlayer.PlayerSkins.IsSkinNormalUnlocked(_skinData.skinName);
                    }

                    if (isUnlocked)
                    {
                        tryBtn.gameObject.SetActive(false);
                        inTrial.SetActive(false);
                    }
                    else
                    {
                        bool isSkinInTrial = DataAccountPlayer.PlayerSkins.TrySkin == _skinData.skinName.ToString();
                        tryBtn.gameObject.SetActive(!isSkinInTrial);
                        inTrial.SetActive(isSkinInTrial);
                    }

                    getBtn.gameObject.SetActive(isUnlocked);
                    watchedAdsNumberTxt.text =
                        $"{playerSkins.GetAdsWatchedNumber(_skinData.skinName)}/{_skinData.numberOfAds}";
                }
            }
        }
    }
}