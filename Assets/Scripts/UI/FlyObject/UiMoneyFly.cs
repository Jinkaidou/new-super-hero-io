﻿using Base.Core.GameResources;
using Controller.LoadData;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI.FlyObject
{
    public class UiMoneyFly : MonoBehaviour
    {
        [SerializeField] private Image moneyIcon;

        public void InitData(MoneyType moneyType, Vector3 targetPosition, float duration)
        {
            moneyIcon.sprite = LoadResourceController.Instance.LoadMoneyIcon(moneyType);

            float randomX = Random.Range(-150, 150);
            float randomY = Random.Range(-150, 150);
            var position = transform.position;
            var firstMove = new Vector2(position.x + randomX, position.y + randomY);
            transform.DOMove(firstMove, 0.5f)
                .OnComplete(() =>
                {
                    transform.DOMove(targetPosition, duration)
                        .OnComplete(() =>
                        {
                            gameObject.SetActive(false);
                        });
                });
        }
    }
}