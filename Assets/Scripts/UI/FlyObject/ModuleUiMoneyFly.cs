﻿using System.Collections;
using System.Collections.Generic;
using Base.Core.GameResources;
using UnityEngine;

namespace UI.FlyObject
{
    public class ModuleUiMoneyFly : MonoBehaviour
    {
        [SerializeField] private UiMoneyFly moneySample;
        [SerializeField] private Transform goldParent;

        private List<UiMoneyFly> _allMoneyFlies = new List<UiMoneyFly>();

        public void InitData(MoneyType moneyType, int number, Vector3 startPos, Vector3 targetPos)
        {
            switch (moneyType)
            {
                case MoneyType.Gold:
                    var numberOfGold = number / 5;
                    if (numberOfGold <= 0)
                    {
                        numberOfGold = 1;
                    }
                    StartCoroutine(SpawnMoneyCoroutine(moneyType, numberOfGold, startPos, targetPos));
                    break;
                case MoneyType.Life:
                    StartCoroutine(SpawnMoneyCoroutine(moneyType, number, startPos, targetPos));
                    break;
                default:
                    gameObject.SetActive(false);
                    break;
            }
        }

        private IEnumerator SpawnMoneyCoroutine(MoneyType moneyType, int number, Vector3 startPos, Vector3 targetPos)
        {
            float waitSecond = 1 / (float) number;
            for (int i = 0; i < number; i++)
            {
                var moneyFly = GetMoneyFly();
                moneyFly.transform.position = startPos;
                moneyFly.gameObject.SetActive(true);
                moneyFly.InitData(moneyType, targetPos, 1f);

                yield return new WaitForSeconds(waitSecond);
            }

            yield return new WaitForSeconds(1.5f);
            gameObject.SetActive(false);
        }

        private UiMoneyFly GetMoneyFly()
        {
            foreach (var moneyFly in _allMoneyFlies)
            {
                if (!moneyFly.gameObject.activeInHierarchy)
                {
                    return moneyFly;
                }
            }

            var moneyClone = Instantiate(moneySample, goldParent, false);
            _allMoneyFlies.Add(moneyClone);
            return moneyClone;
        }
    }
}