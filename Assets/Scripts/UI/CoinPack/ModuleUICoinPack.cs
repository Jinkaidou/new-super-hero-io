using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.MainMenu
{
    public class ModuleUICoinPack : MonoBehaviour
    {
        [SerializeField] private Button back;

        private void Awake()
        {
            back.onClick.AddListener(OnClickBattle);
        }

        private void OnClickBattle()
        {
            this.gameObject.SetActive(false);
        }
    }
}