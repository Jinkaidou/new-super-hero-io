using Base.Core.GameResources;
using DataAccount;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.MainMenu
{
    public class ModuleUICoinPackElement : MonoBehaviour
    {
        [SerializeField] private Button purchase;
        [SerializeField] private TextMeshProUGUI priceTxt;
        [SerializeField] private TextMeshProUGUI valueTxt;
        [SerializeField] private int value;
        [SerializeField] private float price;
        

        private void Awake()
        {
            priceTxt.text = price.ToString() + "$";
            valueTxt.text = value.ToString() + " coin";
            purchase.onClick.AddListener(OnClickPurchase);
        }

        private void OnClickPurchase()
        {
            DataAccountPlayer.PlayerMoney.SetMoney(true, MoneyType.Gold, value);
        }
    }
}