﻿using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace UI.Toast
{
    public class ModuleUiToast : MonoBehaviour
    {
        [SerializeField] private RectTransform startPos;
        [SerializeField] private RectTransform targetPos;
        [SerializeField] private TextMeshProUGUI contentTxt;
        [SerializeField] private RectTransform toast;

        private Coroutine _revertCor;

        public void InitData(string content)
        {
            toast.anchoredPosition = startPos.anchoredPosition;
            contentTxt.text = content;
            toast.DOAnchorPos(targetPos.anchoredPosition, 0.5f);

            _revertCor = StartCoroutine(Revert());
        }

        private IEnumerator Revert()
        {
            yield return new WaitForSeconds(3f);
            toast.DOAnchorPos(startPos.anchoredPosition, 0.5f)
                .OnComplete(() =>
                {
                    gameObject.SetActive(false);
                });
        }

        private void OnDisable()
        {
            if (_revertCor != null)
            {
                StopCoroutine(_revertCor);
                _revertCor = null;
            }
        }
    }
}