﻿using System.Collections;
using System.Collections.Generic;
using Base.Core;
using Base.Core.Ads;
using Base.Core.Analytics;
using Base.Core.GameResources;
using Base.Core.Popup;
using DataAccount;
using TMPro;
using UI.EndGameRewardPopup;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI.RevivePopup
{
    public class ModuleUiRevivePopup : PopupBase
    {
        [SerializeField] private TextMeshProUGUI titleTxt;
        [SerializeField] private TextMeshProUGUI countTimeTxt;
        [SerializeField] private Button giveUpBtn;
        [SerializeField] private Button reviveBtn;
        [SerializeField] private TextMeshProUGUI reviveRemainTxt;
        [SerializeField] private GameObject _adsImage;

        public List<string> titleLoop = new List<string>();

        private const int CountTime = 10;
        private Coroutine _countTimeCor;

        private void Awake()
        {
            giveUpBtn.onClick.AddListener(OnClickGiveUp);
            reviveBtn.onClick.AddListener(OnClickRevive);
            CheckTutorial();
        }


        private void CheckTutorial()
        {
            if (DataAccountPlayer.PlayerTutorialData.firstTimeRevive)
            { 
                //reviveBtn.gameObject.SetActive(false);
            }
        }
        
        public void InitData(int currentLife, int totalRevive)
        {
            reviveRemainTxt.text = $"Revive {currentLife}/{totalRevive}";
            if (DataAccountPlayer.PlayerTutorialData.countPlayGame <= 2)
            {
                _adsImage.SetActive(false);
            }
            else
            {
                _adsImage.SetActive(true);
            }
            if (currentLife <= 0)
            {
                reviveBtn.interactable = false;
            }
        }

        protected override void OnShow()
        {
            titleTxt.text = titleLoop[Random.Range(0, titleLoop.Count)];
            _countTimeCor = StartCoroutine(CountReviveTime());
        }

        protected override void OnHide()
        {
            if (_countTimeCor != null)
            {
                StopCoroutine(_countTimeCor);
                _countTimeCor = null;
            }
        }

        private IEnumerator CountReviveTime()
        {
            int timer = CountTime;
            while (timer >= 0)
            {
                Timer(timer);
                yield return new WaitForSeconds(1f);

                timer -= 1;
            }

            OnClickGiveUp();
        }

        private void Timer(int timer)
        {
            countTimeTxt.text = $"Continue?\n{timer}";
        }

        private void OnClickGiveUp()
        {
           
            Close();
            var giveupPopup =
                (ModuleUiEndGameRewardPopup) PopupController.Instance.OpenPopupAndKeepParent(PopupType
                    .EndGameRewardPopup);
            var statParams = new StatParamMessage();
            statParams.Point = DataAccountPlayer.PlayerStatistic.point;
            statParams.Kill = DataAccountPlayer.PlayerStatistic.kill;
            statParams.Time = DataAccountPlayer.PlayerStatistic.time;
            giveupPopup.InitData(statParams, DataAccountPlayer.PlayerStatistic.goldLastRound);
            AdsManager.Instance.PlayInterstitialAds((result) =>
            {
                if (result != ShowAdResult.Finished)
                {
                    PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                    return;
                }
                FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
            });
            FirebaseManager.Instance.LogEvent(FirebaseParams.QUITDONE);
            
            
            DataAccountPlayer.PlayerTutorialData.ChangeReviveValue(false);
            DataAccountPlayer.PlayerTutorialData.ChangeValueEndGame();
            
            // Close();
            // var giveupPopup =
            //     (ModuleUiEndGameRewardPopup) PopupController.Instance.OpenPopupAndKeepParent(PopupType
            //         .EndGameRewardPopup);
            // var statParams = new StatParamMessage();
            // statParams.Point = DataAccountPlayer.PlayerStatistic.point;
            // statParams.Kill = DataAccountPlayer.PlayerStatistic.kill;
            // statParams.Time = DataAccountPlayer.PlayerStatistic.time;
            // giveupPopup.InitData(statParams, DataAccountPlayer.PlayerStatistic.goldLastRound);
            
        }

        private void OnClickRevive()
        {
            PlayAdsReward();
            Close();
           
        }

        private void PlayAdsReward()
        {
            if (DataAccountPlayer.PlayerTutorialData.countPlayGame <= 2)
            {
                //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
                this.PostEvent(EventID.ReviveSuccess);
            }
            else
            {
                FirebaseManager.Instance.LogEvent(FirebaseParams.ENTER_ADS);
                FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
                //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
                //this.PostEvent(EventID.ReviveSuccess);
                if (!AdsManager.Instance.IsHaveVideoRewardedAds())
                {
                    PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                    return;
                }
                AdsManager.Instance.PlayRewardedAds((result) =>
                {
                    if (result != ShowAdResult.Finished)
                    {
                        PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                        return;
                    }
                    FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                    FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
                    //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
                    this.PostEvent(EventID.ReviveSuccess);
                });
            }
           
        }
    }
}