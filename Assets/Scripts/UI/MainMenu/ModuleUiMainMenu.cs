﻿using Base.Core;
using Base.Core.Popup;
using Base.Core.Sound;
using DataAccount;
using TMPro;
using UI.LoadingScene;
using UI.SkinPopup;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using Base.Core.Analytics;

namespace UI.MainMenu
{
    public class ModuleUiMainMenu : PopupBase
    {
        [Header("Bottom Buttons")]
        [SerializeField] private Button battleBtn;
        [SerializeField] private Button skinBtn;
        [SerializeField] private Button statUpgradeBtn;

        [Header("Right Buttons")]
        [SerializeField] private Button dailyCheckInBtn;
        [SerializeField] private Button spinBtn;
        [SerializeField] private Button coinPack;
        [SerializeField] private ModuleUICoinPack coinPackPopUp;

        [Header("Other Buttons")]
        [SerializeField] private Button settingBtn;

        [Header("Change Name")] 
        [SerializeField] private TMP_InputField playerNameInput;
        [SerializeField] private TextMeshProUGUI timeTxt;
        [SerializeField] private TextMeshProUGUI killTxt;
        [SerializeField] private TextMeshProUGUI pointTxt;
        
        [SerializeField] private TextMeshProUGUI bestTimeTxt;
        [SerializeField] private TextMeshProUGUI bestKillTxt;
        [SerializeField] private TextMeshProUGUI bestPointTxt;
        [SerializeField] private GameObject darkPanel;
        
        [SerializeField] private GameObject greenArrowBattle;
        [SerializeField] private GameObject greenArrowUpgrade;

        private void Awake()
        {
            this.RegisterListener(EventID.StatUpgradeSuccessFree, (sender, param) =>
            {
                SetPanelTutourial();
            });
        }

        private void Start()
        {
            battleBtn.onClick.AddListener(OnClickBattle);
            skinBtn.onClick.AddListener(OnClickSkin);
            statUpgradeBtn.onClick.AddListener(OnClickStatUpgrade);

            dailyCheckInBtn.onClick.AddListener(OnClickDailyCheckIn);
            spinBtn.onClick.AddListener(OnClickSpin);
            coinPack.onClick.AddListener(OnClickCoinPack);

            settingBtn.onClick.AddListener(OnClickSetting);
            
            playerNameInput.onEndEdit.AddListener(OnPlayerNameEndEdit);
            SetPanelTutourial();

        }

        private void OnEnable()
        {
            SetPanelTutourial();
        }

        protected override void OnShow()
        {
            PopupController.Instance.ShowHud();
            PopupController.Instance.GetHud().ShowBackButton(false);
            LoadStatistics();
        }

        protected override void OnHide()
        {
        }


        private void SetPanelTutourial()
        {
            SetDefault();
            if (DataAccountPlayer.PlayerTutorialData.firstTimeBattle)
            {
                greenArrowBattle.SetActive(true);
                darkPanel.SetActive(true);
                darkPanel.GetComponent<RectTransform>().SetSiblingIndex(7);
            }
            else if (DataAccountPlayer.PlayerTutorialData.unlockUpgrade)
            {
                greenArrowUpgrade.SetActive(true);
                darkPanel.SetActive(true);
                battleBtn.interactable = false;
                skinBtn.interactable = false;
                darkPanel.GetComponent<RectTransform>().SetSiblingIndex(2);
            }
            else if(DataAccountPlayer.PlayerTutorialData.numberDead == 2 && DataAccountPlayer.PlayerTutorialData.unlockSpin)
            {
                OnClickSpin();
            }
            else if(DataAccountPlayer.PlayerTutorialData.numberDead == 3 && DataAccountPlayer.PlayerTutorialData.unlockDaily)
            {
                OnClickDailyCheckIn();
            }
        }

        private void SetDefault()
        {
            darkPanel.SetActive(false);
            greenArrowUpgrade.SetActive(false);
            greenArrowBattle.SetActive(false);
            battleBtn.interactable = true;
            skinBtn.interactable = true;
        }
        
        private void LoadStatistics()
        {
            playerNameInput.text = DataAccountPlayer.PlayerStatistic.playerName;
            
            var timeSpan = TimeSpan.FromSeconds(DataAccountPlayer.PlayerStatistic.time);
            timeTxt.text = timeSpan.ToString(@"mm\:ss");
            killTxt.text = DataAccountPlayer.PlayerStatistic.kill.ToString();
            pointTxt.text = DataAccountPlayer.PlayerStatistic.point.ToString();

            var bestTimeSpan = TimeSpan.FromSeconds(DataAccountPlayer.PlayerStatistic.bestTime);
            bestTimeTxt.text = bestTimeSpan.ToString(@"mm\:ss");
            bestKillTxt.text = DataAccountPlayer.PlayerStatistic.bestKill.ToString();
            bestPointTxt.text = DataAccountPlayer.PlayerStatistic.bestPoint.ToString();
        }

        private void OnClickBattle()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            GameManager.Instance.LoadScene(SceneName.GamePlayScreen);
            DataAccountPlayer.PlayerTutorialData.changeFirstTimeBattle(false);
            DataAccountPlayer.PlayerTutorialData.ChangeCountPlayGame();
            FirebaseManager.Instance.LogEvent(FirebaseParams.BUTTON_PLAY);
        }

        private void OnClickSkin()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            var skinPopup = (ModuleUiSkinPopup) PopupController.Instance.OpenPopupAndCloseParentWithHud(PopupType.SkinPopup);
            skinPopup.LoadSkin(DataAccountPlayer.PlayerSkins.skinUsing);
            FirebaseManager.Instance.LogEvent(FirebaseParams.BUTTON_SKIN);
        }

        private void OnClickStatUpgrade()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            PopupController.Instance.OpenPopupAndCloseParentWithHud(PopupType.StatUpgradePopup);
            DataAccountPlayer.PlayerTutorialData.ChangeUpgradeValue(false);
            FirebaseManager.Instance.LogEvent(FirebaseParams.BUTTON_UPGRADE);
        }

        private void OnClickSetting()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            PopupController.Instance.OpenPopupAndKeepParent(PopupType.SettingPopup);
        }

        private void OnClickDailyCheckIn()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            PopupController.Instance.OpenPopupAndKeepParent(PopupType.DailyCheckInPopup);
        }

        private void OnClickSpin()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            PopupController.Instance.OpenPopupAndKeepParent(PopupType.SpinPopup);
        }
        
        private void OnClickCoinPack()
        {
            coinPackPopUp.gameObject.SetActive(true);
        }
        private void OnPlayerNameEndEdit(string currentName)
        {
            FirebaseManager.Instance.LogEvent(FirebaseParams.BUTTON_NAME);
            if (string.IsNullOrEmpty(currentName) || string.IsNullOrWhiteSpace(currentName))
            {
                PopupController.Instance.OpenNotification(ContentString.NameInvalid);
                playerNameInput.text = DataAccountPlayer.PlayerStatistic.playerName;
                return;
            }
            
            // Save New Name;
            PopupController.Instance.OpenNotification(ContentString.NameChangeSuccess);
            DataAccountPlayer.PlayerStatistic.OnChangeName(currentName);
        }
    }
}