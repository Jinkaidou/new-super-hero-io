﻿using Base.Core.Popup;
using UnityEngine;

namespace UI.MainMenu
{
    public class ManagerHomeMenu : MonoBehaviour
    {
        private void Start()
        {
            PopupController.Instance.OpenPopupAndKeepParent(PopupType.MainMenu);
        }
    }
}