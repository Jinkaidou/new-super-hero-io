﻿using TMPro;
using UnityEngine;

namespace UI.EndGameRewardPopup
{
    public class StatElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI titleTxt;
        [SerializeField] private TextMeshProUGUI valueTxt;

        public void InitData(string title, string value)
        {
            titleTxt.text = title;
            valueTxt.text = value;
        }
    }
}