﻿using System;

namespace UI.EndGameRewardPopup
{
    public class StatParamMessage
    {
        public float Point;
        public int Kill;
        public long Time;

        public string GetStatValue(StatParam statParam)
        {
            switch (statParam)
            {
                case StatParam.Point:
                    return Point.ToString();
                case StatParam.Kill:
                    return Kill.ToString();
                case StatParam.Time:
                    var timeSpan = TimeSpan.FromSeconds(Time);
                    return timeSpan.ToString(@"mm\:ss");
            }

            return string.Empty;
        }
    }

    public enum StatParam
    {
        Point,
        Kill,
        Time,
    }
}