﻿using System;
using System.Collections.Generic;
using Base.Core;
using Base.Core.Ads;
using Base.Core.Analytics;
using Base.Core.GameResources;
using Base.Core.Popup;
using Controller.LoadData;
using DataAccount;
using DG.Tweening;
using Model.DataHero;
using TMPro;
using UI.EndGameNewSkinUnlock;
using UI.LoadingScene;
using UnityEngine;
using UnityEngine.UI;

namespace UI.EndGameRewardPopup
{
    public class ModuleUiEndGameRewardPopup : PopupBase
    {
        [SerializeField] private StatElement statElementSample;
        [SerializeField] private RectTransform statParentContent;

        [SerializeField] private TextMeshProUGUI playerNameTxt;
        [SerializeField] private Button closeBtn;
        [SerializeField] private Button watchAdBtn;
        [SerializeField] private Button freeBtn;
        [SerializeField] private TextMeshProUGUI goldAddTxt;
        [SerializeField] private TextMeshProUGUI goldAddFreeTxt;
        [SerializeField] private Animator pointer;

        [SerializeField] private TextMeshProUGUI skinUnlockPercentTxt;
        [SerializeField] private Image skinUnlockFill;

        [SerializeField] private TextMeshProUGUI killTxt;
        [SerializeField] private TextMeshProUGUI pointTxt;
        [SerializeField] private TextMeshProUGUI timeTxt;
        
        private int _goldEarn = 100;
        private List<StatElement> _allStatElements = new List<StatElement>();
        private List<SkinData> _normalSkins;

        private void Awake()
        {
            watchAdBtn.onClick.AddListener(OnClickWatchAd);
            freeBtn.onClick.AddListener(OnclickFree);
            closeBtn.onClick.AddListener(OnClickClose);

            _normalSkins = LoadResourceController.Instance.LoadHeroDataCollection().normalSkins;
        }

        protected override void OnShow()
        {
            playerNameTxt.text = DataAccountPlayer.PlayerStatistic.playerName;
            LoadSkinProcess();
        }

        protected override void OnHide()
        {
        }

        public void InitData(StatParamMessage statParamMessage, int goldEarn)
        {
            _goldEarn = goldEarn;
            LoadStats(statParamMessage);
            var check = DataAccountPlayer.PlayerTutorialData;
            if (check.numberDead <= 1)
            {
                freeBtn.gameObject.SetActive(true);
                watchAdBtn.gameObject.SetActive(false);
            }
            else
            {
                freeBtn.gameObject.SetActive(false);
                watchAdBtn.gameObject.SetActive(true);
            }
        }

        #region Stats

        private void LoadStats(StatParamMessage statParamMessage)
        {
            HideAllElements();
            
            foreach (StatParam statParam in Enum.GetValues(typeof(StatParam)))
            {
                string value = statParamMessage.GetStatValue(statParam);
                var statElement = GetStatElement();
                statElement.InitData(statParam.ToString(), value);
            }
        }

        private StatElement GetStatElement()
        {
            foreach (var element in _allStatElements)
            {
                if (element.gameObject.activeInHierarchy)
                    continue;

                element.gameObject.SetActive(true);
                return element;
            }

            var elementClone = Instantiate(statElementSample, statParentContent, false);
            _allStatElements.Add(elementClone);
            elementClone.gameObject.SetActive(true);
            return elementClone;
        }
        
        private void HideAllElements()
        {
            foreach (var element in _allStatElements)
            {
                element.gameObject.SetActive(false);
            }
        }

        #endregion

        #region Pointer X Reward

        private float _x5Value = 60f;
        private float _x3Value = 220f;
        // private float _x2Value = 435f;
        private int _currentGoldValue;

        private void FixedUpdate()
        {
            CheckGold();
        }

        private void CheckGold()
        {
            var currentPos = Mathf.Abs(pointer.transform.localPosition.x);
            if (currentPos <= _x5Value)
            {
                // x5 value
                if (_currentGoldValue != _goldEarn * 5)
                {
                    _currentGoldValue = _goldEarn * 5;
                    goldAddTxt.text = _currentGoldValue.ToString();
                    goldAddFreeTxt.text = _currentGoldValue.ToString();
                    
                }

                return;
            }

            if (currentPos > _x5Value && currentPos <= _x3Value)
            {
                // x3 value
                if (_currentGoldValue != _goldEarn * 3)
                {
                    _currentGoldValue = _goldEarn * 3;
                    goldAddTxt.text = _currentGoldValue.ToString();
                    goldAddFreeTxt.text = _currentGoldValue.ToString();
                }
                
                return;
            }
            
            // x2 value
            if (_currentGoldValue != _goldEarn * 2)
            {
                _currentGoldValue = _goldEarn * 2;
                goldAddTxt.text = _currentGoldValue.ToString();
                goldAddFreeTxt.text = _currentGoldValue.ToString();
            }
        }

        #endregion

        #region Skin Unlock

        private const float MaxProgress = 4;
        
        private void LoadSkinProcess()
        {
            SkinType skinUnlocking = DataAccountPlayer.PlayerSkins.GetCurrentNormalSkinUnlocking(out int processNumb);
            var skinName = SkinType.none;
            if (skinUnlocking == SkinType.none)
            {
                // Create new skin to unlock.
                bool isHaveSkin = false;
                foreach (var skinData in _normalSkins)
                {
                    bool isUnlocked = DataAccountPlayer.PlayerSkins.IsSkinNormalUnlocked(skinData.skinName);
                    if (!isUnlocked)
                    {
                        isHaveSkin = true;
                        DataAccountPlayer.PlayerSkins.OnUpdateUnlockNormalSkin(skinData.skinName);
                        skinUnlockFill.sprite = LoadResourceController.Instance.LoadSkinIcon(skinData.skinName);
                        skinName = skinData.skinName;
                        break;
                    }
                }

                if (!isHaveSkin)
                {
                    skinUnlockFill.transform.parent.gameObject.SetActive(false);
                    return;
                }
            }
            else
            {
                DataAccountPlayer.PlayerSkins.OnUpdateUnlockNormalSkin(skinUnlocking);
                skinUnlockFill.sprite = LoadResourceController.Instance.LoadSkinIcon(skinUnlocking);
            }

            int oldProcess = processNumb;
            float currentPercent = oldProcess / MaxProgress;
            float nextPercent = (oldProcess + 1) / MaxProgress;
            
            skinUnlockPercentTxt.text = $"{currentPercent * 100 :N0}%";
            skinUnlockFill.fillAmount = currentPercent;
            
            DOTween.To(() => currentPercent, value => currentPercent = value, nextPercent, 2f)
                .OnUpdate(() =>
                {
                    skinUnlockPercentTxt.text = $"{currentPercent * 100 :N0}%";
                    if (skinName != SkinType.none)
                    {
                        skinUnlockFill.sprite = LoadResourceController.Instance.LoadSkinIcon(skinName);
                        DataAccountPlayer.PlayerSkins.ChangeSkinUnlock(skinName);
                    }
                    else if(skinUnlocking != SkinType.none)
                    {
                        skinUnlockFill.sprite = LoadResourceController.Instance.LoadSkinIcon(skinUnlocking);
                        DataAccountPlayer.PlayerSkins.ChangeSkinUnlock(skinUnlocking);
                    }
                   
                    skinUnlockFill.fillAmount = currentPercent;
                })
                .SetUpdate(true)
                .OnComplete(() =>
                {
                    if (oldProcess + 1 >= MaxProgress)
                    {
                        skinUnlockFill.sprite = LoadResourceController.Instance.LoadSkinIcon(skinUnlocking);
                        OnNewSkinUnlocked(skinUnlocking);
                    }
                });
        }

        private void OnNewSkinUnlocked(SkinType skinType)
        {
            var popUp = (ModuleUiEndGameNewSkinUnlockPopup) PopupController.Instance.OpenPopupAndKeepParent(PopupType.EndGameNewSkinUnlock);
            popUp.InitData(skinType);
        }

        #endregion

        #region Button Clicks

        private void OnClickClose()
        {
            GameManager.Instance.LoadScene(SceneName.HomeScene);
        }

        private void OnclickFree()
        {
            FirebaseManager.Instance.LogEvent(FirebaseParams.EndGameTakeGold);
            DataAccountPlayer.PlayerTutorialData.ChangeUpgradeValue(true);
            freeBtn.interactable = false;
            pointer.enabled = false;
            ResourceHelper.AddResource(new Resource(ResourceType.Money, (int)MoneyType.Gold, _currentGoldValue));
            PopupController.Instance.ShowEarnReward(MoneyType.Gold, _currentGoldValue, goldAddTxt.transform.position);
            PopupController.Instance.GetHud().ShowBackButton(false);
            this.StartDelayMethod(3f, OnClickClose);
        }
        
        private void OnClickWatchAd()
        {
            watchAdBtn.interactable = false;
            pointer.enabled = false;
            // Watch Ads => if success => Add reward
            // if not success => pointer.enabled = true
            // ResourceHelper.AddResource(new Resource(ResourceType.Money, (int)MoneyType.Gold, _currentGoldValue));
            // PopupController.Instance.ShowEarnReward(MoneyType.Gold, _currentGoldValue, goldAddTxt.transform.position);
            // PopupController.Instance.GetHud().ShowBackButton(false);
            // this.StartDelayMethod(1.5f, OnClickClose);
            
            if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            {
                PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                return;
            }
            AdsManager.Instance.PlayRewardedAds((result) =>
            {
                if (result != ShowAdResult.Finished)
                {
                    PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                    return;
                }
                FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
                //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
                watchAdBtn.interactable = false;
                pointer.enabled = false;
                // Watch Ads => if success => Add reward
                // if not success => pointer.enabled = true
                ResourceHelper.AddResource(new Resource(ResourceType.Money, (int) MoneyType.Gold, _currentGoldValue));
                PopupController.Instance.ShowEarnReward(MoneyType.Gold, _currentGoldValue, goldAddTxt.transform.position);
                PopupController.Instance.GetHud().ShowBackButton(false);
                this.StartDelayMethod(3f, OnClickClose);
            });
        }

        #endregion
    }
}