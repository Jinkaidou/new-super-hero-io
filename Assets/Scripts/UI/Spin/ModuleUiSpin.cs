﻿using System;
using System.Collections;
using System.Collections.Generic;
using Base.Core;
using Base.Core.Ads;
using Base.Core.Analytics;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using DataAccount;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI.Spin
{
    public class ModuleUiSpin : PopupBase
    {
        [SerializeField] private Button spinBtn;
        [SerializeField] private Button watchAdBtn;
        [SerializeField] private Transform wheel;

        [SerializeField] private TextMeshProUGUI cooldownTxt;
        [SerializeField] private GameObject blockCanvas;
        [SerializeField] private GameObject handTutSpin;
        public GameObject star;
        public List<SpinElement> rewards = new List<SpinElement>();

        private float _totalValue;
        private const int NumberOfAdCanWatch = 5;
        private Coroutine _countDownCor;
        private Coroutine _playSoundCor;
        private bool _backOpen;
        
        private void Awake()
        {
            spinBtn.onClick.AddListener(OnClickSpin);
            watchAdBtn.onClick.AddListener(OnClickWatchAdSpin);
            CheckTutSpin();
            _backOpen = true;
            foreach (var spinElement in rewards)
            {
                _totalValue += spinElement.Percent;
            }

            float startPercent = 0f;
            foreach (var element in rewards)
            {
                var percentAdd = element.Percent / _totalValue;
                element.SetRangePercent(startPercent, startPercent + percentAdd);
                startPercent += percentAdd;
            }
        }

        private void Update()
        {
            PopupController.Instance.GetHud().ShowBackButton(_backOpen);
        }

        private void CheckTutSpin()
        {
            if (DataAccountPlayer.PlayerTutorialData.unlockSpin)
            {
                handTutSpin.gameObject.SetActive(true);
                
            }
            else
            {
                handTutSpin.gameObject.SetActive(false);
            }
        }
        
        private void OnClickWatchAdSpin()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            FirebaseManager.Instance.LogEvent(FirebaseParams.ENTER_ADS);
            _backOpen = false;
            // Spin((SpinElement reward) =>
            // {
            //     DataAccountPlayer.PlayerSpin.OnWatchAdSpin();
            //     LoadData();
            //
            //     if (reward.Resource.resourceType == ResourceType.Money)
            //     {
            //         // var moduleUi = (ModuleUiSpinReward) PopupController.Instance.OpenPopupAndKeepParent(PopupType.SpinRewardPopup);
            //         // moduleUi.InitData(reward.Resource);
            //         ResourceHelper.AddResource(reward.Resource);
            //         PopupController.Instance.ShowEarnReward((MoneyType) reward.Resource.id, reward.Resource.value,
            //             reward.RewardTransform);
            //     }
            //     else if (reward.Resource.resourceType == ResourceType.Skin)
            //     {
            //         ResourceHelper.AddResource(reward.Resource);
            //         PopupController.Instance.ShowRewardPopup(reward.Resource);
            //     }
            //     else
            //     {
            //         ResourceHelper.AddResource(reward.Resource);
            //         PopupController.Instance.ShowRewardPopup(reward.Resource);
            //     }
            // });
            
            if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            {
                PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                return;
            }
            
            AdsManager.Instance.PlayRewardedAds((showAdResult) =>
            {
                if (showAdResult != ShowAdResult.Finished)
                {
                    PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                    return;
                }
            
                FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForSpin);
                Spin((SpinElement reward) =>
                {
                    DataAccountPlayer.PlayerSpin.OnNormalSpin();
                    LoadData();

                    if (reward.Resource.resourceType == ResourceType.Money)
                    {
                        // var moduleUi = (ModuleUiSpinReward) PopupController.Instance.OpenPopupAndKeepParent(PopupType.SpinRewardPopup);
                        // moduleUi.InitData(reward.Resource);
                        star.gameObject.SetActive(true);
                        ResourceHelper.AddResource(reward.Resource);
                        PopupController.Instance.ShowEarnReward((MoneyType) reward.Resource.id, reward.Resource.value,
                            reward.RewardTransform);
                        LeanTween.delayedCall(0.7f, () =>
                        {
                            star.gameObject.SetActive(false);
                            _backOpen = true;
                        });
                    }
                    else
                    {
                        ResourceHelper.AddResource(reward.Resource);
                        PopupController.Instance.ShowRewardPopup(reward.Resource);
                        _backOpen = true;
                    }
                    // DataAccountPlayer.PlayerSpin.OnWatchAdSpin();
                    // LoadData();
                    //
                    // if (reward.Resource.resourceType == ResourceType.Money)
                    // {
                    //     var moduleUi = (ModuleUiSpinReward) PopupController.Instance.OpenPopupAndKeepParent(PopupType.SpinRewardPopup);
                    //     moduleUi.InitData(reward.Resource);
                    //     ResourceHelper.AddResource(reward.Resource);
                    //     PopupController.Instance.ShowEarnReward((MoneyType) reward.Resource.id, reward.Resource.value,
                    //         reward.RewardTransform);
                    // }
                    // else if (reward.Resource.resourceType == ResourceType.Skin)
                    // {
                    //     ResourceHelper.AddResource(reward.Resource);
                    //     PopupController.Instance.ShowRewardPopup(reward.Resource);
                    // }
                    // else
                    // {
                    //     ResourceHelper.AddResource(reward.Resource);
                    //     PopupController.Instance.ShowRewardPopup(reward.Resource);
                    // }
                });
            });
            PopupController.Instance.GetHud().ShowBackButton(true);
        }

        private void OnClickSpin()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            DataAccountPlayer.PlayerTutorialData.ChangeSpinValue(false);
            _backOpen = false;
            Spin((SpinElement reward) =>
            {
                DataAccountPlayer.PlayerSpin.OnNormalSpin();
                LoadData();

                if (reward.Resource.resourceType == ResourceType.Money)
                {
                    // var moduleUi = (ModuleUiSpinReward) PopupController.Instance.OpenPopupAndKeepParent(PopupType.SpinRewardPopup);
                    // moduleUi.InitData(reward.Resource);
                    star.gameObject.SetActive(true);
                    ResourceHelper.AddResource(reward.Resource);
                    PopupController.Instance.ShowEarnReward((MoneyType) reward.Resource.id, reward.Resource.value,
                        reward.RewardTransform);
                    LeanTween.delayedCall(0.7f, () =>
                    {
                        star.gameObject.SetActive(false);
                        _backOpen = true;
                    });
                }
                else
                {
                    ResourceHelper.AddResource(reward.Resource);
                    PopupController.Instance.ShowRewardPopup(reward.Resource);
                    _backOpen = true;
                }
            });
           
        }

        private void Spin(Action<SpinElement> finishAction)
        {
            blockCanvas.SetActive(true);
            _playSoundCor = this.StartCoroutine(PlayTickSound());
            var spinElement = GetSpinElement();
            wheel.DOLocalRotate(new Vector3(0f, 0f, 360f), 1f, RotateMode.FastBeyond360)
                .SetLoops(1)
                .SetEase(Ease.Linear)
                .SetRelative()
                .OnComplete(() =>
                {
                    var targetAngle = CalculateTargetAngle(spinElement);
                    wheel.DOLocalRotate(new Vector3(0f, 0f, targetAngle), 3f, RotateMode.LocalAxisAdd)
                        .OnComplete(() =>
                        {
                            if (_playSoundCor != null)
                                StopCoroutine(_playSoundCor);

                            HideBlock();
                            finishAction?.Invoke(spinElement);
                        });
                });
        }

        private IEnumerator PlayTickSound()
        {
            float timer = 0;
            float timeWait = 0.1f;
            while (true)
            {
                yield return new WaitForSeconds(timeWait);
                // characterSound.PlayOneShot(SoundType.spin_tick);

                timer += timeWait;
                if (timer >= 2.5f)
                {
                    timeWait = 0.4f;
                }
                else if (timer >= 1f)
                {
                    timeWait = 0.2f;
                }
            }
        }

        private void HideBlock()
        {
            blockCanvas.SetActive(false);
            PopupController.Instance.GetHud().ShowBlockCanvas(false);
        }

        private float CalculateTargetAngle(SpinElement itemDetail)
        {
            var randomAngle = Random.Range(itemDetail.MinAngle, itemDetail.MaxAngle);
            float targetAngle = 0f;
            if (wheel.localEulerAngles.z < randomAngle)
            {
                targetAngle = Mathf.Abs(randomAngle - wheel.localEulerAngles.z) + 360f;
            }
            else
            {
                targetAngle = 360f - Mathf.Abs(randomAngle - wheel.localEulerAngles.z) + 360f;
            }
            return targetAngle;
        }

        private SpinElement GetSpinElement()
        {
            var randomPercent = Random.Range(0f, 1f);
            foreach (var spinElement in rewards)
            {
                if (spinElement.IsInRange(randomPercent))
                {
                    return spinElement;
                }
            }

            return rewards[0];
        }

        protected override void OnShow()
        {
            PopupController.Instance.ShowHud();
            PopupController.Instance.GetHud().ShowBackButton(true);
            blockCanvas.SetActive(false);
            LoadData();
        }

        private void LoadData()
        {
            var lastTimeSpin = DataAccountPlayer.PlayerSpin.lastTimeSpin;
            if (UtilityGame.GetCurrentTime() - lastTimeSpin >= 86400)
            {
                spinBtn.gameObject.SetActive(true);
                watchAdBtn.gameObject.SetActive(false);
                cooldownTxt.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                spinBtn.gameObject.SetActive(false);

                var numberOfAdWatched = DataAccountPlayer.PlayerSpin.adWatchInDay;
                bool isCanWatchAd = numberOfAdWatched < NumberOfAdCanWatch;
                watchAdBtn.gameObject.SetActive(isCanWatchAd);

                cooldownTxt.transform.parent.gameObject.SetActive(true);
                long remainTime = 86400 - (UtilityGame.GetCurrentTime() - lastTimeSpin);

                if (_countDownCor != null)
                {
                    StopCoroutine(_countDownCor);
                }
                _countDownCor = StartCoroutine(CountDownWatchAd(remainTime));
            }
        }

        private IEnumerator CountDownWatchAd(long _remainTime)
        {
            cooldownTxt.text = $"Free spin in:\n{GetCurrentTimeString(_remainTime)}";

            while (_remainTime > 0)
            {
                yield return new WaitForSeconds(1);
                _remainTime--;
                cooldownTxt.text = $"Free spin in:\n{GetCurrentTimeString(_remainTime)}";
            }

            LoadData();
        }

        private string GetCurrentTimeString(float time)
        {
            var timeSpan = TimeSpan.FromSeconds(time);
            return timeSpan.ToString(@"hh\:mm\:ss");
        }

        protected override void OnHide()
        {
            //blockCanvas.SetActive(true);
            PopupController.Instance.GetHud().ShowBackButton(false);
        }

        private void OnDisable()
        {
            if (_countDownCor != null)
            {
                StopCoroutine(_countDownCor);
                _countDownCor = null;
            }

            if (_playSoundCor != null)
            {
                StopCoroutine(_playSoundCor);
                _playSoundCor = null;
            }
        }
    }
}