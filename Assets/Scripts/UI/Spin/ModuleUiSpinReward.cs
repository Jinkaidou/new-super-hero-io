﻿using Base.Core;
using Base.Core.GameResources;
using Base.Core.Popup;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Spin
{
    public class ModuleUiSpinReward : PopupBase
    {
        [SerializeField] private Transform board;

        [SerializeField] private GameObject goldIcon;
        [SerializeField] private GameObject lifeIcon;
        [SerializeField] private Text value;

        [SerializeField] private Button closeBtn;
        [SerializeField] private Button x2Btn;

        private Resource _resource;

        private void Awake()
        {
            closeBtn.onClick.AddListener(OnClickClose);
            x2Btn.onClick.AddListener(OnClickX2Reward);
        }

        protected override void OnShow()
        {
            board.localScale = Vector3.zero;
            board.DOScale(Vector3.one, 0.5f).SetUpdate(true);

            closeBtn.gameObject.SetActive(false);
            this.StartDelayMethod(3f, () =>
            {
                closeBtn.gameObject.SetActive(true);
            });

// #if !UNITY_EDITOR
//             AdsManager.Instance.PlayBannerAds();
//             ResizeUiForBannerAd();
// #endif
        }

        public void InitData(Resource resource)
        {
            _resource = resource;
            if (_resource.id == (int) MoneyType.Gold)
            {
                goldIcon.SetActive(true);
                lifeIcon.SetActive(false);
            }
            else
            {
                lifeIcon.SetActive(true);
                goldIcon.SetActive(false);
            }

            value.text = resource.value.ToString();
        }

        protected override void OnHide()
        {
            // AdsManager.Instance.HideBannerAds();
        }

        private void OnClickClose()
        {
            ResourceHelper.AddResource(_resource);
            Close();
            if (_resource.resourceType == ResourceType.Money)
            {
                PopupController.Instance.ShowEarnReward((MoneyType) _resource.id, _resource.value, goldIcon.transform.position);
                // FirebaseManager.Instance.LogEarnCurrency((MoneyType) _resource.id, _resource.value, EarnSourceType.Spin);
            }
            else
            {
                PopupController.Instance.ShowRewardPopup(_resource);
            }
        }

        private void OnClickX2Reward()
        {
            // watch ads
            // if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            // {
            //     PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
            //     return;
            // }
            //
            // AdsManager.Instance.PlayRewardedAds((result) =>
            // {
            //     if (result == ShowAdResult.Finished)
            //     {
            //         FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForSpinX2);
            //
            //         _resource.value *= 2;
            //         ResourceHelper.AddResource(_resource);
            //         Close();
            //
            //         if (_resource.resourceType == ResourceType.Money)
            //         {
            //             PopupController.Instance.ShowEarnReward((MoneyType) _resource.id, _resource.value, goldIcon.transform.position);
            //             FirebaseManager.Instance.LogEarnCurrency((MoneyType) _resource.id, _resource.value, EarnSourceType.Spin);
            //         }
            //         else
            //         {
            //             PopupController.Instance.ShowRewardToast(_resource);
            //         }
            //     }
            //     else
            //     {
            //         PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
            //     }
            // });
        }

        private void ResizeUiForBannerAd()
        {
            // var rect = transform.GetComponent<RectTransform>();
            //

            // float bannerHeight = AdsManager.Instance.GetBannerHeight();
            // if (bannerHeight <= -1)
            // {
            //     rect.offsetMin = Vector2.zero;
            // }
            // else
            // {
            //     rect.offsetMin = new Vector2(0, bannerHeight + 70f);
            // }
        }
    }
}