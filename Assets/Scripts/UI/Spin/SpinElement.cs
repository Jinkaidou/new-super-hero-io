﻿using System;
using System.Collections.Generic;
using Base.Core.GameResources;
using Controller.LoadData;
using DataAccount;
using UI.ItemReward;
using UnityEngine;
using Model.DataHero;
using Random = UnityEngine.Random;

namespace UI.Spin
{
    public class SpinElement : MonoBehaviour
    {
        [SerializeField] private float percent;
        [SerializeField] private float minAngle;
        [SerializeField] private float maxAngle;
        [SerializeField] private ModuleItemReward reward;
        private HeroCollection _heroDataCollection;
        public float Percent => percent;

        public float MinAngle => minAngle;

        public float MaxAngle => maxAngle;

        public Resource Resource => reward.ResourceData;

        public Vector3 RewardTransform => reward.IconPosition;

        private float _minPercent;
        private float _maxPercent;

        private List<int> _skinId = new List<int>();
        private void Start()
        {
            _heroDataCollection = LoadResourceController.Instance.LoadHeroDataCollection();
            RandomSkin();
            if (reward.ResourceData.resourceType == ResourceType.Skin)
            {
                
                var id = Random.Range(0, _skinId.Count );
                var idSkin = _skinId[id];
                var skinId = (SkinType) idSkin;
                if (DataAccountPlayer.PlayerSkins.IsHaveOwnedSkin(skinId))
                {
                    return;
                }
                reward.ResourceData.id = idSkin;
                //reward.RotateSkin();
            }
        }

        private void RandomSkin()
        {
            foreach (var idSkin in _heroDataCollection.premiumSkins)
            {
                _skinId.Add((int)idSkin.skinName);
            }
        }
        
        public void SetRangePercent(float min, float max)
        {
            _minPercent = min;
            _maxPercent = max;
        }

        public bool IsInRange(float result)
        {
            if (result >= _minPercent && result <= _maxPercent)
            {
                return true;
            }

            return false;
        }
        
        
    }
}