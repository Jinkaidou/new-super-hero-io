﻿using Base.Core.Popup;
using Base.Core.Sound;
using DataAccount;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.SettingPopup
{
    public class ModuleUiSetting : PopupBase
    {
        [SerializeField] private Transform board;
        [SerializeField] private Button closeBtn;
        [SerializeField] private Button backgroundBtn;

        [SerializeField] private Button musicOnBtn;
        [SerializeField] private Button musicOffBtn;

        [SerializeField] private Button soundOnBtn;
        [SerializeField] private Button soundOffBtn;

        [SerializeField] private Button vibrationOnBtn;
        [SerializeField] private Button vibrationOffBtn;

        [SerializeField] private TextMeshProUGUI versionTxt;

        private void Start()
        {
            closeBtn.onClick.AddListener(Close);
            backgroundBtn.onClick.AddListener(Close);

            musicOnBtn.onClick.AddListener(() => OnMusicChange(false));
            musicOffBtn.onClick.AddListener(() => OnMusicChange(true));

            soundOnBtn.onClick.AddListener(() => OnSoundChange(false));
            soundOffBtn.onClick.AddListener(() => OnSoundChange(true));

            vibrationOnBtn.onClick.AddListener(() => OnVibrationChange(false));
            vibrationOffBtn.onClick.AddListener(() => OnVibrationChange(true));

            versionTxt.text = $"Version {Application.version}";
        }

        protected override void OnShow()
        {
            board.localScale = Vector3.zero;
            board.DOScale(Vector3.one, 0.5f).SetUpdate(true);
            
            var playerSettings = DataAccountPlayer.PlayerSettings;

            musicOnBtn.gameObject.SetActive(playerSettings.MusicOff);
            musicOffBtn.gameObject.SetActive(!playerSettings.MusicOff);

            soundOnBtn.gameObject.SetActive(playerSettings.SoundOff);
            soundOffBtn.gameObject.SetActive(!playerSettings.SoundOff);

            vibrationOnBtn.gameObject.SetActive(playerSettings.VibrationOff);
            vibrationOffBtn.gameObject.SetActive(!playerSettings.VibrationOff);
        }

        private void OnMusicChange(bool isOff)
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            DataAccountPlayer.PlayerSettings.SetMusic(isOff);
            musicOnBtn.gameObject.SetActive(DataAccountPlayer.PlayerSettings.MusicOff);
            musicOffBtn.gameObject.SetActive(!DataAccountPlayer.PlayerSettings.MusicOff);
        }

        private void OnSoundChange(bool isOff)
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            DataAccountPlayer.PlayerSettings.SetSound(isOff);
            soundOnBtn.gameObject.SetActive(DataAccountPlayer.PlayerSettings.SoundOff);
            soundOffBtn.gameObject.SetActive(!DataAccountPlayer.PlayerSettings.SoundOff);
        }

        private void OnVibrationChange(bool isOff)
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            DataAccountPlayer.PlayerSettings.SetVibration(isOff);
            vibrationOnBtn.gameObject.SetActive(DataAccountPlayer.PlayerSettings.VibrationOff);
            vibrationOffBtn.gameObject.SetActive(!DataAccountPlayer.PlayerSettings.VibrationOff);
        }

        protected override void OnHide()
        {
        }
    }
}