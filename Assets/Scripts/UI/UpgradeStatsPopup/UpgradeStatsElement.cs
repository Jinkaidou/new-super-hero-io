﻿using Base.Core.Ads;
using Base.Core.Analytics;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using Controller.LoadData;
using DataAccount;
using TMPro;
using UI.UpgradeStatsPopup.Data;
using UnityEngine;
using UnityEngine.UI;

namespace UI.UpgradeStatsPopup
{
    public class UpgradeStatsElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameTxt;
        [SerializeField] private Image statIcon;
        [SerializeField] private TextMeshProUGUI levelTxt;
        [SerializeField] private TextMeshProUGUI statDetailTxt;
        [SerializeField] private TextMeshProUGUI statAddTxt;

        [SerializeField] private TextMeshProUGUI upgradeCostTxt;
        [SerializeField] private Button upgradeBtn;
        [SerializeField] private Button watchAdUpgradeBtn;
        [SerializeField] private Button freeUpdate;
        public GameObject statFX;
        public Animator text;
        private StatUpgradeData _data;

        private void Start()
        {
            upgradeBtn.onClick.AddListener(OnClickUpgrade);
            watchAdUpgradeBtn.onClick.AddListener(OnClickWatchAdUpgrade);
            freeUpdate.onClick.AddListener(OnClickUpgradeFree);
        }

        private void SetTutourial()
        {
            var playerTutorialData = DataAccountPlayer.PlayerTutorialData;
            if (playerTutorialData.firstTimeUpgrade)
            {
                freeUpdate.gameObject.SetActive(true);
                watchAdUpgradeBtn.gameObject.SetActive(false);
                upgradeBtn.gameObject.SetActive(false);
            }
            else
            {
                freeUpdate.gameObject.SetActive(false);
            }
        }
        
        public void InitData(StatUpgradeData data)
        {
            _data = data;
            nameTxt.text = _data.name;
            statIcon.sprite = LoadResourceController.Instance.LoadStatUpgradeIcon(_data.statType);
            levelTxt.text = $"Level {DataAccountPlayer.PlayerStats.GetStatLevel(_data.statType)}";
            
            var value = DataAccountPlayer.PlayerStats.GetStatLevel(_data.statType) * _data.percentAdd;

            statDetailTxt.text = _data.statType.ToString();
            statAddTxt.text = $"+<color=#4AFF00>{value}</color>%";

            var cost = _data.cost + DataAccountPlayer.PlayerStats.GetPrice(_data.statType);
            upgradeCostTxt.text =cost.ToString();
            LoadButtons();
        }

        public void ReloadData()
        {
            levelTxt.text = $"Level {DataAccountPlayer.PlayerStats.GetStatLevel(_data.statType)}";
            statAddTxt.text = $"+<color=#4AFF00>{_data.percentAdd * DataAccountPlayer.PlayerStats.GetStatLevel(_data.statType)}</color>%";
            var cost = _data.cost + DataAccountPlayer.PlayerStats.GetPrice(_data.statType);
            upgradeCostTxt.text =cost.ToString();
            LoadButtons();
        }

        private void LoadButtons()
        {
            var cost = _data.cost + DataAccountPlayer.PlayerStats.GetPrice(_data.statType);
            bool isEnoughMoney = DataAccountPlayer.PlayerMoney.IsEnough(MoneyType.Gold, cost);
            upgradeBtn.gameObject.SetActive(isEnoughMoney);
            watchAdUpgradeBtn.gameObject.SetActive(!isEnoughMoney);
            SetTutourial();
            statFX.SetActive(false);
        }

        private void EffectButton()
        {
            this.PostEvent(EventID.StatUpgradeSuccess);
            text.enabled = true;
            text.Play("Text_valueUpgrade",-1,0);
            statFX.SetActive(true);
            LeanTween.delayedCall(0.8f, () =>
            {
                statFX.SetActive(false);
                text.enabled = false;
            });
        }
        
        private void OnClickUpgradeFree()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            DataAccountPlayer.PlayerStats.OnStatUpgrade(_data.statType);
            DataAccountPlayer.PlayerTutorialData.ChangeUpgradeValue(false);
            DataAccountPlayer.PlayerTutorialData.ChangeValueUpgradeFirstTime(false);
            EffectButton();
            this.PostEvent(EventID.StatUpgradeSuccessFree);
        }
        
        private void OnClickUpgrade()
        {
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            if (!DataAccountPlayer.PlayerMoney.IsEnough(MoneyType.Gold, _data.cost))
            {
                PopupController.Instance.OpenNotification(ContentString.NotEnoughMoney);
                return;
            }

            var price = _data.cost + DataAccountPlayer.PlayerStats.GetPrice(_data.statType);
            DataAccountPlayer.PlayerMoney.SetMoney(false, MoneyType.Gold, price);
            DataAccountPlayer.PlayerStats.OnStatUpgrade(_data.statType);
            EffectButton();
        }

        private void OnClickWatchAdUpgrade()
        {
            FirebaseManager.Instance.LogEvent(FirebaseParams.ENTER_ADS);
            if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            {
                PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                return;
            }
            AdsManager.Instance.PlayRewardedAds((result) =>
            {
                if (result != ShowAdResult.Finished)
                {
                    PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                    return;
                }
                FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
                //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
                FirebaseManager.Instance.LogUpgradeStat(_data.statType);
            });
            
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            DataAccountPlayer.PlayerStats.OnStatUpgrade(_data.statType);

            
            EffectButton();
        }
    }
}