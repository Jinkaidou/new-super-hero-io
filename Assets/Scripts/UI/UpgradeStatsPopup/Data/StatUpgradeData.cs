﻿using System;
using Base.Core.GameResources;

namespace UI.UpgradeStatsPopup.Data
{
    [Serializable]
    public class StatUpgradeData
    {
        public string name;
        public float percentAdd;
        public int cost;
        public StatType statType;
    }
}