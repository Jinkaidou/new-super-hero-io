﻿using System.Collections.Generic;
using Base.Core.GameResources;
using UnityEngine;

namespace UI.UpgradeStatsPopup.Data
{
    // DO NOT change line CreateAssetMenu
    [CreateAssetMenu(menuName = "Data/StatUpgradeData", fileName = "StatUpgradeData")]
    public class StatUpgradeDataAsset : ScriptableObject
    {
        public List<StatUpgradeData> statUpgradeData = new List<StatUpgradeData>();

        public StatUpgradeData GetStatUpgrade(StatType statType)
        {
            foreach (var data in statUpgradeData)
            {
                if (data.statType == statType)
                {
                    return data;
                }
            }

            return null;
        }
    }
}