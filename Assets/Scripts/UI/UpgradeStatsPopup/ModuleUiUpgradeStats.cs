﻿using System;
using System.Collections.Generic;
using Base.Core.Ads;
using Base.Core.Analytics;
using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using Controller.LoadData;
using DataAccount;
using UI.UpgradeStatsPopup.Data;
using UnityEngine;
using UnityEngine.UI;

namespace UI.UpgradeStatsPopup
{
    public class ModuleUiUpgradeStats : PopupBase
    {
        [SerializeField] private Button resetAllBtn;
        [SerializeField] private RectTransform elementParent;
        [SerializeField] private UpgradeStatsElement elementSample;
        
        private List<UpgradeStatsElement> _allElements = new List<UpgradeStatsElement>();
        private StatUpgradeDataAsset _dataAsset;

        private void Awake()
        {
            _dataAsset = LoadResourceController.Instance.LoadStatUpgradeData();
            resetAllBtn.onClick.AddListener(OnClickResetAll);
            
            this.RegisterListener(EventID.StatUpgradeSuccess, (sender, param) =>
            {
                ReloadData();
            });
        }

        protected override void OnShow()
        {
            PopupController.Instance.GetHud().ShowBackButton(true);
            if (_allElements.Count <= 0)
            {
                SetData();
            }
            else
            {
                ReloadData();
            }
        }

        protected override void OnHide()
        {
            PopupController.Instance.GetHud().ShowBackButton(false);
        }

        private void SetData()
        {
            var allStats = _dataAsset.statUpgradeData;
            foreach (var stat in allStats)
            {
                var elementClone = Instantiate(elementSample, elementParent, false);
                _allElements.Add(elementClone);
                
                elementClone.InitData(stat);
            }
        }

        private void ReloadData()
        {
            foreach (var element in _allElements)
            {
                element.ReloadData();
            }
        }

        private void OnClickResetAll()
        {
            FirebaseManager.Instance.LogEvent(FirebaseParams.ENTER_ADS);
            SoundManager.Instance.PlayOneShot(SoundType.ClickButton);
            
            // int totalGolds = 0;
            //
            // foreach (StatType statType in Enum.GetValues(typeof(StatType)))
            // {
            //     int currentLevel = DataAccountPlayer.PlayerStats.GetStatLevel(statType);
            //     if (currentLevel <= 1)
            //         continue;
            //
            //     var statData = _dataAsset.GetStatUpgrade(statType);
            //     int levelReturn = currentLevel - 1;
            //     int goldReturn = levelReturn * statData.cost;
            //     totalGolds += goldReturn;
            // }
            //     
            // DataAccountPlayer.PlayerMoney.SetMoney(true, MoneyType.Gold, totalGolds);
            // DataAccountPlayer.PlayerStats.OnResetAllStat();
            // PopupController.Instance.GetHud().ShowBackButton(true);
            // ReloadData();
            
            if (!AdsManager.Instance.IsHaveVideoRewardedAds())
            {
                PopupController.Instance.OpenNotification(ContentString.AdsNotHave);
                return;
            }
            AdsManager.Instance.PlayRewardedAds((result) =>
            {
                if (result != ShowAdResult.Finished)
                {
                    PopupController.Instance.OpenNotification(ContentString.WatchAdFail);
                    return;
                }
                //FirebaseManager.Instance.LogAdsRewardFor(AdRewardFor.WatchAdsForShop);
                FirebaseManager.Instance.LogEvent(FirebaseParams.WATCH_AD);
                FirebaseManager.Instance.LogEvent(FirebaseParams.DONE_ADS);
                int totalGolds = 0;
            
                foreach (StatType statType in Enum.GetValues(typeof(StatType)))
                {
                    int currentLevel = DataAccountPlayer.PlayerStats.GetStatLevel(statType);
                    if (currentLevel <= 1)
                        continue;
            
                    var statData = _dataAsset.GetStatUpgrade(statType);
                    int levelReturn = currentLevel - 1;
                    int goldReturn = levelReturn * statData.cost;
                    totalGolds += goldReturn;
                }
                
                DataAccountPlayer.PlayerMoney.SetMoney(true, MoneyType.Gold, totalGolds);
                DataAccountPlayer.PlayerStats.OnResetAllStat();
                PopupController.Instance.GetHud().ShowBackButton(true);
                ReloadData();
            });
            
        }
    }
}