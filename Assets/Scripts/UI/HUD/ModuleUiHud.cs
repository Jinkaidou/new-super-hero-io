﻿using Base.Core.GameResources;
using Base.Core.Popup;
using Base.Core.Sound;
using DataAccount;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.HUD
{
    public class ModuleUiHud : PopupBase
    {
        [SerializeField] private Button goldAddBtn;
        [SerializeField] private Button backBtn;

        [SerializeField] private TextMeshProUGUI goldNumberTxt;

        [SerializeField] private GameObject blockCanvas;

        public Transform goldIcon;

        public Vector2 GoldPosition => goldIcon.position;

        public Vector2 LifePosition => Vector2.zero;


        private void Awake()
        {
            this.RegisterListener(EventID.EarnMoney, (sender, param) =>
            {
                OnMoneyChange1((MoneyType) param);
            });

            this.RegisterListener(EventID.SpendMoney, (sender, param) =>
            {
                OnMoneyChange((MoneyType) param);
            });

            goldAddBtn.onClick.AddListener(OnClickAddGold);
            backBtn.onClick.AddListener(OnClickBackBtn);
        }

        private void OnEnable()
        {
            LoadGoldNumber();
            ShowBlockCanvas(false);
        }

        public void ShowBlockCanvas(bool isShow)
        {
            blockCanvas.SetActive(isShow);
        }

        public void ShowBackButton(bool isShow)
        {
            backBtn.gameObject.SetActive(isShow);
        }

        private void OnClickAddGold()
        {
            SoundManager.Instance.PlaySound(SoundType.ClickButton);
            PopupController.Instance.OpenPopupAndCloseParentWithHud(PopupType.ShopPopup);
        }

        private void OnClickBackBtn()
        {
            SoundManager.Instance.PlayOneShot(SoundType.PopupClose);
            PopupController.Instance.CloseCurrentPopupAndOpenParent();
        }

        private void OnMoneyChange(MoneyType moneyType)
        {
            if (moneyType == MoneyType.Gold)
            {
                LoadGoldNumber();
                return;
            }
        }
        
        private void OnMoneyChange1(MoneyType moneyType)
        {
            backBtn.gameObject.SetActive(false);
            if (moneyType == MoneyType.Gold)
            {
                LoadGoldNumber();
                return;
            }
        }
        private void LoadGoldNumber()
        {
            var newValue = DataAccountPlayer.PlayerMoney.GetMoney(MoneyType.Gold);
            int.TryParse(goldNumberTxt.text, out var myScore);
            DOTween.To(() => myScore, value => myScore = value, newValue, 1f)
                .OnUpdate(() => goldNumberTxt.text = myScore.ToString())
                .SetUpdate(true);
            goldNumberTxt.transform.DOScale(new Vector3(1.25f, 1.25f), 0.5f)
                .OnComplete(() =>
                {
                    goldNumberTxt.transform.DOScale(Vector3.one, 0.5f);
                });
        }

        protected override void OnShow()
        {
        }

        protected override void OnHide()
        {
        }
    }
}