﻿using System.Collections.Generic;
using Base.Core.GameResources;

namespace DataAccount
{
    public class PlayerGift
    {
        public int totalAdsWatched;

        public List<SkinType> skinUnlocked;

        public PlayerGift()
        {
            skinUnlocked = new List<SkinType>();
        }

        public void OnWatchAdSuccess()
        {
            totalAdsWatched += 1;
            DataAccountPlayer.SavePlayerGift();
        }

        public void OnUnlockSkin(SkinType skinType, int adReduce)
        {
            if (!skinUnlocked.Contains(skinType))
            {
                skinUnlocked.Add(skinType);
                totalAdsWatched -= adReduce;
                DataAccountPlayer.SavePlayerGift();
            }
        }

        public bool IsSkinUnlocked(SkinType skin)
        {
            bool isUnlocked = skinUnlocked.Contains(skin);
            if (!isUnlocked)
            {
                isUnlocked = DataAccountPlayer.PlayerSkins.IsHaveOwnedSkin(skin);
            }

            return isUnlocked;
        }
    }
}