using UnityEngine;

namespace DataAccount
{
    public class PlayerStatistic 
    {
        public long time;
        public int kill;
        public float point;

        public long bestTime;
        public int bestKill;
        public float bestPoint;

        public int goldLastRound;
        public string playerName;

        
        
        public PlayerStatistic()
        {
            int randomNumb = Random.Range(0, 10000);
            playerName = $"Player_{randomNumb}";
        }

        public void OnChangeName(string nameNew)
        {
            playerName = nameNew;
            DataAccountPlayer.SavePlayerStatistic();
        }
        
        public void SetValueTime(long value)
        {
            time = value;
            DataAccountPlayer.SavePlayerStatistic();
        }

        public void SetValueKill(int value)
        {
            kill = value;
            DataAccountPlayer.SavePlayerStatistic();
        }

        public void SetValuePoint(float value)
        {
            point = value;
            DataAccountPlayer.SavePlayerStatistic();
        }

        public void SetValueGold(int value)
        {
            goldLastRound = value;
            DataAccountPlayer.SavePlayerStatistic();
        }
        
        public void SetBestTime(long value)
        {
            if (value > bestTime)
            {
                bestTime = value;
                DataAccountPlayer.SavePlayerStatistic();
            }
        }
        
        public void SetBestKill(int value)
        {
            if (value > bestKill)
            {
                bestKill = value;
                DataAccountPlayer.SavePlayerStatistic();
            }
        }
        
        public void SetBestPoint(float value)
        {
            if (value > bestPoint)
            {
                bestPoint = value;
                DataAccountPlayer.SavePlayerStatistic();
            }
        }
    }
}
