﻿using System.Collections.Generic;
using Base.Core.GameResources;

namespace DataAccount
{
    public class PlayerSkins
    {
        public SkinType skinUsing;
        public List<SkinType> skinOwned;

        // key: skin name; value: number of ad watch.
        public Dictionary<SkinType, int> skinAdsWatched;

        //key: skin name; value: number of percent the skin has unlocked (4/4) (use for normal skin)
        public Dictionary<SkinType, int> skinNormalUnlockProcess;

        public SkinType skinNameUnlock = SkinType.america;
        public PlayerSkins()
        {
            skinOwned = new List<SkinType>()
            {
                SkinType.vietnam
            };
            
            skinUsing = skinOwned[0];
            skinAdsWatched = new Dictionary<SkinType, int>();
            skinNormalUnlockProcess = new Dictionary<SkinType, int>()
            {
                {SkinType.vietnam, 4}
            };
        }

        public string TrySkin { get; set; }

        public SkinType SkinJustGet { get; set; } = SkinType.none;

        public bool IsHaveOwnedSkin(SkinType skinName)
        {
            return skinOwned.Contains(skinName);
        }

        public int GetAdsWatchedNumber(SkinType skinName)
        {
            if (skinAdsWatched.ContainsKey(skinName))
            {
                return skinAdsWatched[skinName];
            }

            return 0;
        }

        public bool OnChangeSkin(SkinType skinName)
        {
            if (IsHaveOwnedSkin(skinName))
            {
                skinUsing = skinName;
                DataAccountPlayer.SavePlayerSkins();
                return true;
            }

            return false;
        }

        public void OnWatchAds(SkinType skinName)
        {
            if (skinAdsWatched.ContainsKey(skinName))
            {
                skinAdsWatched[skinName] += 1;
            }
            else
            {
                skinAdsWatched.Add(skinName, 1);
            }

            DataAccountPlayer.SavePlayerSkins();
        }

        public void AddOwnedSkin(SkinType skinName)
        {
            if (!skinOwned.Contains(skinName))
            {
                skinOwned.Add(skinName);
                DataAccountPlayer.SavePlayerSkins();
            }
        }

        public void OnRandomSkinGold(SkinType skinName)
        {
            if (!skinOwned.Contains(skinName))
            {
                skinOwned.Add(skinName);
            }
            
            DataAccountPlayer.SavePlayerSkins();
        }

        public SkinType GetCurrentNormalSkinUnlocking(out int currentProcess)
        {
            foreach (var keyValuePair in skinNormalUnlockProcess)
            {
                if (keyValuePair.Value < 4)
                {
                    currentProcess = keyValuePair.Value;
                    return keyValuePair.Key;
                }
            }

            currentProcess = 0;
            return SkinType.none;
        }

        public bool IsSkinNormalUnlocked(SkinType skinType)
        {
            return skinNormalUnlockProcess.ContainsKey(skinType);
        }

        public void ChangeSkinUnlock(SkinType skinType)
        {
            skinNameUnlock = skinType;
            DataAccountPlayer.SavePlayerSkins(); 
        }
        
        public void OnUpdateUnlockNormalSkin(SkinType skinType)
        {
            if (skinNormalUnlockProcess.ContainsKey(skinType))
            {
                skinNormalUnlockProcess[skinType] += 1;
            }
            else
            {
                skinNormalUnlockProcess[skinType] = 1;
            }
            
            DataAccountPlayer.SavePlayerSkins();
        }
    }
}