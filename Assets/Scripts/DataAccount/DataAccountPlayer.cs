﻿namespace DataAccount
{
    public static class DataAccountPlayer
    {
        private static PlayerMoney _playerMoney;
        private static PlayerDailyCheckIn _playerDailyCheckIn;
        private static PlayerSettings _playerSettings;
        private static PlayerSkins _playerSkins;
        private static PlayerSpin _playerSpin;
        private static PlayerGift _playerGift;
        private static PlayerStats _playerStats;
        private static PlayerStatistic _playerStatistic;
        private static PlayerTutorialData _playerTutorialData;
        
        #region Getters

        public static PlayerMoney PlayerMoney
        {
            get
            {
                if (_playerMoney != null)
                {
                    return _playerMoney;
                }

                _playerMoney = ES3.Load(DataAccountPlayerConstants.PlayerMoney, new PlayerMoney());
                return _playerMoney;
            }
        }

        public static PlayerDailyCheckIn PlayerDailyCheckIn
        {
            get
            {
                if (_playerDailyCheckIn != null)
                {
                    return _playerDailyCheckIn;
                }

                var playerDailyCheckIn = new PlayerDailyCheckIn();
                _playerDailyCheckIn = ES3.Load(DataAccountPlayerConstants.PlayerDailyCheckIn, playerDailyCheckIn);
                return _playerDailyCheckIn;
            }
        }

        public static PlayerSettings PlayerSettings
        {
            get
            {
                if (_playerSettings != null)
                {
                    return _playerSettings;
                }

                var playerSettings = new PlayerSettings();
                _playerSettings = ES3.Load(DataAccountPlayerConstants.PlayerSettings, playerSettings);
                return _playerSettings;
            }
        }

        public static PlayerSkins PlayerSkins
        {
            get
            {
                if (_playerSkins != null)
                    return _playerSkins;

                var playerSkins = new PlayerSkins();
                _playerSkins = ES3.Load(DataAccountPlayerConstants.PlayerSkins, playerSkins);
                return _playerSkins;
            }
        }

        public static PlayerSpin PlayerSpin
        {
            get
            {
                if (_playerSpin != null)
                    return _playerSpin;

                var playerSpin = new PlayerSpin();
                _playerSpin = ES3.Load(DataAccountPlayerConstants.PlayerSpin, playerSpin);
                return _playerSpin;
            }
        }

        public static PlayerGift PlayerGift
        {
            get
            {
                if (_playerGift != null)
                    return _playerGift;
                
                var playerGift = new PlayerGift();
                _playerGift = ES3.Load(DataAccountPlayerConstants.PlayerGift, playerGift);
                return _playerGift;
            }
        }

        public static PlayerStats PlayerStats
        {
            get
            {
                if (_playerStats != null)
                    return _playerStats;
                
                var playerStats = new PlayerStats();
                _playerStats = ES3.Load(DataAccountPlayerConstants.PlayerStats, playerStats);
                return _playerStats;
            }
        }


        public static PlayerStatistic PlayerStatistic
        {
            get
            {
                if (_playerStatistic != null)
                    return _playerStatistic;
                
                var playerStatistic = new PlayerStatistic();
                _playerStatistic = ES3.Load(DataAccountPlayerConstants.PlayerStatistic, playerStatistic);
                return _playerStatistic;
            }
        }
        
        public static PlayerTutorialData PlayerTutorialData
        {
            get
            {
                if (_playerTutorialData != null)
                    return _playerTutorialData;
                
                var playerTutorialData = new PlayerTutorialData();
                _playerTutorialData = ES3.Load(DataAccountPlayerConstants.PlayerTutorialData, playerTutorialData);
                return _playerTutorialData;
            }
        }
        
        #endregion

        #region Save

        public static void SavePlayerMoney()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerMoney, _playerMoney);
        }

        public static void SavePlayerDailyCheckIn()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerDailyCheckIn, _playerDailyCheckIn);
        }

        public static void SavePlayerSettings()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerSettings, _playerSettings);
        }
        
        public static void SavePlayerSkins()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerSkins, _playerSkins);
        }

        public static void SavePlayerSpin()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerSpin, _playerSpin);
        }

        public static void SavePlayerGift()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerGift, _playerGift);
        }

        public static void SavePlayerStats()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerStats, _playerStats);
        }

        public static void SavePlayerStatistic()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerStatistic, _playerStatistic);
        }
        
        public static void SavePlayerTutorialData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerTutorialData, _playerTutorialData);
        }
        
        #endregion
    }
}