﻿namespace DataAccount
{
    public struct DataAccountPlayerConstants
    {
        public const string PlayerMoney = "PlayerMoney";
        public const string PlayerDailyCheckIn = "PlayerDailyCheckIn";
        public const string PlayerSettings = "PlayerSettings";
        public const string PlayerSkins = "PlayerSkins";
        public const string PlayerSpin = "PlayerSpin";
        public const string PlayerGift = "PlayerGift";
        public const string PlayerStats = "PlayerStats";
        public const string PlayerStatistic = "PlayerStatistic";
        public const string PlayerTutorialData = "PlayerTutorialData";
        public const string PlayerName = "PlayerName";
    }
}