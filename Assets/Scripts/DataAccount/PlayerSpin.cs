﻿using Base.Core;

namespace DataAccount
{
    public class PlayerSpin
    {
        public long lastTimeSpin;
        public int adWatchInDay;

        public void OnNormalSpin()
        {
            lastTimeSpin = UtilityGame.GetCurrentTime();
            adWatchInDay = 0;
            DataAccountPlayer.SavePlayerSpin();
        }

        public void OnWatchAdSpin()
        {
            adWatchInDay += 1;
            DataAccountPlayer.SavePlayerSpin();
        }
    }
}