﻿using System.Collections.Generic;
using Base.Core.GameResources;

namespace DataAccount
{
    public class PlayerStats
    {
        //key: statType; value: level
        public Dictionary<StatType, int> allStats;
        public Dictionary<StatType, int> allStatsCost;
        
        public PlayerStats()
        {
            allStats = new Dictionary<StatType, int>();
            allStatsCost = new Dictionary<StatType, int>();
        }

        public int GetStatLevel(StatType statType)
        {
            return allStats.ContainsKey(statType) ? allStats[statType] : 1;
        }

        public int GetPrice(StatType statType)
        {
            return allStatsCost.ContainsKey(statType) ? allStatsCost[statType] : 0;
        }
        
        public void OnStatUpgrade(StatType statType, int value = 1)
        {
            if (allStats.ContainsKey(statType))
            {
                allStats[statType] += value;
                allStatsCost[statType] += 15;
            }
            else
            {
                allStats[statType] = 1 + value;
                allStatsCost[statType] = 15;
            }
            
            DataAccountPlayer.SavePlayerStats();
        }

        public void OnResetAllStat()
        {
            var keys = new List<StatType>(allStats.Keys);
            foreach (var key in keys)
            {
                allStats[key] = 1;
                allStatsCost[key] = 0;
            }
            
            DataAccountPlayer.SavePlayerStats();
        }
    }
}